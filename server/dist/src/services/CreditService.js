"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreditService = void 0;
const di_1 = require("@tsed/di");
const typeorm_1 = require("typeorm");
const CreditLog_1 = require("../entities/CreditLog");
const DateRange_1 = require("../models/Common/DateRange");
const Page_1 = require("../models/Common/Page");
const CreditRepo_1 = require("../repositorys/CreditRepo");
let CreditService = class CreditService {
    constructor(creditRepo) {
        this.creditRepo = creditRepo;
    }
    async findAll(dateRange, page) {
        const [items, total] = await this.creditRepo.findAndCount({
            relations: ['pepay', 'user'],
            where: {
                ...DateRange_1.DateRangeParams.toWhereBetween('createdAt', dateRange),
                deletedAt: typeorm_1.IsNull(),
            },
            ...Page_1.PageParams.toTypeOrmOptions(page),
        });
        return {
            items,
            total,
        };
    }
    async create(appId, data) {
        console.log(appId, data);
        let dbData = new CreditLog_1.CreditLog({
            ...data,
            appId,
            createdAt: new Date(),
        });
        dbData = await this.creditRepo.save(dbData);
        return await this.creditRepo.findOne(dbData.id, {
            relations: ['user', 'pepay'],
        });
    }
    async edit(id, data) {
        let dbData = await this.creditRepo.findOne(id, {
            relations: ['user', 'pepay'],
        });
        dbData = Object.assign(dbData, data);
        await this.creditRepo.save(dbData);
        return dbData;
    }
    async delete(id) {
        const dbData = await this.creditRepo.findOne(id, {
            relations: ['user', 'pepay'],
        });
        dbData.deletedAt = new Date();
        await this.creditRepo.save(dbData);
        return dbData;
    }
    async stats() {
        const result = await this.creditRepo.calcStats();
        console.log(result);
        return result;
    }
};
CreditService = __decorate([
    di_1.Service(),
    __metadata("design:paramtypes", [CreditRepo_1.CreditRepo])
], CreditService);
exports.CreditService = CreditService;
//# sourceMappingURL=CreditService.js.map