"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const di_1 = require("@tsed/di");
const typeorm_1 = require("typeorm");
const BaseError_1 = require("../exceptions/BaseError");
const UserRepo_1 = require("../repositorys/UserRepo");
const UserSerivce_1 = require("./UserSerivce");
const jwt = require("jsonwebtoken");
const AppRepo_1 = require("../repositorys/AppRepo");
const Info_1 = require("../models/Auth/Info");
const Body_1 = require("../models/Register/Body");
const SMSService_1 = require("./SMSService");
const User_1 = require("../entities/User");
const EmailService_1 = require("./EmailService");
const ValidBody_1 = require("../models/Forget/ValidBody");
const Temp_1 = require("../models/Valid/Temp");
const ResetBody_1 = require("../models/Forget/ResetBody");
const ForgetResult_1 = require("../models/Auth/ForgetResult");
const Application_1 = require("../entities/Application");
let AuthService = class AuthService {
    constructor(userRepo, userSerivice, appRepo, smsService, emailService) {
        this.userRepo = userRepo;
        this.userSerivice = userSerivice;
        this.appRepo = appRepo;
        this.smsService = smsService;
        this.emailService = emailService;
    }
    async generateToken(apiKey, userId) {
        const dbApp = await this.appRepo.findOne({ apiKey });
        if (dbApp === undefined || dbApp === null) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.API_KEY_NOT_FOUND);
        }
        return jwt.sign({
            userId,
            exp: new Date().getTime() + 1000 * 60 * 24 * 365,
        }, dbApp.apiSecert);
    }
    async basicLogin(username, password) {
        const dbData = await this.userRepo.findOne({
            where: {
                username,
                deletedAt: typeorm_1.IsNull(),
                isDisabled: false,
            },
        });
        if (dbData === undefined) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.AUTH_FAILED);
        }
        if (this.userSerivice.cryptoPassword(password) !== dbData.password) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.AUTH_FAILED);
        }
        if (dbData.isDisabled) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.AUTH_FAILED);
        }
        return new Info_1.BasicAuthInfo({
            userId: dbData.id,
        });
    }
    async tokenLogin(appId, token) {
        const dbApp = await this.appRepo.findOne(appId);
        const payload = jwt.verify(token, dbApp.apiSecert);
        if (!(payload.exp > new Date().getTime())) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.AUTH_TOKEN_FAILED);
        }
        const dbData = await this.userRepo.findOne({
            id: payload.userId,
            deletedAt: typeorm_1.IsNull(),
            isDisabled: false,
        });
        if (dbData === undefined) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.AUTH_FAILED);
        }
        return new Info_1.BasicAuthInfo({
            userId: dbData.id,
        });
    }
    async webLogin(apiKey, username, password) {
        const res = await this.basicLogin(username, password);
        return new Info_1.WebAuthInfo({
            userId: res.userId,
            token: await this.generateToken(apiKey, res.userId),
        });
    }
    async webProviderLogin(apiKey, provider, email) {
        const res = await this.webProviderLogin(apiKey, provider, email);
        return new Info_1.WebAuthInfo({
            userId: res.id,
            token: await this.generateToken(apiKey, res.id),
        });
    }
    async register(appId, apiKey, data, manager) {
        await this.smsService.$validSMSCode(data.phone, data.smsCode, manager);
        if (await this.userRepo.exist({ username: data.username })) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_USERNAME_IS_EXIST);
        }
        if (await this.userRepo.exist({ email: data.email })) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_EMAIL_IS_EXIST);
        }
        if (await this.userRepo.exist({ phone: data.phone })) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_PHONE_IS_EXIST);
        }
        let dbData = new User_1.User({
            ...data,
            isSmsValid: true,
            appId: appId,
        });
        dbData = await manager.save(dbData);
        return new Info_1.WebAuthInfo({
            userId: dbData.id,
            token: await this.generateToken(apiKey, dbData.id),
        });
    }
    async sendForgetEmail(appId, data, manager) {
        const dbUser = await manager.findOne(User_1.User, {
            email: data.email,
            username: data.username,
            phone: data.phone,
        });
        if (dbUser === undefined || dbUser === null) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_NOT_FOUND);
        }
        await this.smsService.$validSMSCode(data.phone, data.smsCode, manager);
        await this.emailService.$sendValidEmail(appId, {
            email: data.email,
            temp: Temp_1.ValidTemp.FOREGT,
            ...data,
        }, manager);
    }
    async resetPassword(token, data, manager) {
        const emailLog = await this.emailService.$validToken(token, manager);
        const dbUser = await manager.findOne(User_1.User, {
            email: emailLog.email,
            phone: data.phone,
            deletedAt: typeorm_1.IsNull(),
        });
        if (dbUser === undefined || dbUser === null) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_NOT_FOUND);
        }
        if (data.password !== data.confirmPassword) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.REGISTER_TWO_PASSWORD_FAILED);
        }
        dbUser.password = this.userSerivice.cryptoPassword(data.password);
        await manager.save(dbUser);
        const app = await manager.findOne(Application_1.Application, emailLog.appId);
        return new ForgetResult_1.ForgetResult({
            apiKey: app.apiKey,
            redirectUrl: emailLog.redirectUrl,
        });
    }
    async validForgetToken(token) {
        const email = await this.emailService.getByToken(token);
        const app = await this.appRepo.findOne(email.appId);
        return new ForgetResult_1.ForgetResult({
            ...email,
            apiKey: app.apiKey,
        });
    }
};
__decorate([
    typeorm_1.Transaction(),
    __param(3, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, Body_1.RegisterBody,
        typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AuthService.prototype, "register", null);
__decorate([
    typeorm_1.Transaction(),
    __param(2, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, ValidBody_1.ForgetValidBody,
        typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AuthService.prototype, "sendForgetEmail", null);
__decorate([
    typeorm_1.Transaction(),
    __param(2, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, ResetBody_1.ForgetResetBody,
        typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], AuthService.prototype, "resetPassword", null);
AuthService = __decorate([
    di_1.Service(),
    __metadata("design:paramtypes", [UserRepo_1.UserRepo,
        UserSerivce_1.UserService,
        AppRepo_1.AppRepo,
        SMSService_1.SMSService,
        EmailService_1.EmailService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=AuthService.js.map