import {Description, Property, Required} from '@tsed/schema';

export class PepayAddBody {
  @Required()
  @Property()
  userId: number;

  @Required()
  @Property()
  orderItem: string;

  @Required()
  @Property()
  amount: number;

  @Required()
  @Property()
  @Description(
    '若pepay有回傳訊息時，則會再將相關的訊息回傳至這個Url, 回傳內容同查詢訂單的內容'
  )
  returnUrl: string;
}
