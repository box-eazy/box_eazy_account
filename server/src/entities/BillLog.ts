import {Column, Entity, ManyToOne} from 'typeorm';
import {User} from './User';
import {Application} from './Application';
import {CommonBase} from './Common/Base';
import {Description, Property} from '@tsed/schema';

@Entity()
export class BillLog extends CommonBase<BillLog> {
  @ManyToOne(() => User)
  @Property(User)
  user: User | null;

  @Column()
  @Property()
  userId: number;

  @Column({default: 0})
  @Property()
  amount: number;

  @Column()
  @Property()
  note: string;

  @ManyToOne(() => Application)
  app?: Application | null;

  @Column()
  @Property()
  appId: number;

  @Column()
  @Property()
  bankCode: string;

  @Column()
  @Property()
  bankAccount: string;

  @Column()
  @Property()
  isBill: boolean;
}
