import {Service} from '@tsed/di';
import axios from 'axios';
import {addMinutes, format} from 'date-fns';
import {SMSLog} from '../entities/SMSLog';
import {SMSRepo} from '../repositorys/SMSRepo';
import * as iconv from 'iconv-lite';
import * as qs from 'qs';
import {BaseError} from '../exceptions/BaseError';
import {UserRepo} from '../repositorys/UserRepo';
import {ValidTemp} from '../models/Valid/Temp';
import {EntityManager, Transaction, TransactionManager} from 'typeorm';
import {User} from '../entities/User';
import {BaseRepo} from '../repositorys/BaseRepo';
@Service()
export class SMSService {
  private id = '588';
  private password = '588mms588';
  private smsUrl = 'http://api.message.net.tw/send.php';
  constructor(private sms: SMSRepo, private user: UserRepo) {}
  private randomCode(len = 6) {
    let code = '';
    for (let i = 0; i < len; i++) {
      code = `${code}${Math.floor(Math.random() * 10)}`;
    }
    return code;
  }

  private async sendSMS(phone: string, content: string) {
    const res = await axios.request({
      url: `${this.smsUrl}`,
      params: {
        id: this.id,
        password: this.password,
        tel: phone,
        msg: content,
        mtype: 'G',
        encoding: 'utf8',
      },
    });
  }
  @Transaction()
  async sendSMSCode(
    appId: number,
    phone: string,
    temp: ValidTemp,
    @TransactionManager() manager?: EntityManager
  ) {
    const dbData = await manager.findOne(User, {
      phone: phone,
    });
    if (temp === ValidTemp.UPDATE_PROFILE || temp === ValidTemp.FOREGT) {
      if (dbData === undefined || dbData === null) {
        throw new BaseError(BaseError.Code.USER_NOT_FOUND);
      }
      await manager.save(dbData);
    } else if (temp === ValidTemp.REGISTER) {
      if (dbData !== undefined && dbData !== null) {
        throw new BaseError(BaseError.Code.USER_PHONE_IS_EXIST);
      }
    }

    const code = this.randomCode(6);
    const expired = addMinutes(new Date(), 10);
    const content = `您的驗證碼為：${code}，請在${format(
      expired,
      'yyyy-MM-dd HH:mm:ss'
    )}前使用`;

    try {
      await this.sendSMS(phone, content);
    } catch (err) {
      throw new BaseError(BaseError.Code.SMS_SEND_ERROR);
    }

    await manager.save(
      new SMSLog({
        appId,
        code,
        phone,
        expired,
        isUse: false,
        createdAt: new Date(),
        content,
        temp,
      })
    );
  }

  async $validSMSCode(phone: string, code: string, manager?: EntityManager) {
    const dbData = await manager.findOne(SMSLog, {
      phone,
      code,
    });

    console.log(dbData);

    if (dbData === undefined || dbData === null) {
      throw new BaseError(BaseError.Code.SMS_CODE_NOT_FOUND);
    }

    if (dbData.isUse) {
      throw new BaseError(BaseError.Code.SMS_CODE_IS_USE);
    }

    if (dbData.expired < new Date()) {
      throw new BaseError(BaseError.Code.SMS_CODE_EXPIRED);
    }

    dbData.isUse = true;

    await manager.save(dbData);
  }

  @Transaction()
  async validSMSCode(
    phone: string,
    code: string,
    @TransactionManager() manager?: EntityManager
  ) {
    return this.$validSMSCode(phone, code, manager);
  }
}
