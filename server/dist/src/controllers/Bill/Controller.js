"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const AuthApp_1 = require("../../decorators/AuthApp");
const BillLog_1 = require("../../entities/BillLog");
const BearerTokenAuth_1 = require("../../middlewares/BearerTokenAuth");
const AppAuthInfo_1 = require("../../models/AppAuthInfo");
const AddBody_1 = require("../../models/Bill/AddBody");
const DateRange_1 = require("../../models/Common/DateRange");
const Page_1 = require("../../models/Common/Page");
const BillService_1 = require("../../services/BillService");
const Response_1 = require("../../services/Response");
let BillController = class BillController {
    constructor(bill, res) {
        this.bill = bill;
        this.res = res;
    }
    findAll(dateRange, pageParams, userId) {
        return this.res.toPage(this.bill.findAll(dateRange, pageParams));
    }
    add(data, app) {
        return this.res.toResult(this.bill.create(app.appId, data));
    }
    edit(id, data) {
        return this.res.toResult(this.bill.edit(id, data));
    }
    delete(id) {
        return this.res.toResult(this.bill.delete(id));
    }
};
__decorate([
    schema_1.Summary('取得出帳列表'),
    common_1.Get(),
    schema_1.Returns(200, Response_1.ResponseService.toPagiation(BillLog_1.BillLog)),
    __param(0, common_1.QueryParams()),
    __param(1, common_1.QueryParams()),
    __param(2, common_1.QueryParams('userId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [DateRange_1.DateRangeParams,
        Page_1.PageParams, Number]),
    __metadata("design:returntype", void 0)
], BillController.prototype, "findAll", null);
__decorate([
    schema_1.Summary('新增出帳資料'),
    common_1.Post(),
    schema_1.Returns(200, Response_1.ResponseService.toResult(BillLog_1.BillLog)),
    __param(0, common_1.BodyParams()), __param(1, AuthApp_1.AuthApp()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [AddBody_1.BillAddBody, AppAuthInfo_1.AuthAppInfo]),
    __metadata("design:returntype", void 0)
], BillController.prototype, "add", null);
__decorate([
    schema_1.Summary('修改出帳資料(部份更新)'),
    common_1.Patch('/:id'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(BillLog_1.BillLog)),
    __param(0, common_1.PathParams('id')), __param(1, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, AddBody_1.BillAddBody]),
    __metadata("design:returntype", void 0)
], BillController.prototype, "edit", null);
__decorate([
    schema_1.Summary('刪除出帳資料'),
    common_1.Delete('/:id'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(BillLog_1.BillLog)),
    __param(0, common_1.PathParams('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], BillController.prototype, "delete", null);
BillController = __decorate([
    common_1.Controller('/billing'),
    schema_1.Description('出帳相關Api'),
    BearerTokenAuth_1.BearerTokenAuth(),
    __metadata("design:paramtypes", [BillService_1.BillService, Response_1.ResponseService])
], BillController);
exports.BillController = BillController;
//# sourceMappingURL=Controller.js.map