import {BodyParams, Controller, PathParams, Post} from '@tsed/common';
import {Description, Returns, Security, Summary, Tags} from '@tsed/schema';
import {AuthApp} from '../../decorators/AuthApp';
import {BearerTokenAuth} from '../../middlewares/BearerTokenAuth';
import {AuthAppInfo} from '../../models/AppAuthInfo';
import {BasicAuthBody, WebAuthBody} from '../../models/Auth/Basic';
import {BasicAuthInfo, WebAuthInfo} from '../../models/Auth/Info';
import {AuthService} from '../../services/AuthService';
import {ResponseService} from '../../services/Response';

@Controller('/auth')
@Description('使用者認證相關API')
export class AuthController {
  constructor(private auth: AuthService, private res: ResponseService) {}

  @BearerTokenAuth()
  @Summary('使用帳號及密碼取得userId')
  @Post()
  @Returns(200, ResponseService.toResult(BasicAuthInfo))
  basicAuth(@BodyParams() data: BasicAuthBody) {
    return this.res.toResult(
      this.auth.basicLogin(data.username, data.password)
    );
  }

  @BearerTokenAuth()
  @Summary('使用token取得使用者的userId')
  @Post('/:token')
  @Returns(200, ResponseService.toResult(BasicAuthInfo))
  tokenAuth(@PathParams('token') token: string, @AuthApp() app: AuthAppInfo) {
    return this.res.toResult(this.auth.tokenLogin(app.appId, token));
  }
}
