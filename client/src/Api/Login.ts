import {baseAxios} from './Base';

export const WebLogin = async (
  apiKey: string,
  username: string,
  password: string
) => {
  const res = await baseAxios.request<{result: {token: string}}>({
    url: '/web/auth',
    method: 'POST',
    data: {
      apiKey,
      username,
      password,
    },
  });

  return res.data.result.token;
};
