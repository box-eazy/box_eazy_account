"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilterController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const AuthApp_1 = require("../../decorators/AuthApp");
const FilterLog_1 = require("../../entities/FilterLog");
const BearerTokenAuth_1 = require("../../middlewares/BearerTokenAuth");
const AppAuthInfo_1 = require("../../models/AppAuthInfo");
const Page_1 = require("../../models/Common/Page");
const AddBody_1 = require("../../models/FilterLog/AddBody");
const FilterService_1 = require("../../services/FilterService");
const Response_1 = require("../../services/Response");
let FilterController = class FilterController {
    constructor(filter, res) {
        this.filter = filter;
        this.res = res;
    }
    findAll(page, phone) {
        return this.res.toPage(this.filter.findAll(phone, page));
    }
    create(data, app) {
        return this.res.toResult(this.filter.create(app.appId, data));
    }
    edit(id, data) {
        return this.res.toResult(this.filter.update(id, data));
    }
    delete(id) {
        return this.res.toResult(this.filter.delete(id));
    }
};
__decorate([
    common_1.Get(),
    schema_1.Summary('取得黑白名單列表'),
    schema_1.Returns(200, Response_1.ResponseService.toPagiation(FilterLog_1.FilterLog)),
    __param(0, common_1.QueryParams()),
    __param(1, common_1.QueryParams('phone')),
    __param(1, schema_1.Description('電話過濾，可以用來判斷電話是否在名單內')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Page_1.PageParams, String]),
    __metadata("design:returntype", void 0)
], FilterController.prototype, "findAll", null);
__decorate([
    common_1.Post(),
    schema_1.Summary('新增黑/白名單'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(FilterLog_1.FilterLog)),
    __param(0, common_1.BodyParams()), __param(1, AuthApp_1.AuthApp()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [AddBody_1.FilterLogAddBody, AppAuthInfo_1.AuthAppInfo]),
    __metadata("design:returntype", void 0)
], FilterController.prototype, "create", null);
__decorate([
    common_1.Put('/:id'),
    schema_1.Summary('修改黑/白名單'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(FilterLog_1.FilterLog)),
    __param(0, common_1.PathParams('id')), __param(1, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, AddBody_1.FilterLogAddBody]),
    __metadata("design:returntype", void 0)
], FilterController.prototype, "edit", null);
__decorate([
    common_1.Delete('/:id'),
    schema_1.Summary('刪除黑/白名單'),
    __param(0, common_1.PathParams('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], FilterController.prototype, "delete", null);
FilterController = __decorate([
    common_1.Controller('/filter'),
    schema_1.Description('黑白名單相關api'),
    BearerTokenAuth_1.BearerTokenAuth(),
    __metadata("design:paramtypes", [FilterService_1.FilterService, Response_1.ResponseService])
], FilterController);
exports.FilterController = FilterController;
//# sourceMappingURL=Controller.js.map