import {AxiosError} from 'axios';

const isAxiosError = (err: any): err is AxiosError =>
  err.response !== undefined;

export const getApiErrorMessage = (err: any) => {
  if (isAxiosError(err)) {
    if (err.response !== undefined) {
      switch (err.response.data.errCode) {
        case 40000:
          return '查詢不到對應的會員';
        case 40002:
          return '該電話已經註冊';
        case 40001:
          return '該電子信箱已經被註冊';
        case 40003:
          return '使用者帳號重覆';
        case 70000:
          return '簡訊發送失敗';
        case 70001:
          return '簡訊驗證碼已過期，請重新發送簡訊';
        case 70002:
          return '簡訊驗證碼已被使用，請重新發送簡訊';
        case 70003:
          return '簡訊驗證碼錯誤';
        case 70010:
          return '發送驗證信失敗';
        case 70011:
          return '重設密碼已過期，請重新發送信件';
        case 70012:
        case 70013:
          return '重設密碼失敗，請重新發送信件';

        default:
          return '未知的錯誤';
      }
    }
  } else {
    return err.message;
  }
};
