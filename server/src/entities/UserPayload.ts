import {Column, Entity, ManyToOne} from 'typeorm';
import {User} from './User';
import {Application} from './Application';
import {CommonBase} from './Common/Base';

@Entity()
export class UserPayload extends CommonBase {
  constructor(init?: Partial<UserPayload>) {
    super(init);
  }

  @ManyToOne(() => User)
  user?: User;

  @Column()
  userId: number;

  @ManyToOne(() => Application)
  app?: Application;

  @Column()
  appId: number;

  @Column()
  fieldName: string;

  @Column()
  fieldValue: string;
}
