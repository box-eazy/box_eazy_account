"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.notEmpty = exports.isEmpty = void 0;
exports.isEmpty = (value) => {
    return value !== null && value !== undefined;
};
exports.notEmpty = (value) => !exports.isEmpty(value);
//# sourceMappingURL=empty.js.map