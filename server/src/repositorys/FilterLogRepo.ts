import {EntityRepository} from 'typeorm';
import {FilterLog} from '../entities/FilterLog';
import {BaseRepo} from './BaseRepo';

@EntityRepository(FilterLog)
export class FilterLogRepo extends BaseRepo<FilterLog> {}
