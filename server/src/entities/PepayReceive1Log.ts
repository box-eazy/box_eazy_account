import {Property} from '@tsed/schema';
import {Column, Entity, JoinColumn, OneToOne} from 'typeorm';
import {CommonBase} from './Common/Base';
import {PepayLog} from './PepayLog';

@Entity()
export class PepayReceive1Log extends CommonBase<PepayReceive1Log> {
  @OneToOne(() => PepayLog)
  @JoinColumn()
  pepay: PepayLog;

  @Column()
  @Property()
  pepayId: number;

  @Column({nullable: true})
  @Property()
  sessId: string;

  @Column({nullable: true})
  @Property()
  orderId: string;

  @Column({nullable: true})
  @Property()
  shopId: string;

  @Column({nullable: true})
  @Property()
  prodId: string;

  @Column({nullable: true})
  @Property()
  userId: string;

  @Column({nullable: true})
  @Property()
  amount: number;

  @Column({nullable: true})
  @Property()
  currency: string;

  @Column({nullable: true})
  @Property()
  checkCode: string;

  @Column({nullable: true})
  @Property()
  shopPara: string;

  @Column({default: false})
  isReturn: boolean;
}
