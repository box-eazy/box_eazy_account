const dotenv = require('dotenv');
dotenv.config();
module.exports = {
    ROOT_DIR: process.cwd(),
    TEST: process.env.NODE_ENV === 'test',
    DEV: process.env.NODE_ENV !== 'production',
    DB_HOST: process.env.DB_HOST,
    DB_PORT: process.env.DB_PORT,
    DB_USERNAME: process.env.DB_USER,
    DB_PASSWORD: process.env.DB_PASS,
    DB_DATABASE: process.env.DB_DATABASE,
    REDIS_HOST: process.env.REDIS_HOST,
    REDIS_PORT: parseInt(process.env.REDIS_PORT),
    REDIS_PASSWORD: process.env.REDIS_PASSWORD,
    DATE_FORMATS: {
        DATE_TIME: 'yyyy-MM-dd HH:mm:ss',
    },
    PORT: process.env.PORT,
    EMAIL_ACCOUNT: process.env.EMAIL_ACCOUNT,
    EMAIL_PASSWORD: process.env.EMAIL_PASSWORD,
};
//# sourceMappingURL=envConfig.js.map