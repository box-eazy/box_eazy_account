"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PepayReceive1Log = void 0;
const schema_1 = require("@tsed/schema");
const typeorm_1 = require("typeorm");
const Base_1 = require("./Common/Base");
const PepayLog_1 = require("./PepayLog");
let PepayReceive1Log = class PepayReceive1Log extends Base_1.CommonBase {
};
__decorate([
    typeorm_1.OneToOne(() => PepayLog_1.PepayLog),
    typeorm_1.JoinColumn(),
    __metadata("design:type", PepayLog_1.PepayLog)
], PepayReceive1Log.prototype, "pepay", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayReceive1Log.prototype, "pepayId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Log.prototype, "sessId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Log.prototype, "orderId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Log.prototype, "shopId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Log.prototype, "prodId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Log.prototype, "userId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayReceive1Log.prototype, "amount", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Log.prototype, "currency", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Log.prototype, "checkCode", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Log.prototype, "shopPara", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], PepayReceive1Log.prototype, "isReturn", void 0);
PepayReceive1Log = __decorate([
    typeorm_1.Entity()
], PepayReceive1Log);
exports.PepayReceive1Log = PepayReceive1Log;
//# sourceMappingURL=PepayReceive1Log.js.map