import {EntityRepository} from 'typeorm';
import {Application} from '../entities/Application';
import {BaseRepo} from './BaseRepo';

@EntityRepository(Application)
export class AppRepo extends BaseRepo<Application> {}
