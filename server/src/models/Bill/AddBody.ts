import {Property} from '@tsed/schema';

export class BillAddBody {
  @Property()
  userId: number;

  @Property()
  amount: number;

  @Property()
  note: string;

  @Property()
  isBill: boolean;

  @Property()
  bankAccount: string;

  @Property()
  bankCode: string;
}
