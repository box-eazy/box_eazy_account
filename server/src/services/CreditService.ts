import {Service} from '@tsed/di';
import {Between, IsNull} from 'typeorm';
import {CreditLog} from '../entities/CreditLog';
import {DateRangeParams} from '../models/Common/DateRange';
import {PageParams} from '../models/Common/Page';
import {CreditAddBody} from '../models/Credit/AddBody';
import {CreditStats} from '../models/Credit/Stats';
import {CreditRepo} from '../repositorys/CreditRepo';

@Service()
export class CreditService {
  constructor(private creditRepo: CreditRepo) {}

  async findAll(dateRange: DateRangeParams, page: PageParams) {
    const [items, total] = await this.creditRepo.findAndCount({
      relations: ['pepay', 'user'],
      where: {
        ...DateRangeParams.toWhereBetween('createdAt', dateRange),
        deletedAt: IsNull(),
      },
      ...PageParams.toTypeOrmOptions(page),
    });

    return {
      items,
      total,
    };
  }

  async create(appId: number, data: CreditAddBody) {
    console.log(appId, data);
    let dbData = new CreditLog({
      ...data,
      appId,
      createdAt: new Date(),
    });
    dbData = await this.creditRepo.save(dbData);
    return await this.creditRepo.findOne(dbData.id, {
      relations: ['user', 'pepay'],
    });
  }

  async edit(id: number, data: CreditAddBody) {
    let dbData = await this.creditRepo.findOne(id, {
      relations: ['user', 'pepay'],
    });

    dbData = Object.assign(dbData, data);

    await this.creditRepo.save(dbData);

    return dbData;
  }

  async delete(id: number) {
    const dbData = await this.creditRepo.findOne(id, {
      relations: ['user', 'pepay'],
    });

    dbData.deletedAt = new Date();

    await this.creditRepo.save(dbData);

    return dbData;
  }

  async stats() {
    const result = await this.creditRepo.calcStats();
    console.log(result);
    return result;
  }
}
