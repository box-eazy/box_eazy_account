import {Brackets, EntityRepository, Like, In} from 'typeorm';
import {User} from '../entities/User';
import {UserPayload} from '../entities/UserPayload';
import {FilterParam} from '../models/Common/Filter';
import {PageParams, PageResult} from '../models/Common/Page';
import {UserListItem} from '../models/User/List';
import {UserSearchBody} from '../models/User/Search';
import {BaseRepo} from './BaseRepo';

@EntityRepository(User)
export class UserRepo extends BaseRepo<User> {
  async search(data: UserSearchBody): Promise<PageResult<UserListItem>> {
    const {keyword, page} = data;
    let qb = this.createQueryBuilder('user').where('user.deletedAt is null');

    qb = qb.leftJoinAndSelect('user.payloads', 'payload');
    qb = qb.leftJoinAndSelect('user.filters', 'filters');

    if (data.userIds && data.userIds.length > 0) {
      qb = qb.andWhere('user.id IN (:...userIds)', {userIds: data.userIds});
    }

    if (keyword !== undefined) {
      qb.andWhere(
        new Brackets(qb => {
          // qb.where(qb => {
          //   const sub = qb
          //     .subQuery()
          //     .from(UserPayload, 'sub_payload')
          //     .select('userId')
          //     .distinct(true)
          //     .where('user.id = sub_payload.userId')
          //     .andWhere('sub_payload.fieldValue LIKE :keyword')
          //     .getQuery();
          //   return `user.id IN ${sub}`;
          // });
          // qb.orWhere('user.email LIKE :keyword');
          qb.orWhere('user.phone LIKE :keyword');
          qb.orWhere('user.username LIKE :keyword');
          qb.orWhere('user.nickname LIKE :keyword');
          return qb;
        })
      );

      qb.setParameter('keyword', `%${keyword}%`);
    }

    if (data.filters !== undefined) {
      const {filters} = data;

      for (const filter of filters) {
        qb = qb.andWhere(qb => {
          const result = [];

          const processPayload = (filter: FilterParam) => {
            const key = filter.key.replace('payload.', '');
            let payloadQb: any = qb
              .subQuery()
              .from(UserPayload, 'filter_payload')
              .select('userId')
              .distinct();
            if (filter.value === undefined || filter.value === null) {
              const gruopConcatPayload = qb
                .subQuery()
                .from(UserPayload, 'p')
                .select('GROUP_CONCAT(p.fieldName)', 'fieldNames')
                .addSelect('p.userId', 'userId')
                .groupBy('userId');

              payloadQb = qb
                .subQuery()
                .from(`(${gruopConcatPayload.getQuery()})`, 'p')
                .select('userId')
                .distinct()
                .where(
                  `p.fieldNames ${filter.isNot ? 'NOT' : ''} LIKE '%${key}%'`
                );
            } else {
              payloadQb = payloadQb.where(
                `user.id = filter_payload.userId AND (filter_payload.fieldName='${key}' AND filter_payload.fieldValue='${filter.value}')`
              );
            }

            return `user.id IN ${payloadQb.getQuery()}`;
          };

          const processGeneral = (filter: FilterParam) => {
            if (filter.value) {
              return `${filter.isNot ? 'NOT' : ''} user.${filter.key} = ${
                filter.value
              }`;
            } else {
              return `user.${filter.key} IS ${filter.isNot ? '' : 'NOT'} NULL`;
            }
          };

          if (Array.isArray(filter)) {
            const orList = [];
            for (const subFilter of filter) {
              if (subFilter.key.includes('payload.')) {
                orList.push(processPayload(subFilter));
              } else {
                orList.push(processGeneral(subFilter));
              }
            }
            if (orList.length > 0) {
              result.push(orList.join(' OR '));
            }
          } else {
            if (filter.key.includes('payload.')) {
              result.push(processPayload(filter));
            } else {
              result.push(processGeneral(filter));
            }
          }

          const text = result.map(x => `(${x})`).join('AND');
          return text;
        });
      }
    }

    if (
      page !== undefined &&
      page.pageSize !== undefined &&
      page.pageIndex !== undefined
    ) {
      qb = qb.skip(page.pageIndex * page.pageSize);
      qb = qb.take(page.pageSize);
    }

    if (data.orderBy !== undefined) {
      if (data.orderBy.key.includes('payload.')) {
        qb = qb.addSelect(
          sqb =>
            sqb
              .subQuery()
              .from(UserPayload, 'orderPayload')
              .select('fieldValue')
              .where(
                'fieldName=:orderPayloadKey AND orderPayload.userId = user.id',
                {orderPayloadKey: data.orderBy.key.replace('payload.', '')}
              ),
          'payloadOrderBy'
        );
        qb.orderBy({
          payloadOrderBy: data.orderBy.by,
        });
      } else {
        qb = qb.orderBy({
          [`user.${data.orderBy.key}`]: data.orderBy.by,
        });
      }
    }

    const [items, total] = await qb.getManyAndCount();

    return {
      items: items.map(x => new UserListItem(x)),
      total,
    };
  }
}
