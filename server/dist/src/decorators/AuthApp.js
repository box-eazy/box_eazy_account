"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthApp = exports.Pipe = void 0;
const common_1 = require("@tsed/common");
const core_1 = require("@tsed/core");
const AppService_1 = require("../services/AppService");
let Pipe = class Pipe {
    async transform(req) {
        return this.app.decodeToken(req.token);
    }
};
__decorate([
    common_1.Inject(),
    __metadata("design:type", AppService_1.AppService)
], Pipe.prototype, "app", void 0);
Pipe = __decorate([
    common_1.Injectable()
], Pipe);
exports.Pipe = Pipe;
function AuthApp() {
    return core_1.useDecorators(common_1.Req(), common_1.UsePipe(Pipe));
}
exports.AuthApp = AuthApp;
//# sourceMappingURL=AuthApp.js.map