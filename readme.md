# 既有平台需要調整的程式
* 必要
    * 登入相關程式 (參考以下登入流程以及 /auth & /users 相關API)
    * 取得/更新會員資料等程式 (參考user payload, 以及 /users API)
    * 金流相關程式 (參考 /pepay 相關 API)
* 非必要
    * 簡訊驗證 & Email 驗證 (由於user.isSmsValid & isEmailValid都是透過 /users 進行控制，所以這邊不一定要調整)

# Eazy 會員整合平台簡易說明

## 網頁登入流程(For 黃頁、Eazy Dating)
![image](./web-login-flow.png)

## APP 登入流程 (For CallBible)
![image](./app-login-flow.png)

## 登入方式

> 連結到登入頁面 http://13.113.142.165:30011 (畫面為暫時測試用), 並且帶入 api_key 以及 redirect_url 在 querystring上，若是登入成功則會將對應的token回傳至redirect_url

> 若透過網頁進行登入，若登入成功時會將登入狀態記錄在session之中，下次使用者進入時，則會略過登入頁直接redirect到指定連結

> 若要進行登入的話，則需將使用者指向至 /logout 的連結中，登出成功後將轉向redirect_url

```
http://13.113.142.165:30011?api_key=[api_key]&redirect_url=[redirect_url]

登入成功後指向到
[redirect_url]?token=[token]
```



## Api Token 產生方式

> 可利用jsonwebtoken來產生jwt token 

> exp 為該 token 的有效期限，建立不要設的太長，每次發送請求時產生新的token會比較保險

```javascript
    jwt.sign(
      {
        apiKey,
        appId,
        exp: new Date().getTime() + 60000,
      },
      apiSecert,
      {
        algorithm: 'HS256',
      }
    )
```


## Api Token 使用方式

> 在發送 Api 請求前，將token帶入headers 的 Authorization, 格式可參考下方

```
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGlLZXkiOiJYZkoxcHF0VlV5UDQiLCJhcHBJZCI6MSwiZXhwIjoxNjE0ODUwNjIwOTc3LCJpYXQiOjE2MTQ3NjQyMjB9.sv9c4UjHZjheRmJPzwb-nltPGQ_anv01qMfac2u_w3c
```


## Api 錯誤訊息

> 可參考 swagger 文件


## User Payload

> 若是需要擴充 user 現有的欄位，可以將相關資料寫入 payloay 之中，可以參考 PUT: /api/users/{id}/payload/{fieldName}



## Swagger 登入方式

> 若 API 上面帶有鎖的符號時，則表示該Api需要token才可使用，可透過 Swagger UI右上方 Authorize 按鈕，並輸入 token來進行驗證