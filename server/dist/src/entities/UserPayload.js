"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPayload = void 0;
const typeorm_1 = require("typeorm");
const User_1 = require("./User");
const Application_1 = require("./Application");
const Base_1 = require("./Common/Base");
let UserPayload = class UserPayload extends Base_1.CommonBase {
    constructor(init) {
        super(init);
    }
};
__decorate([
    typeorm_1.ManyToOne(() => User_1.User),
    __metadata("design:type", User_1.User)
], UserPayload.prototype, "user", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], UserPayload.prototype, "userId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => Application_1.Application),
    __metadata("design:type", Application_1.Application)
], UserPayload.prototype, "app", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], UserPayload.prototype, "appId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], UserPayload.prototype, "fieldName", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], UserPayload.prototype, "fieldValue", void 0);
UserPayload = __decorate([
    typeorm_1.Entity(),
    __metadata("design:paramtypes", [Object])
], UserPayload);
exports.UserPayload = UserPayload;
//# sourceMappingURL=UserPayload.js.map