import {CollectionOf, Enum, Property} from '@tsed/schema';
import {FilterLog} from '../../entities/FilterLog';
import {BaseModel} from '../Common/BaseModel';
import {FilterLogType} from '../FilterLog/AddBody';
import {UserPayload} from './Payload';
import {RegisterProvider} from './RegisterProvider';

export class UserListItem extends BaseModel<UserListItem> {
  constructor(init?: Partial<UserListItem>) {
    super(init);
    if (init.payloads !== undefined) {
      this.payloadObj = this.payloads.reduce((curr, item) => {
        return {
          ...curr,
          [item.fieldName]: item.fieldValue,
        };
      }, {});
    }
  }

  @Property()
  id: number;

  @Property()
  username: string;

  @Property()
  facebookId: string | null;

  @Property()
  googleId: string | null;

  @Property()
  phone: string;

  @Property()
  email: string;

  @Property()
  nickname: string;

  @Property()
  isEmailValid: boolean;

  @Property()
  isSmsValid: boolean;

  @Property()
  image: string | null;

  @CollectionOf(UserPayload)
  payloads: UserPayload[];

  @Property()
  payloadObj: any;

  @CollectionOf(FilterLog)
  filters: FilterLog[];

  @Property()
  @Enum(RegisterProvider)
  provider: RegisterProvider;
}
