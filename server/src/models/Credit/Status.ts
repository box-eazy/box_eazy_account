import {Description} from '@tsed/schema';

export enum CreditStatus {
  CREATED = 'CREATED',
  WAITING = 'WATING',
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
}
