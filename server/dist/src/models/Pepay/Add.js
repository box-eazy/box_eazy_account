"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PepayAddBody = void 0;
const schema_1 = require("@tsed/schema");
class PepayAddBody {
}
__decorate([
    schema_1.Required(),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayAddBody.prototype, "userId", void 0);
__decorate([
    schema_1.Required(),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayAddBody.prototype, "orderItem", void 0);
__decorate([
    schema_1.Required(),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayAddBody.prototype, "amount", void 0);
__decorate([
    schema_1.Required(),
    schema_1.Property(),
    schema_1.Description('若pepay有回傳訊息時，則會再將相關的訊息回傳至這個Url, 回傳內容同查詢訂單的內容'),
    __metadata("design:type", String)
], PepayAddBody.prototype, "returnUrl", void 0);
exports.PepayAddBody = PepayAddBody;
//# sourceMappingURL=Add.js.map