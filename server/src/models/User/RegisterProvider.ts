export enum RegisterProvider {
  GENERAL = 'GENERAL',
  FACEBOOK = 'FACEBOOK',
  GOOGLE = 'GOOGLE',
}
