"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const orderId_1 = require("./orderId");
const date_fns_1 = require("date-fns");
const generate = new orderId_1.GenerateOrderId('EAZY', 'yyyyMMdd', 10);
const mockId = `EAZY${date_fns_1.format(new Date(), 'yyyyMMdd')}0000033456`;
describe('generator orderId', () => {
    it('generateOrder', () => {
        expect(generate.generateOrderId(33456)).toBe(mockId);
    });
    it('getOrderNum', () => {
        expect(generate.getOrderNum(mockId)).toBe(33456);
    });
});
//# sourceMappingURL=orderId.spec.js.map