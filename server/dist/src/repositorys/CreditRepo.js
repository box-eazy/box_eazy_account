"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreditRepo = void 0;
const typeorm_1 = require("typeorm");
const Application_1 = require("../entities/Application");
const CreditLog_1 = require("../entities/CreditLog");
const BaseRepo_1 = require("./BaseRepo");
const dateFns = require("date-fns");
const Stats_1 = require("../models/Credit/Stats");
const _ = require("lodash");
let CreditRepo = class CreditRepo extends BaseRepo_1.BaseRepo {
    async calcStats() {
        const today = dateFns.format(new Date(), 'yyyy-MM-dd');
        const qb = this.createQueryBuilder('credit').innerJoin(Application_1.Application, 'app', 'app.id = credit.appId AND isCreditStats = true');
        qb.select('credit.appId', 'appId');
        qb.select('app.name', 'appName');
        qb.addSelect('SUM(credit.amount)', 'totalSum');
        qb.addSelect(`SUM(IF(credit.createdAt BETWEEN '${today} 00:00:00' AND '${today} 23:59:59', credit.amount, 0))`, 'toDaySum');
        qb.groupBy('credit.appId');
        const result = await qb.getRawMany();
        const parsedResult = result.map(x => ({
            ...x,
            toDaySum: parseInt(x.toDaySum),
            totalSum: parseInt(x.totalSum),
        }));
        return new Stats_1.CreditStats({
            toDaySum: _.sumBy(parsedResult, 'toDaySum'),
            totalSum: _.sumBy(parsedResult, 'totalSum'),
            sumByApp: parsedResult,
        });
    }
};
CreditRepo = __decorate([
    typeorm_1.EntityRepository(CreditLog_1.CreditLog)
], CreditRepo);
exports.CreditRepo = CreditRepo;
//# sourceMappingURL=CreditRepo.js.map