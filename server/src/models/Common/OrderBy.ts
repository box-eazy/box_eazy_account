import {Property} from '@tsed/schema';

export class OrderByParam {
  @Property()
  key: string;

  @Property()
  by: 'DESC' | 'ASC';
}
