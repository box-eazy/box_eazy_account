"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseModel = void 0;
class BaseModel {
    constructor(init) {
        Object.assign(this, init);
    }
}
exports.BaseModel = BaseModel;
//# sourceMappingURL=BaseModel.js.map