import {IsNull} from 'typeorm';
import {BillLog} from '../entities/BillLog';
import {BillAddBody} from '../models/Bill/AddBody';
import {DateRangeParams} from '../models/Common/DateRange';
import {PageParams} from '../models/Common/Page';
import {BillRepo} from '../repositorys/BillRepo';

export class BillService {
  constructor(private bill: BillRepo) {}

  async findAll(date: DateRangeParams, page: PageParams) {
    const [items, total] = await this.bill.findAndCount({
      relations: ['user'],
      where: {
        deletedAt: IsNull(),
        ...DateRangeParams.toWhereBetween('createdAt', date),
      },
      ...PageParams.toTypeOrmOptions(page),
    });

    return {
      items,
      total,
    };
  }

  async create(appId: number, data: BillAddBody) {
    let dbData = new BillLog({
      ...data,
      appId,
      createdAt: new Date(),
    });

    dbData = await this.bill.save(dbData);

    return dbData;
  }

  async edit(id: number, data: BillAddBody) {
    let dbData = await this.bill.findOne(id, {
      relations: ['user'],
    });

    dbData = Object.assign(dbData, data);
    dbData.updatedAt = new Date();

    return dbData;
  }

  async delete(id: number) {
    const dbData = await this.bill.findOne(id, {
      relations: ['user'],
    });

    dbData.deletedAt = new Date();

    return dbData;
  }
}
