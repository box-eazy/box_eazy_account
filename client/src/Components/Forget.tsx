import React, { ChangeEvent, useCallback, useState } from 'react';
import EazyCommon from '@eazy/widget';
import { ForgetApi } from '../Api/Forget';
import { useQueryString } from '../hooks/useQueryString';
import { useModal } from '@eazy/widget/Components/Modal/Context';
import { getApiErrorMessage } from '../libs/handleError';
import { SendMailResult } from './SendMailResult';
import { SendSMSButton } from './SendSMSButton';
import { getCurrentUrl } from '../libs/getCurrentUrl';

export const EazyForget: React.FC = () => {
  const {
    api_key: apiKey,
    app_id: appId,
    redirect_url: redirectUrl,
  } = useQueryString<{ api_key: string; app_id: string; redirect_url: string }>();
  const [data, setData] = useState({
    username: '',
    email: '',
    phone: '',
    smsCode: '',
    validUrl: getCurrentUrl() + '/forget',
    redirectUrl,
  });

  const modal = useModal();
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  const $onChange = useCallback(
    (key: keyof typeof data) => (e: ChangeEvent<HTMLInputElement>) => {
      setData(prev => ({
        ...prev,
        [key]: e.target.value,
      }));
    },
    []
  );

  const $onSubmit = useCallback(async () => {
    setLoading(true);
    try {
      await ForgetApi(apiKey, data);
      setSuccess(true);
    } catch (err) {
      await modal.alert(getApiErrorMessage(err));
    }
    setLoading(false);
  }, [data, apiKey]);

  if (success) {
    return (
      <SendMailResult
        title="忘記密碼"
        email={data.email}
        type="FORGET"
        redirectUrl={redirectUrl}
        validUrl="/forget"
        apiKey={apiKey}
      >
        <div className="text-center">
          <div>您的重設密碼請求已送出</div>
          <div>請至Email信箱收取驗證連結</div>
          <div>並重設密碼</div>
        </div>
      </SendMailResult>
    );
  }

  return (
    <EazyCommon.Layout.Wrapper
      headerTitle="忘記密碼"
      back
      footer={
        <EazyCommon.Box p={'1rem'}>
          <EazyCommon.Button
            block
            size="lg"
            color="primary"
            type="submit"
            form="EazyForgetForm"
          >
            送出，發送Email驗證信
          </EazyCommon.Button>
        </EazyCommon.Box>
      }
    >
      <EazyCommon.Form.Wrapper onSubmit={$onSubmit} id="EazyForgetForm">
        <EazyCommon.Form.FormItem title="會員帳號">
          <EazyCommon.Controls.Input
            placeholder="輸入會員帳號"
            value={data.username}
            onChange={$onChange('username')}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="Email">
          <EazyCommon.Controls.Input
            placeholder="輸入Email"
            value={data.email}
            onChange={$onChange('email')}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem
          title="手機號碼"
          suffix={
            <SendSMSButton phone={data.phone} apiKey={apiKey} type="FORGET" />
          }
        >
          <EazyCommon.Controls.Input
            placeholder="輸入手機號碼"
            value={data.phone}
            onChange={$onChange('phone')}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="簡訊驗證碼">
          <EazyCommon.Controls.Input
            placeholder="輸入簡訊驗證碼"
            value={data.smsCode}
            onChange={$onChange('smsCode')}
            required
          />
        </EazyCommon.Form.FormItem>
      </EazyCommon.Form.Wrapper>
    </EazyCommon.Layout.Wrapper>
  );
};
