import EazyCommon from '@eazy/widget';
import React, {useCallback} from 'react';
import {SendEmailApi} from '../Api/SendEmailApi';
import {useQueryString} from '../hooks/useQueryString';
import {getCurrentUrl} from '../libs/getCurrentUrl';
import {getApiErrorMessage} from '../libs/handleError';

export type SendMailResultProps = {
  title: React.ReactNode;
  email: string;
  type: any;
  validUrl: string;
  apiKey: string;
  redirectUrl: string;
};
export const SendMailResult: React.FC<SendMailResultProps> = ({
  title,
  email,
  type,
  validUrl,
  children,
  apiKey,
  redirectUrl,
}) => {
  const history = EazyCommon.ReactRouterDom.useHistory();
  const modal = EazyCommon.ModalUtils.useModal();
  const $resendEmail = useCallback(async () => {
    try {
      await SendEmailApi(
        apiKey,
        email,
        `${getCurrentUrl()}/${validUrl}`,
        redirectUrl,
        type
      );
      await modal.alert('重新寄送驗證信成功，麻煩請確認驗證信');
    } catch (err) {
      await modal.alert(getApiErrorMessage(err));
    }
  }, [email, type]);

  return (
    <EazyCommon.Layout.Wrapper
      back
      headerTitle={title}
      footer={
        <EazyCommon.Box p={12}>
          <EazyCommon.Button
            onClick={() => history.push(`/${window.location.search}`)}
            size="lg"
            color="primary"
            block
          >
            回到登入頁
          </EazyCommon.Button>
        </EazyCommon.Box>
      }
    >
      <EazyCommon.Box bg="gray.2" p={12} m={12}>
        {children}
      </EazyCommon.Box>
      <EazyCommon.Box textAlign="center" m={24}>
        沒有收到Email驗證信嗎？
      </EazyCommon.Box>
      <EazyCommon.Box m={12}>
        <EazyCommon.Button
          outline
          color="primary"
          block
          size="lg"
          onClick={$resendEmail}
        >
          重新發送Email驗證信
        </EazyCommon.Button>
      </EazyCommon.Box>
    </EazyCommon.Layout.Wrapper>
  );
};
