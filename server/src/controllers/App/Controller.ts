import {BodyParams, Controller, Get, Post} from '@tsed/common';
import {Returns, Summary} from '@tsed/schema';
import {Application} from '../../entities/Application';
import {AppService} from '../../services/AppService';
import {ResponseService} from '../../services/Response';

@Controller('/apps')
export class AppController {
  constructor(private app: AppService, private res: ResponseService) {}

  @Post()
  @Summary('Create App')
  @Returns(200, ResponseService.toResult(Application))
  async create(@BodyParams('name') name: string) {
    return this.res.toResult(this.app.create(name));
  }

  @Get()
  @Summary('FindAll')
  findAll() {
    return this.res.toItems(this.app.findAll());
  }
}
