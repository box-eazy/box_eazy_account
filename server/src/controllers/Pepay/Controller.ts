import {BodyParams, Controller, Get, Post, QueryParams} from '@tsed/common';
import {Description, Required, Returns, Security, Summary} from '@tsed/schema';
import {AuthApp} from '../../decorators/AuthApp';
import {PepayLog} from '../../entities/PepayLog';
import {BaseError} from '../../exceptions/BaseError';
import {BearerTokenAuth} from '../../middlewares/BearerTokenAuth';
import {AuthAppInfo} from '../../models/AppAuthInfo';
import {DateRangeParams} from '../../models/Common/DateRange';
import {PageParams} from '../../models/Common/Page';
import {PepayAddBody} from '../../models/Pepay/Add';
import {PepayPostForm} from '../../models/Pepay/PostForm';
import {PepayReceive1Body} from '../../models/Pepay/Receive1';
import {PepayReceive2Body} from '../../models/Pepay/Receive2';
import {PepayService} from '../../services/PepayService';
import {ResponseService} from '../../services/Response';

@Controller('/pepay')
@Description('Pepay金流相關API')
export class PepayController {
  constructor(private pepay: PepayService, private res: ResponseService) {}

  @Get()
  @Summary('取得PepayList')
  @Returns(200, ResponseService.toPagiation(PepayLog))
  @BearerTokenAuth()
  findAll(
    @QueryParams() page: PageParams,
    @QueryParams() dateRange: DateRangeParams
  ) {
    return this.res.toPage(this.pepay.findAll(page, dateRange));
  }

  @Summary('建立Pepay訂單')
  @Post()
  @Returns(200, ResponseService.toResult(PepayPostForm))
  @BearerTokenAuth()
  async create(@BodyParams() data: PepayAddBody, @AuthApp() app: AuthAppInfo) {
    return this.res.toResult(this.pepay.create(app.appId, data));
  }

  @Summary('取得訂單資料 & 狀態')
  @Get('/:orderId')
  @Returns(200, ResponseService.toResult(PepayLog))
  @BearerTokenAuth()
  findOne() {}

  @Post('/:orderId/receive1')
  @Returns(200, String)
  async receive1(@BodyParams() data: PepayReceive1Body) {
    const res = await this.pepay.createReceive1(data);
    return this.pepay.toOutputString(res);
  }

  @Get('/:orderId/receive2')
  async receive2(@QueryParams() data: PepayReceive2Body) {
    try {
      const res = await this.pepay.createReceive2(data);
      return 'RES_CODE=0';
    } catch (err) {
      console.log('receive2 error', err);
      if (err instanceof BaseError) {
        if (err.errCode === BaseError.Code.PEPAY_RECEIVE02_IS_EXIST) {
          return 'RES_CODE=20290';
        }
      }
      return 'RES_CODE=20000';
    }
  }
}
