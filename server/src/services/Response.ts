import {Service} from '@tsed/di';
import {CollectionOf, Property} from '@tsed/schema';
import {ErrorCode} from '../models/Error';
import {BaseRes, ItemsRes, PageRes, ResultRes} from '../models/Res';

@Service()
export class ResponseService {
  static Base = BaseRes;
  static toPagiation<T extends Object>(instance: T) {
    class Cls extends BaseRes {
      @CollectionOf(instance)
      items: T[];

      @Property()
      total: number;
    }
    Object.defineProperty(Cls, 'name', {
      value: `${(instance as any).name}PageItems`,
    });
    return Cls;
  }
  static toItems<T extends Object>(instance: T) {
    class Cls extends BaseRes {
      @CollectionOf(instance)
      items: T[];
    }
    Object.defineProperty(Cls, 'name', {
      value: `${(instance as any).name}Items`,
    });
    return Cls;
  }

  static toResult<T extends Object>(instance: T) {
    class Cls extends BaseRes {
      @Property(instance)
      result: T;
    }
    Object.defineProperty(Cls, 'name', {
      value: `${(instance as any).name}Result`,
    });
    return Cls;
  }

  async toResult<T>(wait: Promise<T> | T): Promise<ResultRes<T>> {
    return {
      result: await wait,
      errCode: ErrorCode.SUCCESS,
    };
  }
  async toItems<T>(wait: Promise<T[]>): Promise<ItemsRes<T>> {
    return {
      items: await wait,
      errCode: ErrorCode.SUCCESS,
    };
  }
  async toPage<T>(
    wait: Promise<{items: T[]; total: number}>
  ): Promise<PageRes<T>> {
    const result = {
      ...(await wait),
      errCode: ErrorCode.SUCCESS,
    };
    console.log(result);
    return result;
  }

  async Ok(): Promise<BaseRes> {
    return {
      errCode: ErrorCode.SUCCESS,
    };
  }

  async Error(code: ErrorCode, message?: string): Promise<BaseRes> {
    return {
      errCode: code,
      errMessage: message,
    };
  }
}
