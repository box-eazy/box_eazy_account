import {Property} from '@tsed/schema';
import {BillLog} from '../../entities/BillLog';

export class BillListItem {
  @Property()
  userId: number;

  @Property()
  appId: number;

  @Property()
  amount: number;

  @Property()
  note: string;

  @Property()
  createdAt: string;
}
