import {Provider, registerProvider} from '@tsed/di';
import {Description, Enum, Property} from '@tsed/schema';
import {RegisterProvider} from '../User/RegisterProvider';

export class RegisterBody {
  @Property()
  apiKey: string;

  @Property()
  nickname: string;

  @Property()
  username: string;

  @Property()
  password: string;

  @Property()
  confirmPassword: string;

  @Property()
  email: string;

  @Property()
  phone: string;

  @Property()
  smsCode: string;

  @Property()
  @Enum(RegisterProvider)
  @Description('註冊方式')
  provider: RegisterProvider;
}
