import {Service} from '@tsed/di';
import {create, update} from 'lodash';
import {FilterLog} from '../entities/FilterLog';
import {BaseError} from '../exceptions/BaseError';
import {PageParams} from '../models/Common/Page';
import {FilterLogAddBody} from '../models/FilterLog/AddBody';
import {FilterLogRepo} from '../repositorys/FilterLogRepo';
import {BaseService} from './BaseSerivce';

@Service()
export class FilterService extends BaseService {
  constructor(private filterLog: FilterLogRepo) {
    super();
  }
  async findAll(phone: string, page: PageParams) {
    const [items, total] = await this.filterLog.findAndCount({
      where: {
        phone,
      },
      ...PageParams.toTypeOrmOptions(page),
    });
    return {
      items,
      total,
    };
  }

  async findOne(id: number) {
    return this.filterLog.findOne({id});
  }

  async update(id: number, data: FilterLogAddBody) {
    if (!(await this.filterLog.exist({id}))) {
      throw new BaseError(BaseError.Code.FILTER_NOT_FOUND);
    }
    await this.filterLog.update(
      {
        id,
      },
      {
        ...data,
        updatedAt: new Date(),
      }
    );

    return this.filterLog.findOne(id);
  }

  async create(appId: number, data: FilterLogAddBody) {
    if (await this.filterLog.exist({phone: data.phone})) {
      throw new BaseError(BaseError.Code.FILTER_PHONE_IS_EXIST);
    }

    return await this.filterLog.save({
      ...data,
      appId,
      createdAt: new Date(),
    });
  }

  async delete(id: number) {
    if (!(await this.filterLog.exist({id}))) {
      throw new BaseError(BaseError.Code.FILTER_NOT_FOUND);
    }
    const dbData = await this.filterLog.findOne(id);
    await this.filterLog.delete({
      id,
    });

    return dbData;
  }
}
