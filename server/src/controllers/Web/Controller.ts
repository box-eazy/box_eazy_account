import {
  BodyParams,
  Controller,
  Get,
  PathParams,
  Post,
  Session,
} from '@tsed/common';
import {Description, Returns, string, Summary, Tags} from '@tsed/schema';
import {EmailLog} from '../../entities/EmailLog';
import {WebAuthBody} from '../../models/Auth/Basic';
import {ForgetResult} from '../../models/Auth/ForgetResult';
import {WebAuthInfo} from '../../models/Auth/Info';
import {ForgetResetBody} from '../../models/Forget/ResetBody';
import {ForgetValidBody} from '../../models/Forget/ValidBody';
import {RegisterBody} from '../../models/Register/Body';
import {ValidTemp} from '../../models/Valid/Temp';
import {AppService} from '../../services/AppService';
import {AuthService} from '../../services/AuthService';
import {EmailService} from '../../services/EmailService';
import {ResponseService} from '../../services/Response';
import {SMSService} from '../../services/SMSService';
import {UserService} from '../../services/UserSerivce';

@Controller('/web')
@Tags('Web')
@Description('整合系統網頁用Api')
export class WebController {
  constructor(
    private user: UserService,
    private res: ResponseService,
    private sms: SMSService,
    private auth: AuthService,
    private app: AppService,
    private email: EmailService
  ) {}

  @Post('/auth')
  @Returns(200, ResponseService.toResult(WebAuthInfo))
  @Summary('會員登入')
  async login(@BodyParams() data: WebAuthBody, @Session() session) {
    const res = await this.auth.webLogin(
      data.apiKey,
      data.username,
      data.password
    );
    session.user = {
      id: res.userId,
    };
    return this.res.toResult(res);
  }

  @Post('/auth/provider')
  @Returns(200, ResponseService.toResult(WebAuthInfo))
  @Summary('FB / Google登入')
  async loginByParams(
    @BodyParams('apiKey') apiKey: string,
    @BodyParams('provider') provider: string,
    @BodyParams('email') email: string,
    @Session() session
  ) {
    const res = await this.auth.webProviderLogin(apiKey, provider, email);
    session.user = {
      id: res.userId,
    };
    return this.res.toResult(res);
  }

  @Post('/register')
  @Summary('會員註冊')
  async register(@BodyParams() data: RegisterBody) {
    const app = await this.app.getAppByApiKey(data.apiKey);
    return this.res.toResult(this.auth.register(app.id, app.apiKey, data));
  }

  @Post('/forget')
  @Summary('忘記密碼')
  @Returns(200, ResponseService.Base)
  async forgetValidData(@BodyParams() data: ForgetValidBody) {
    const app = await this.app.getAppByApiKey(data.apiKey);
    await this.auth.sendForgetEmail(app.id, data);
    return this.res.Ok();
  }

  @Post('/forget/:token')
  @Summary('reset password')
  @Returns(200, ResponseService.toResult(ForgetResult))
  forgetResetPassword(
    @BodyParams() data: ForgetResetBody,
    @PathParams('token') token: string
  ) {
    return this.res.toResult(this.auth.resetPassword(token, data));
  }

  @Get('/forget/:token')
  @Summary('valid token')
  @Returns(200, ResponseService.toResult(ForgetResult))
  validForgetToken(@PathParams('token') token: string) {
    return this.res.toResult(this.auth.validForgetToken(token));
  }

  @Post('/sms')
  @Summary('Send SMS')
  @Returns(200, ResponseService.Base)
  async sendSMS(
    @BodyParams('apiKey') apiKey: string,
    @BodyParams('phone') phone: string,
    @BodyParams('type') type: ValidTemp
  ) {
    const app = await this.app.getAppByApiKey(apiKey);
    await this.sms.sendSMSCode(app.id, phone, type);
    return this.res.Ok();
  }

  @Post('/email')
  @Summary('Send Email')
  async sendEmail(
    @BodyParams('apiKey') apiKey: string,
    @BodyParams('email') email: string,
    @BodyParams('type') type: ValidTemp,
    @BodyParams('redirectUrl') redirectUrl: string,
    @BodyParams('validUrl') validUrl: string
  ) {
    const app = await this.app.getAppByApiKey(apiKey);
    await this.email.sendValidEmail(app.id, {
      email,
      temp: type,
      redirectUrl,
      validUrl,
    });
    return this.res.Ok();
  }

  @Post('/email/:token')
  @Returns(200, ResponseService.toResult(EmailLog))
  async validToken(@PathParams('token') token: string) {
    return this.res.toResult(this.email.validToken(token));
  }
}
