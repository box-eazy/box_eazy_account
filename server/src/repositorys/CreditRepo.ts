import {EntityRepository} from 'typeorm';
import {Application} from '../entities/Application';
import {CreditLog} from '../entities/CreditLog';
import {BaseRepo} from './BaseRepo';
import * as dateFns from 'date-fns';
import {CreditStats} from '../models/Credit/Stats';
import * as _ from 'lodash';

@EntityRepository(CreditLog)
export class CreditRepo extends BaseRepo<CreditLog> {
  async calcStats() {
    const today = dateFns.format(new Date(), 'yyyy-MM-dd');
    const qb = this.createQueryBuilder('credit').innerJoin(
      Application,
      'app',
      'app.id = credit.appId AND isCreditStats = true'
    );

    qb.select('credit.appId', 'appId');
    qb.select('app.name', 'appName');
    qb.addSelect('SUM(credit.amount)', 'totalSum');
    qb.addSelect(
      `SUM(IF(credit.createdAt BETWEEN '${today} 00:00:00' AND '${today} 23:59:59', credit.amount, 0))`,
      'toDaySum'
    );

    qb.groupBy('credit.appId');
    const result = await qb.getRawMany<{
      appId: number;
      appName: string;
      toDaySum: string;
      totalSum: string;
    }>();

    const parsedResult = result.map(x => ({
      ...x,
      toDaySum: parseInt(x.toDaySum),
      totalSum: parseInt(x.totalSum),
    }));

    return new CreditStats({
      toDaySum: _.sumBy(parsedResult, 'toDaySum'),
      totalSum: _.sumBy(parsedResult, 'totalSum'),
      sumByApp: parsedResult,
    });
  }
}
