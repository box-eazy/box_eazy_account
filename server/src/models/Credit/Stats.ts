import {CollectionOf, Property} from '@tsed/schema';
import {BaseModel} from '../Common/BaseModel';

export class CreditAppStats extends BaseModel<CreditAppStats> {
  @Property()
  appName: string;
  @Property()
  toDaySum: number;
  @Property()
  totalSum: number;
}

export class CreditStats extends BaseModel<CreditStats> {
  @Property()
  toDaySum: number;
  @Property()
  totalSum: number;

  @CollectionOf(CreditAppStats)
  sumByApp: CreditAppStats[];
}
