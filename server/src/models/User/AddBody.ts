import {CollectionOf, Enum, Format, Property} from '@tsed/schema';
import {UserPayload} from './Payload';
import {RegisterProvider} from './RegisterProvider';

export class UserAddBody {
  @Property()
  username: string;

  @Property()
  password: string;

  @Property()
  phone: string;

  @Property()
  @Format('email')
  email: string;

  @Property()
  nickname: string;

  @Property()
  isEmailValid: boolean;

  @Property()
  isSmsValid: boolean;

  @Property()
  image: string | null;

  @CollectionOf(UserPayload)
  payloads: UserPayload[];

  @Property()
  @Enum(RegisterProvider)
  provider: RegisterProvider;
}
