import {Req, Service} from '@tsed/common';

import * as cryptoJs from 'crypto-js';
import {
  EntityManager,
  In,
  IsNull,
  Transaction,
  TransactionManager,
} from 'typeorm';
import {User} from '../entities/User';
import {UserPayload} from '../entities/UserPayload';
import {BaseError} from '../exceptions/BaseError';
import {PageParams} from '../models/Common/Page';
import {UserAddBody} from '../models/User/AddBody';
import {UserBody} from '../models/User/Body';
import {UserSearchBody} from '../models/User/Search';
import {UserUpdatePayloadBody} from '../models/User/UpdatePayloadBody';
import {UserPayloadRepo} from '../repositorys/UserPayloadRepo';
import {UserRepo} from '../repositorys/UserRepo';
import {BaseService} from './BaseSerivce';

@Service()
export class UserService extends BaseService {
  constructor(private user: UserRepo, private payload: UserPayloadRepo) {
    super();
  }

  cryptoPassword(password: string) {
    return cryptoJs.SHA256(password).toString();
  }

  async delete(userId: number) {
    await this.user.update({id: userId}, {deletedAt: new Date()});
    return this.findOne(userId);
  }

  async findAll(data: UserSearchBody) {
    return this.user.search(data);
  }

  async adFindAll(search: UserSearchBody) {
    return this.user.search(search);
  }

  async findOne(id: number) {
    const dbData = await this.user.findOne({
      where: {
        id,
        deletedAt: IsNull(),
      },
      relations: ['payloads'],
    });

    if (dbData === null || dbData === undefined) {
      throw new BaseError(BaseError.Code.USER_NOT_FOUND);
    }
    return new UserBody(dbData);
  }

  @Transaction()
  async create(
    appId: number,
    data: UserAddBody,
    @TransactionManager() manager?: EntityManager
  ): Promise<UserBody> {
    if (await this.user.exist({email: data.email})) {
      throw new BaseError(BaseError.Code.USER_EMAIL_IS_EXIST);
    }

    if (await this.user.exist({phone: data.phone})) {
      throw new BaseError(BaseError.Code.USER_PHONE_IS_EXIST);
    }

    if (await this.user.exist({username: data.username})) {
      throw new BaseError(BaseError.Code.USER_USERNAME_IS_EXIST);
    }

    const dbData = new User({
      appId,
      createdAt: new Date(),
      email: data.email,
      username: data.username,
      password: this.cryptoPassword(data.password),
      nickname: data.nickname,
      phone: data.phone,
      isEmailValid: data.isEmailValid,
      isSmsValid: data.isSmsValid,
      image: data.image,
    });
    await manager.save(dbData);
    const payloadList = data.payloads.map(
      x =>
        new UserPayload({
          appId,
          userId: dbData.id,
          fieldName: x.fieldName,
          fieldValue: x.fieldValue,
        })
    );

    await manager.save(payloadList);

    return new UserBody({
      ...dbData,
      payloads: payloadList,
    });
  }

  @Transaction()
  async patchUpdate(
    id: number,
    data: Partial<UserAddBody>,
    @TransactionManager() manager?: EntityManager
  ) {
    let dbData = await this.user.findOne(id, {relations: ['payloads']});
    if (dbData === undefined) {
      throw new BaseError(BaseError.Code.USER_NOT_FOUND);
    }
    const {payloads, ...userData} = data;

    if (userData.password) {
      userData.password = this.cryptoPassword(userData.password);
    }

    dbData = Object.assign(dbData, userData);
    await manager.save(dbData);

    if (payloads !== undefined) {
      const oldPayloads = dbData.payloads;
      const newPayloads = payloads.map(x => new UserPayload(x));

      const diff = this.payload.differenceList(
        oldPayloads,
        newPayloads,
        'fieldName'
      );

      await manager.delete(UserPayload, {
        id: In(diff.remove.map(x => x.id)),
      });
      await manager.save(diff.update);
      await manager.save(diff.add);
    }

    return new UserBody(
      await manager.findOne(User, id, {
        relations: ['payloads'],
      })
    );
  }

  @Transaction()
  async updateMultiPayload(
    appId: number,
    userId: number,
    data: UserUpdatePayloadBody,
    isOverwrite: boolean,
    @TransactionManager() manager?: EntityManager
  ) {
    if (isOverwrite) {
      await manager.delete(UserPayload, {
        userId,
      });
    } else {
      await manager.delete(UserPayload, {
        fieldName: In(data.items.map(x => x.fieldName)),
      });
    }

    if (data.items) {
      const userPayloadList = data.items.map(
        x =>
          new UserPayload({
            ...x,
            userId,
            appId,
          })
      );

      await manager.save(userPayloadList);
    }

    const userData = await manager.findOne(User, userId, {
      relations: ['payloads'],
    });

    return new UserBody(userData);
  }

  async updatePayload(
    appId: number,
    userId: number,
    fieldName: string,
    fieldValue: string
  ) {
    if (await this.payload.exist({fieldName, userId})) {
      await this.payload.update({fieldName, userId}, {fieldValue: fieldValue});
    } else {
      await this.payload.save({
        appId,
        fieldName,
        fieldValue,
        userId,
      });
    }
    return await this.findOne(userId);
  }

  async deletePayload(appId: number, userId: number, fieldName: string) {
    await this.payload.delete({
      fieldName,
      userId,
    });
    return await this.findOne(userId);
  }
}
