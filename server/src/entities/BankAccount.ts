import {Column, Entity, ManyToOne} from 'typeorm';
import {User} from './User';
import {CommonBase} from './Common/Base';

@Entity()
export class BankAccount extends CommonBase {
  @ManyToOne(() => User)
  user: User | null;
  @Column()
  accountCode: string;

  @Column()
  accountNumber: string;

  @Column()
  accountName: string;
}
