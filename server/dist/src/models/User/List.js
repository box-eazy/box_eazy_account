"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListItem = void 0;
const schema_1 = require("@tsed/schema");
const FilterLog_1 = require("../../entities/FilterLog");
const BaseModel_1 = require("../Common/BaseModel");
const Payload_1 = require("./Payload");
const RegisterProvider_1 = require("./RegisterProvider");
class UserListItem extends BaseModel_1.BaseModel {
    constructor(init) {
        super(init);
        if (init.payloads !== undefined) {
            this.payloadObj = this.payloads.reduce((curr, item) => {
                return {
                    ...curr,
                    [item.fieldName]: item.fieldValue,
                };
            }, {});
        }
    }
}
__decorate([
    schema_1.Property(),
    __metadata("design:type", Number)
], UserListItem.prototype, "id", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserListItem.prototype, "username", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserListItem.prototype, "facebookId", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserListItem.prototype, "googleId", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserListItem.prototype, "phone", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserListItem.prototype, "email", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserListItem.prototype, "nickname", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", Boolean)
], UserListItem.prototype, "isEmailValid", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", Boolean)
], UserListItem.prototype, "isSmsValid", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserListItem.prototype, "image", void 0);
__decorate([
    schema_1.CollectionOf(Payload_1.UserPayload),
    __metadata("design:type", Array)
], UserListItem.prototype, "payloads", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", Object)
], UserListItem.prototype, "payloadObj", void 0);
__decorate([
    schema_1.CollectionOf(FilterLog_1.FilterLog),
    __metadata("design:type", Array)
], UserListItem.prototype, "filters", void 0);
__decorate([
    schema_1.Property(),
    schema_1.Enum(RegisterProvider_1.RegisterProvider),
    __metadata("design:type", String)
], UserListItem.prototype, "provider", void 0);
exports.UserListItem = UserListItem;
//# sourceMappingURL=List.js.map