import {Property} from '@tsed/schema';
import {BaseModel} from '../Common/BaseModel';

export class PepayPostFormData extends BaseModel<PepayPostFormData> {
  @Property()
  SHOP_ID: string;
  @Property()
  ORDER_ID: string;

  @Property()
  ORDER_ITEM: string;

  @Property()
  PROD_ID: string;
  @Property()
  AMOUNT: number;
  @Property()
  CURRENCY: string;
  @Property()
  SHOP_PARA: string;
  @Property()
  CHECK_CODE: string;
}

export class PepayPostForm extends BaseModel<PepayPostForm> {
  @Property(PepayPostFormData)
  data: PepayPostFormData;

  @Property()
  url: string;
}
