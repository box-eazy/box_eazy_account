import {Property} from '@tsed/schema';
import {Column, Entity, JoinColumn, OneToOne} from 'typeorm';
import {CommonBase} from './Common/Base';
import {PepayLog} from './PepayLog';

@Entity()
export class PepayReceive2Log extends CommonBase<PepayReceive2Log> {
  @OneToOne(() => PepayLog)
  @JoinColumn()
  pepay: PepayLog;

  @Column()
  @Property()
  pepayId: number;

  @Column({nullable: true})
  @Property()
  sessId: string;

  @Column({nullable: true})
  @Property()
  orderId: string;

  @Column()
  @Property()
  billId: string;

  @Column()
  @Property()
  dataId: string;

  @Column({nullable: true})
  @Property()
  shopId: string;

  @Column({nullable: true})
  @Property()
  payType: string;

  @Column({nullable: true})
  @Property()
  prodId: string;

  @Column({nullable: true})
  @Property()
  userId: string;

  @Column({nullable: true})
  @Property()
  sourceAmount: number;

  @Column({nullable: true})
  @Property()
  amount: number;

  @Column({nullable: true})
  @Property()
  currency: string;

  @Column({nullable: true})
  @Property()
  dataCode: number;

  @Column({nullable: true})
  @Property()
  tradeCode: number;

  @Column({nullable: true})
  @Property()
  shopPara: string;

  @Column({nullable: true})
  @Property()
  cdate: string;

  @Column({nullable: true})
  @Property()
  ctime: string;

  @Column({nullable: true})
  @Property()
  billDate: string;

  @Column({nullable: true})
  @Property()
  billTime: string;

  @Column({nullable: true})
  @Property()
  date: string;

  @Column({nullable: true})
  @Property()
  time: string;

  @Column({nullable: true})
  @Property()
  checkCode: string;

  @Column({default: false})
  isReturn: boolean;
}
