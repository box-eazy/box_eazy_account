import {EntityRepository} from 'typeorm';
import {BillLog} from '../entities/BillLog';
import {BaseRepo} from './BaseRepo';

@EntityRepository(BillLog)
export class BillRepo extends BaseRepo<BillLog> {}
