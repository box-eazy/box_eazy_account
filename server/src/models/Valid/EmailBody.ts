import {Description, Enum, Property} from '@tsed/schema';
import {ValidTemp} from './Temp';

export class ValidEmailAddBody {
  @Property()
  email: string;

  @Enum(ValidTemp)
  @Property()
  temp: ValidTemp;

  @Property()
  @Description('驗證信箱的連結')
  validUrl: string;

  @Property()
  redirectUrl: string;
}
