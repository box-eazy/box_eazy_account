"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilterLogRepo = void 0;
const typeorm_1 = require("typeorm");
const FilterLog_1 = require("../entities/FilterLog");
const BaseRepo_1 = require("./BaseRepo");
let FilterLogRepo = class FilterLogRepo extends BaseRepo_1.BaseRepo {
};
FilterLogRepo = __decorate([
    typeorm_1.EntityRepository(FilterLog_1.FilterLog)
], FilterLogRepo);
exports.FilterLogRepo = FilterLogRepo;
//# sourceMappingURL=FilterLogRepo.js.map