"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const di_1 = require("@tsed/di");
const randToken = require("rand-token");
const AppRepo_1 = require("../repositorys/AppRepo");
const jwt = require("jsonwebtoken");
const BaseError_1 = require("../exceptions/BaseError");
let AppService = class AppService {
    constructor(app) {
        this.app = app;
    }
    async create(appName) {
        const apiKey = randToken.generate(12);
        const apiSecert = randToken.generate(16);
        return await this.app.save({
            tilte: appName,
            apiKey,
            apiSecert,
            isDisabled: false,
            createdAt: new Date(),
        });
    }
    async getAppByApiKey(apiKey) {
        const app = await this.app.findOne({
            apiKey,
        });
        if (app === undefined || app === null) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.API_KEY_NOT_FOUND);
        }
        return app;
    }
    async findAll() {
        return await this.app.find();
    }
    async findOne(id) {
        return await this.app.findOne(id);
    }
    decodeToken(token) {
        const decodeData = jwt.decode(token);
        if (decodeData === null) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.API_TOKEN_FAILED);
        }
        if (decodeData.exp === undefined) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.API_TOKEN_FAILED);
        }
        if (decodeData.exp < new Date().getTime()) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.API_TOKEN_EXPIRE);
        }
        return decodeData;
    }
    validToken(token, apiSecert) {
        try {
            jwt.verify(token, apiSecert);
            return true;
        }
        catch (err) {
            return false;
        }
    }
    async encodeToken(appId, apiKey, apiSecert, expire = 60) {
        return jwt.sign({
            apiKey,
            appId,
            exp: new Date().getTime() + expire * 1000,
        }, apiSecert, {
            algorithm: 'HS256',
        });
    }
};
AppService = __decorate([
    di_1.Service(),
    __metadata("design:paramtypes", [AppRepo_1.AppRepo])
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=AppService.js.map