import {ErrorCode} from '../models/Error';

export class BaseError extends Error {
  static Code = ErrorCode;
  constructor(
    public errCode: ErrorCode,
    public errMessage?: string,
    public payload?: any
  ) {
    super(errMessage ? errMessage : ErrorCode[errCode]);
    this.errMessage = errMessage ? errMessage : ErrorCode[errCode];
  }
}
