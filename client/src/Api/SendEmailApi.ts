import {getCurrentUrl} from '../libs/getCurrentUrl';
import {baseAxios} from './Base';

export const SendEmailApi = async (
  apiKey: string,
  email: string,
  validUrl: string,
  redirectUrl: string,
  type: 'REGISTR' | 'FORGET'
) => {
  await baseAxios({
    url: '/web/email',
    method: 'POST',
    data: {
      apiKey,
      email,
      type,
      validUrl,
      redirectUrl,
    },
  });
};

export const ValidEmailToken = async (token: string) => {};
