"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PepayPostForm = exports.PepayPostFormData = void 0;
const schema_1 = require("@tsed/schema");
const BaseModel_1 = require("../Common/BaseModel");
class PepayPostFormData extends BaseModel_1.BaseModel {
}
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayPostFormData.prototype, "SHOP_ID", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayPostFormData.prototype, "ORDER_ID", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayPostFormData.prototype, "ORDER_ITEM", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayPostFormData.prototype, "PROD_ID", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayPostFormData.prototype, "AMOUNT", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayPostFormData.prototype, "CURRENCY", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayPostFormData.prototype, "SHOP_PARA", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayPostFormData.prototype, "CHECK_CODE", void 0);
exports.PepayPostFormData = PepayPostFormData;
class PepayPostForm extends BaseModel_1.BaseModel {
}
__decorate([
    schema_1.Property(PepayPostFormData),
    __metadata("design:type", PepayPostFormData)
], PepayPostForm.prototype, "data", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayPostForm.prototype, "url", void 0);
exports.PepayPostForm = PepayPostForm;
//# sourceMappingURL=PostForm.js.map