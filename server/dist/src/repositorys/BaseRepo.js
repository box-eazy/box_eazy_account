"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRepo = void 0;
const typeorm_1 = require("typeorm");
const _ = require("lodash");
class BaseRepo extends typeorm_1.Repository {
    async exist(where) {
        return (await this.count(where)) > 0;
    }
    differenceList(originalList, newList, differenceKey) {
        const result = {
            update: [],
            remove: [],
            add: [],
        };
        const oldKeys = originalList.map(x => x[differenceKey]);
        const newKeys = newList.map(x => x[differenceKey]);
        const newValue = _.keyBy(newList, differenceKey);
        for (const original of originalList) {
            if (!newKeys.includes(original[differenceKey])) {
                result.remove.push(original);
            }
            else {
                const newData = Object.assign(original, newValue[original[differenceKey]]);
                result.update.push(newData);
            }
        }
        for (const newItem of newList) {
            if (!oldKeys.includes(newItem[differenceKey])) {
                result.add.push(newItem);
            }
        }
        return result;
    }
}
exports.BaseRepo = BaseRepo;
//# sourceMappingURL=BaseRepo.js.map