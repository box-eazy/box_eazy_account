import {Property} from '@tsed/schema';
import {Column, Entity, ManyToOne} from 'typeorm';
import {ValidTemp} from '../models/Valid/Temp';
import {Application} from './Application';
import {CommonBase} from './Common/Base';

@Entity()
export class EmailLog extends CommonBase<EmailLog> {
  @Column()
  email: string;

  @Column({
    type: 'text',
  })
  content: string;

  @Column()
  token: string;

  @Column()
  expired: Date;

  @Column({default: false})
  isUse: boolean;

  @Column({
    comment: '驗證email的url',
  })
  validUrl: string;

  @Column({
    comment: '驗證成功時導向的連結',
  })
  @Property()
  redirectUrl: string;

  @ManyToOne(() => Application)
  app?: Application;

  @Column()
  appId: number;

  @Column()
  temp: ValidTemp;
}
