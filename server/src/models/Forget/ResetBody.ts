import {Property} from '@tsed/schema';

export class ForgetResetBody {
  @Property()
  phone: string;
  @Property()
  password: string;

  @Property()
  confirmPassword: string;
}
