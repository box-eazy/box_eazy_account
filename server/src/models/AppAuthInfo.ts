import {Property} from '@tsed/schema';

export class AuthAppInfo {
  @Property()
  appId: number;
  @Property()
  apiKey: string;

  @Property()
  exp: number;
}
