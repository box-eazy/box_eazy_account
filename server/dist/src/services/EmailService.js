"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailService = void 0;
const di_1 = require("@tsed/di");
const typeorm_1 = require("typeorm");
const EmailLog_1 = require("../entities/EmailLog");
const User_1 = require("../entities/User");
const EmailRepo_1 = require("../repositorys/EmailRepo");
const UserRepo_1 = require("../repositorys/UserRepo");
const fs = require("fs-extra");
const Temp_1 = require("../models/Valid/Temp");
const BaseError_1 = require("../exceptions/BaseError");
const date_fns_1 = require("date-fns");
const EmailBody_1 = require("../models/Valid/EmailBody");
const randToken = require("rand-token");
const nodemailer = require("nodemailer");
const envConfig = require("../../envConfig");
let EmailService = class EmailService {
    constructor(user, email) {
        this.user = user;
        this.email = email;
    }
    sendEmail(email, subject, content) {
        const transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: envConfig.EMAIL_ACCOUNT,
                pass: envConfig.EMAIL_PASSWORD,
            },
        });
        return transporter.sendMail({
            from: envConfig.EMAIL_ACCOUNT,
            to: email,
            subject,
            html: content,
        });
    }
    generateToken() { }
    loadSubject(temp) {
        if (temp === Temp_1.ValidTemp.FOREGT) {
            return 'EAZY會員重設密碼Email驗證';
        }
        else {
            return 'EAZY會員Email驗證';
        }
    }
    loadTemplate(temp, params) {
        let html = fs
            .readFileSync(`${process.cwd()}/static/emailTemp/${temp}.html`)
            .toString();
        for (const key in params) {
            html = html.replace(`[${key}]`, params[key]);
        }
        return html;
    }
    async getByToken(token) {
        const dbData = await this.email.findOne({
            token,
        });
        if (dbData === undefined || dbData === null) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.EMAIL_TOKEN_NOT_FOUND);
        }
        if (dbData.isUse) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.EMAIL_TOKEN_IS_USE);
        }
        if (dbData.expired < new Date()) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.EMAIL_TOKEN_EXPIRED);
        }
        return dbData;
    }
    async $sendValidEmail(appId, data, manager) {
        const { email, temp, redirectUrl, validUrl } = data;
        const token = randToken.generate(20);
        const dbUser = await manager.findOne(User_1.User, { email });
        if (dbUser === undefined || dbUser === null) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_NOT_FOUND);
        }
        const expired = date_fns_1.addMinutes(new Date(), 60);
        const template = this.loadTemplate(temp, {
            EXPIRED: date_fns_1.format(expired, 'yyyy-MM-dd HH:mm'),
            VALID_URL: validUrl,
            TOKEN: token,
        });
        await manager.save(dbUser);
        await manager.save(new EmailLog_1.EmailLog({
            appId,
            content: template,
            email: dbUser.email,
            expired,
            token,
            isUse: false,
            createdAt: new Date(),
            temp,
            validUrl,
            redirectUrl,
        }));
        try {
            await this.sendEmail(email, this.loadSubject(temp), template);
        }
        catch (err) {
            console.log(err);
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.EMAIL_SEND_ERROR);
        }
    }
    async sendValidEmail(appId, data, manager) {
        return this.$sendValidEmail(appId, data, manager);
    }
    async $validToken(token, manager) {
        const dbData = await manager.findOne(EmailLog_1.EmailLog, {
            token,
        });
        if (dbData === null || dbData === undefined) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.EMAIL_TOKEN_NOT_FOUND);
        }
        if (dbData.isUse) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.EMAIL_TOKEN_IS_USE);
        }
        if (dbData.expired < new Date()) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.EMAIL_TOKEN_EXPIRED);
        }
        dbData.isUse = true;
        await manager.save(dbData);
        return dbData;
    }
    async validToken(token, manager) {
        return this.$validToken(token, manager);
    }
};
__decorate([
    typeorm_1.Transaction(),
    __param(2, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, EmailBody_1.ValidEmailAddBody,
        typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], EmailService.prototype, "sendValidEmail", null);
__decorate([
    typeorm_1.Transaction(),
    __param(1, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], EmailService.prototype, "validToken", null);
EmailService = __decorate([
    di_1.Service(),
    __metadata("design:paramtypes", [UserRepo_1.UserRepo, EmailRepo_1.EmailRepo])
], EmailService);
exports.EmailService = EmailService;
//# sourceMappingURL=EmailService.js.map