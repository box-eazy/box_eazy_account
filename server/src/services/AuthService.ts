import {Service} from '@tsed/di';
import {EntityManager, IsNull, Transaction, TransactionManager} from 'typeorm';
import {BaseError} from '../exceptions/BaseError';
import {UserRepo} from '../repositorys/UserRepo';
import {UserService} from './UserSerivce';
import * as jwt from 'jsonwebtoken';
import {isNull} from 'lodash';
import {AppRepo} from '../repositorys/AppRepo';
import {BasicAuthInfo, WebAuthInfo} from '../models/Auth/Info';
import {RegisterBody} from '../models/Register/Body';
import {SMSService} from './SMSService';
import {User} from '../entities/User';
import {EmailService} from './EmailService';
import {ForgetValidBody} from '../models/Forget/ValidBody';
import {ValidTemp} from '../models/Valid/Temp';
import {ForgetResetBody} from '../models/Forget/ResetBody';
import {ForgetResult} from '../models/Auth/ForgetResult';
import {Application} from '../entities/Application';
@Service()
export class AuthService {
  constructor(
    private userRepo: UserRepo,
    private userSerivice: UserService,
    private appRepo: AppRepo,
    private smsService: SMSService,
    private emailService: EmailService
  ) {}

  async generateToken(apiKey: string, userId: number) {
    const dbApp = await this.appRepo.findOne({apiKey});
    if (dbApp === undefined || dbApp === null) {
      throw new BaseError(BaseError.Code.API_KEY_NOT_FOUND);
    }
    return jwt.sign(
      {
        userId,
        exp: new Date().getTime() + 1000 * 60 * 24 * 365,
      },
      dbApp.apiSecert
    );
  }

  async basicLogin(username: string, password: string) {
    const dbData = await this.userRepo.findOne({
      where: {
        username,
        deletedAt: IsNull(),
        isDisabled: false,
      },
    });

    if (dbData === undefined) {
      throw new BaseError(BaseError.Code.AUTH_FAILED);
    }

    if (this.userSerivice.cryptoPassword(password) !== dbData.password) {
      throw new BaseError(BaseError.Code.AUTH_FAILED);
    }

    if (dbData.isDisabled) {
      throw new BaseError(BaseError.Code.AUTH_FAILED);
    }

    return new BasicAuthInfo({
      userId: dbData.id,
    });
  }

  async tokenLogin(appId: number, token: string) {
    const dbApp = await this.appRepo.findOne(appId);
    const payload = jwt.verify(token, dbApp.apiSecert) as {
      userId: number;
      exp: number;
    };

    if (!(payload.exp > new Date().getTime())) {
      throw new BaseError(BaseError.Code.AUTH_TOKEN_FAILED);
    }

    const dbData = await this.userRepo.findOne({
      id: payload.userId,
      deletedAt: IsNull(),
      isDisabled: false,
    });

    if (dbData === undefined) {
      throw new BaseError(BaseError.Code.AUTH_FAILED);
    }

    return new BasicAuthInfo({
      userId: dbData.id,
    });
  }

  async webLogin(apiKey: string, username: string, password: string) {
    const res = await this.basicLogin(username, password);
    return new WebAuthInfo({
      userId: res.userId,
      token: await this.generateToken(apiKey, res.userId),
    });
  }

  async webProviderLogin(apiKey: string, provider: string, email: string) {
    const res = await this.webProviderLogin(apiKey, provider, email);
    return new WebAuthInfo({
      userId: res.id,
      token: await this.generateToken(apiKey, res.id),
    });
  }

  @Transaction()
  async register(
    appId: number,
    apiKey: string,
    data: RegisterBody,
    @TransactionManager() manager?: EntityManager
  ) {
    await this.smsService.$validSMSCode(data.phone, data.smsCode, manager);

    if (await this.userRepo.exist({username: data.username})) {
      throw new BaseError(BaseError.Code.USER_USERNAME_IS_EXIST);
    }

    if (await this.userRepo.exist({email: data.email})) {
      throw new BaseError(BaseError.Code.USER_EMAIL_IS_EXIST);
    }

    if (await this.userRepo.exist({phone: data.phone})) {
      throw new BaseError(BaseError.Code.USER_PHONE_IS_EXIST);
    }

    let dbData = new User({
      ...data,
      isSmsValid: true,
      appId: appId,
    });
    dbData = await manager.save(dbData);
    return new WebAuthInfo({
      userId: dbData.id,
      token: await this.generateToken(apiKey, dbData.id),
    });
  }

  @Transaction()
  async sendForgetEmail(
    appId: number,
    data: ForgetValidBody,
    @TransactionManager() manager?: EntityManager
  ) {
    const dbUser = await manager.findOne(User, {
      email: data.email,
      username: data.username,
      phone: data.phone,
    });
    if (dbUser === undefined || dbUser === null) {
      throw new BaseError(BaseError.Code.USER_NOT_FOUND);
    }

    await this.smsService.$validSMSCode(data.phone, data.smsCode, manager);

    await this.emailService.$sendValidEmail(
      appId,
      {
        email: data.email,
        temp: ValidTemp.FOREGT,
        ...data,
      },
      manager
    );
  }

  @Transaction()
  async resetPassword(
    token: string,
    data: ForgetResetBody,
    @TransactionManager() manager?: EntityManager
  ) {
    const emailLog = await this.emailService.$validToken(token, manager);
    const dbUser = await manager.findOne(User, {
      email: emailLog.email,
      phone: data.phone,
      deletedAt: IsNull(),
    });
    if (dbUser === undefined || dbUser === null) {
      throw new BaseError(BaseError.Code.USER_NOT_FOUND);
    }
    if (data.password !== data.confirmPassword) {
      throw new BaseError(BaseError.Code.REGISTER_TWO_PASSWORD_FAILED);
    }
    dbUser.password = this.userSerivice.cryptoPassword(data.password);
    await manager.save(dbUser);
    const app = await manager.findOne(Application, emailLog.appId);
    return new ForgetResult({
      apiKey: app.apiKey,
      redirectUrl: emailLog.redirectUrl,
    });
  }

  async validForgetToken(token: string) {
    const email = await this.emailService.getByToken(token);
    const app = await this.appRepo.findOne(email.appId);
    return new ForgetResult({
      ...email,
      apiKey: app.apiKey,
    });
  }
}
