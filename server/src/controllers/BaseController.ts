import {Security} from '@tsed/schema';

@Security('bearerAuth')
export class BaseController {}
