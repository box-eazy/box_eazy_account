"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreditController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const AuthApp_1 = require("../../decorators/AuthApp");
const CreditLog_1 = require("../../entities/CreditLog");
const BearerTokenAuth_1 = require("../../middlewares/BearerTokenAuth");
const AppAuthInfo_1 = require("../../models/AppAuthInfo");
const DateRange_1 = require("../../models/Common/DateRange");
const Page_1 = require("../../models/Common/Page");
const AddBody_1 = require("../../models/Credit/AddBody");
const Stats_1 = require("../../models/Credit/Stats");
const Status_1 = require("../../models/Credit/Status");
const CreditService_1 = require("../../services/CreditService");
const Response_1 = require("../../services/Response");
let CreditController = class CreditController {
    constructor(credit, res) {
        this.credit = credit;
        this.res = res;
    }
    findAll(dateRange, page, appId, status, keyword) {
        return this.res.toPage(this.credit.findAll(dateRange, page));
    }
    create(data, app) {
        return this.res.toResult(this.credit.create(app.appId, data));
    }
    edit(id, data) {
        return this.res.toResult(this.credit.edit(id, data));
    }
    delete(id) {
        return this.res.toResult(this.credit.delete(id));
    }
    stats() {
        return this.res.toResult(this.credit.stats());
    }
};
__decorate([
    schema_1.Summary('取得入帳列表'),
    common_1.Get(),
    schema_1.Returns(200, Response_1.ResponseService.toPagiation(CreditLog_1.CreditLog)),
    __param(0, common_1.QueryParams()),
    __param(1, common_1.QueryParams()),
    __param(2, common_1.QueryParams('appId')),
    __param(3, common_1.QueryParams('status')),
    __param(4, common_1.QueryParams('keyword')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [DateRange_1.DateRangeParams,
        Page_1.PageParams, Number, String, String]),
    __metadata("design:returntype", void 0)
], CreditController.prototype, "findAll", null);
__decorate([
    schema_1.Summary('新增入帳資料(用此API新增的入帳資料會與Pepay脫勾)'),
    common_1.Post(),
    schema_1.Returns(200, Response_1.ResponseService.toResult(CreditLog_1.CreditLog)),
    __param(0, common_1.BodyParams()), __param(1, AuthApp_1.AuthApp()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [AddBody_1.CreditAddBody, AppAuthInfo_1.AuthAppInfo]),
    __metadata("design:returntype", void 0)
], CreditController.prototype, "create", null);
__decorate([
    schema_1.Summary('編輯入帳資料'),
    common_1.Put('/:id'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(CreditLog_1.CreditLog)),
    __param(0, common_1.PathParams('id')), __param(1, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, AddBody_1.CreditAddBody]),
    __metadata("design:returntype", void 0)
], CreditController.prototype, "edit", null);
__decorate([
    schema_1.Summary('刪除入帳資料'),
    common_1.Delete('/:id'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(CreditLog_1.CreditLog)),
    __param(0, common_1.PathParams('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], CreditController.prototype, "delete", null);
__decorate([
    schema_1.Summary('/統計各平台收入'),
    common_1.Get('/stats'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Stats_1.CreditStats)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], CreditController.prototype, "stats", null);
CreditController = __decorate([
    common_1.Controller('/credit'),
    schema_1.Description('入帳資料相關API'),
    BearerTokenAuth_1.BearerTokenAuth(),
    __metadata("design:paramtypes", [CreditService_1.CreditService, Response_1.ResponseService])
], CreditController);
exports.CreditController = CreditController;
//# sourceMappingURL=Controller.js.map