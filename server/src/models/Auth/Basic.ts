import {Property} from '@tsed/schema';

export class BasicAuthBody {
  @Property()
  username: string;
  @Property()
  password: string;
}

export class WebAuthBody extends BasicAuthBody {
  @Property()
  apiKey: string;
}
