"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateOrderId = void 0;
const date_fns_1 = require("date-fns");
const lodash_1 = require("lodash");
class GenerateOrderId {
    constructor(name, dateFormat, orderNumLength) {
        this.name = name;
        this.dateFormat = dateFormat;
        this.orderNumLength = orderNumLength;
        this.prefixLength = name.length + dateFormat.length;
    }
    formatDate(date) {
        return date_fns_1.format(date === undefined ? new Date() : date, this.dateFormat);
    }
    getOrderNum(orderId) {
        return parseInt(orderId.slice(this.prefixLength));
    }
    generateOrderId(orderNum, date) {
        return `${this.name}${this.formatDate(date)}${lodash_1.padStart(orderNum.toString(), this.orderNumLength, '0')}`;
    }
    getPrefix(date) {
        return `${this.name}${this.formatDate(date)}`;
    }
}
exports.GenerateOrderId = GenerateOrderId;
//# sourceMappingURL=orderId.js.map