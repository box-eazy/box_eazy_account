import {AjvValidationError} from '@tsed/ajv';
import {Catch, ExceptionFilterMethods, PlatformContext} from '@tsed/common';
import {BadRequest, Exception} from '@tsed/exceptions';
import {BaseError} from '../exceptions/BaseError';

@Catch(BadRequest)
export class AjvErrorFilter implements ExceptionFilterMethods {
  catch(exception: Exception, ctx: PlatformContext) {
    ctx.logger.error(exception);
    if (exception.origin instanceof AjvValidationError) {
      ctx.response.status(400);
      ctx.response.body(
        new BaseError(
          BaseError.Code.AJV_VALID_ERROR,
          undefined,
          exception.origin.errors
        )
      );
    }
  }
}
