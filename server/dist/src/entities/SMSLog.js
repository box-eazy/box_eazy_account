"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SMSLog = void 0;
const typeorm_1 = require("typeorm");
const Temp_1 = require("../models/Valid/Temp");
const Application_1 = require("./Application");
const Base_1 = require("./Common/Base");
let SMSLog = class SMSLog extends Base_1.CommonBase {
};
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], SMSLog.prototype, "phone", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], SMSLog.prototype, "code", void 0);
__decorate([
    typeorm_1.Column({
        type: 'text',
    }),
    __metadata("design:type", String)
], SMSLog.prototype, "content", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Date)
], SMSLog.prototype, "expired", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], SMSLog.prototype, "isUse", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], SMSLog.prototype, "appId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => Application_1.Application),
    __metadata("design:type", Application_1.Application)
], SMSLog.prototype, "app", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], SMSLog.prototype, "temp", void 0);
SMSLog = __decorate([
    typeorm_1.Entity()
], SMSLog);
exports.SMSLog = SMSLog;
//# sourceMappingURL=SMSLog.js.map