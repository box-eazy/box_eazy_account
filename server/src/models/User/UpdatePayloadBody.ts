import {CollectionOf} from '@tsed/schema';
import {UserPayload} from '../../entities/UserPayload';

export class UserUpdatePayloadBody {
  @CollectionOf(UserPayload)
  items: UserPayload[];
}
