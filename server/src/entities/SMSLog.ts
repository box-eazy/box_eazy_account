import {Column, Entity, ManyToOne} from 'typeorm';
import {ValidTemp} from '../models/Valid/Temp';
import {Application} from './Application';
import {CommonBase} from './Common/Base';

@Entity()
export class SMSLog extends CommonBase<SMSLog> {
  @Column()
  phone: string;

  @Column()
  code: string;

  @Column({
    type: 'text',
  })
  content: string;

  @Column()
  expired: Date;

  @Column({default: false})
  isUse: boolean;

  @Column()
  appId: number;

  @ManyToOne(() => Application)
  app?: Application;

  @Column()
  temp: ValidTemp;
}
