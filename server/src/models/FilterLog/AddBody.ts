import {Enum, Property} from '@tsed/schema';

export enum FilterLogType {
  WHITE = 'WHITE',
  BLACK = 'BLACK',
}

export class FilterLogAddBody {
  @Property()
  phone: string;

  @Enum(FilterLogType)
  type: FilterLogType;

  @Property()
  note: string;
}
