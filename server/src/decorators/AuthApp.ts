import {Inject, Injectable, IPipe, Req, UsePipe} from '@tsed/common';
import {useDecorators} from '@tsed/core';
import {AppService} from '../services/AppService';

@Injectable()
export class Pipe implements IPipe<Req> {
  @Inject()
  app: AppService;

  async transform(req: Req) {
    return this.app.decodeToken(req.token);
  }
}

export function AuthApp(): ParameterDecorator {
  return useDecorators(Req(), UsePipe(Pipe));
}
