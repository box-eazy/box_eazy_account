import {
  BodyParams,
  Controller,
  MultipartFile,
  PlatformMulterFile,
  Post,
} from '@tsed/common';
import {Description, Returns, Summary} from '@tsed/schema';
import {BearerTokenAuth} from '../../middlewares/BearerTokenAuth';

@Controller('/files')
@Description('檔案上傳相關API')
@BearerTokenAuth()
export class FileController {
  // @Post('/image')
  // @Returns(200)
  // @Summary('上傳圖片並回傳網址')
  // upload(
  //   @MultipartFile('file') file: PlatformMulterFile,
  //   @BodyParams('dir') dir: string
  // ) {}
}
