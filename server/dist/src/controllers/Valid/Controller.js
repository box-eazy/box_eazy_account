"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const AuthApp_1 = require("../../decorators/AuthApp");
const BearerTokenAuth_1 = require("../../middlewares/BearerTokenAuth");
const AppAuthInfo_1 = require("../../models/AppAuthInfo");
const Res_1 = require("../../models/Res");
const EmailBody_1 = require("../../models/Valid/EmailBody");
const SMSBody_1 = require("../../models/Valid/SMSBody");
const EmailService_1 = require("../../services/EmailService");
const Response_1 = require("../../services/Response");
const SMSService_1 = require("../../services/SMSService");
let ValidController = class ValidController {
    constructor(sms, email, res) {
        this.sms = sms;
        this.email = email;
        this.res = res;
    }
    async sendSMS(app, data) {
        await this.sms.sendSMSCode(app.appId, data.phone, data.temp);
    }
    async validSMS(phone, data) {
        await this.sms.validSMSCode(phone, data.smsCode);
        return this.res.Ok();
    }
    async sendEmail(data, app) {
        await this.email.sendValidEmail(app.appId, data);
        return this.res.Ok();
    }
    async validEmail(token) {
        return this.res.Ok();
    }
};
__decorate([
    schema_1.Summary('發送手機驗證碼'),
    common_1.Post('/sms'),
    schema_1.Returns(200, Res_1.BaseRes),
    __param(0, AuthApp_1.AuthApp()), __param(1, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [AppAuthInfo_1.AuthAppInfo, SMSBody_1.SMSBody]),
    __metadata("design:returntype", Promise)
], ValidController.prototype, "sendSMS", null);
__decorate([
    schema_1.Summary('驗證手機驗證碼'),
    common_1.Post('/sms/:phone'),
    schema_1.Returns(200, Res_1.BaseRes),
    __param(0, common_1.PathParams('phone')),
    __param(1, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, SMSBody_1.SMSValidBody]),
    __metadata("design:returntype", Promise)
], ValidController.prototype, "validSMS", null);
__decorate([
    schema_1.Summary('發送Email驗證信'),
    common_1.Post('/email'),
    schema_1.Returns(200, Res_1.BaseRes),
    __param(0, common_1.BodyParams()),
    __param(1, AuthApp_1.AuthApp()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [EmailBody_1.ValidEmailAddBody,
        AppAuthInfo_1.AuthAppInfo]),
    __metadata("design:returntype", Promise)
], ValidController.prototype, "sendEmail", null);
__decorate([
    schema_1.Summary('驗證Email token'),
    common_1.Post('/email/:token'),
    schema_1.Returns(200, Res_1.BaseRes),
    __param(0, common_1.PathParams('token')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ValidController.prototype, "validEmail", null);
ValidController = __decorate([
    common_1.Controller('/verify'),
    BearerTokenAuth_1.BearerTokenAuth(),
    schema_1.Description('手機 & 信箱驗證相關 API'),
    __metadata("design:paramtypes", [SMSService_1.SMSService,
        EmailService_1.EmailService,
        Response_1.ResponseService])
], ValidController);
exports.ValidController = ValidController;
//# sourceMappingURL=Controller.js.map