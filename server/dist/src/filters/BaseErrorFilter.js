"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseErrorFilter = void 0;
const common_1 = require("@tsed/common");
const BaseError_1 = require("../exceptions/BaseError");
let BaseErrorFilter = class BaseErrorFilter {
    catch(exception, ctx) {
        console.error('BaseErrorFilter', exception.message);
        ctx.logger.error(exception);
        ctx.response.status(400);
        ctx.response.body(exception);
    }
};
BaseErrorFilter = __decorate([
    common_1.Catch(BaseError_1.BaseError)
], BaseErrorFilter);
exports.BaseErrorFilter = BaseErrorFilter;
//# sourceMappingURL=BaseErrorFilter.js.map