"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
const common_1 = require("@tsed/common");
require("@tsed/platform-express");
const bodyParser = require("body-parser");
const compress = require("compression");
const cookieParser = require("cookie-parser");
const methodOverride = require("method-override");
require("@tsed/typeorm");
require("@tsed/swagger");
require("@tsed/passport");
require("@tsed/ajv");
const session = require("express-session");
// import * as passport from 'passport';
require("./filters/AjvErrorFilter");
require("./filters/BaseErrorFilter");
require("./filters/ErrorFilter");
const UserInfo_1 = require("./models/Auth/UserInfo");
const ormConfig = require("../ormconfig");
const mySqlStore = require("express-mysql-session");
const path = require("path");
const envConfig = require("../envConfig");
const expressBerearToken = require("express-bearer-token");
const Error_1 = require("./models/Error");
const MyStoreStore = mySqlStore(session);
const rootDir = __dirname;
let Server = class Server {
    /**
     * This method let you configure the express middleware required by your application to works.
     * @returns {Server}
     */
    $beforeRoutesInit() {
        this.app
            .use(cookieParser())
            .use(compress({}))
            .use(methodOverride())
            .use(bodyParser.json())
            .use(expressBerearToken())
            .use(bodyParser.urlencoded({
            extended: true,
        }))
            .use(session({
            //若是測試環境則不要使用檔案儲存的方式，避免一直產生廢檔
            store: envConfig.TEST
                ? undefined
                : new MyStoreStore({
                    host: ormConfig.host,
                    port: ormConfig.port,
                    user: ormConfig.username,
                    password: ormConfig.password,
                    database: ormConfig.database,
                }),
            secret: 'keyboard cat',
            resave: true,
            saveUninitialized: false,
            cookie: {
                maxAge: 1000 * 60 * 60 * 24 * 365,
                secure: false,
            },
        }));
    }
    $afterRoutesInit() {
        this.app.get('/*', (req, res) => {
            res.sendFile(path.join(envConfig.ROOT_DIR, '../client/build/index.html'));
        });
    }
};
__decorate([
    common_1.Inject(),
    __metadata("design:type", common_1.PlatformApplication)
], Server.prototype, "app", void 0);
__decorate([
    common_1.Configuration(),
    __metadata("design:type", Object)
], Server.prototype, "settings", void 0);
Server = __decorate([
    common_1.Configuration({
        port: envConfig.PORT,
        rootDir,
        statics: {
            '/': {
                root: `${envConfig.ROOT_DIR}/../client/build`,
                index: false,
            },
        },
        ajv: {},
        typeorm: [require('../ormconfig.js')],
        swagger: [
            {
                path: '/docs',
                specVersion: '3.0.3',
                spec: {
                    tags: [{ name: 'Web', description: '整合系統網頁端用Api' }],
                    info: {
                        title: 'title',
                        version: '1.0.0',
                        description: `### ErrorCodeList\n${Error_1.getErrorList()
                            .map(x => `- \`${x.errCode}\`${x.errMessage}`)
                            .join('\n')}`,
                    },
                    components: {
                        securitySchemes: {
                            bearerAuth: {
                                type: 'http',
                                scheme: 'bearer',
                            },
                        },
                    },
                },
            },
        ],
        mount: {
            '/api': '${rootDir}/controllers/**/*.ts',
            '/': '${rootDir}/webControllers/**/*.ts',
        },
        componentsScan: ['${rootDir}/protocols/*{.ts,.js}'],
        passport: {
            userInfoModel: UserInfo_1.AuthUserInfo,
        },
    })
], Server);
exports.Server = Server;
//# sourceMappingURL=Server.js.map