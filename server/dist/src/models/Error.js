"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getErrorList = exports.ErrorCode = void 0;
var ErrorCode;
(function (ErrorCode) {
    ErrorCode[ErrorCode["SUCCESS"] = 200] = "SUCCESS";
    ErrorCode[ErrorCode["AJV_VALID_ERROR"] = 1000] = "AJV_VALID_ERROR";
    ErrorCode[ErrorCode["API_TOKEN_FAILED"] = 10000] = "API_TOKEN_FAILED";
    ErrorCode[ErrorCode["API_TOKEN_EXPIRE"] = 10001] = "API_TOKEN_EXPIRE";
    ErrorCode[ErrorCode["API_KEY_NOT_FOUND"] = 10002] = "API_KEY_NOT_FOUND";
    ErrorCode[ErrorCode["AUTH_FAILED"] = 20000] = "AUTH_FAILED";
    ErrorCode[ErrorCode["AUTH_TOKEN_FAILED"] = 20001] = "AUTH_TOKEN_FAILED";
    ErrorCode[ErrorCode["REGISTER_EMAIL_IS_EXIST"] = 30000] = "REGISTER_EMAIL_IS_EXIST";
    ErrorCode[ErrorCode["REGISTER_PHONE_IS_EXIST"] = 30001] = "REGISTER_PHONE_IS_EXIST";
    ErrorCode[ErrorCode["REGISTER_USERNAME_IS_EXIST"] = 30002] = "REGISTER_USERNAME_IS_EXIST";
    ErrorCode[ErrorCode["REGISTER_TWO_PASSWORD_FAILED"] = 30003] = "REGISTER_TWO_PASSWORD_FAILED";
    ErrorCode[ErrorCode["REGISTER_SMS_VALID_FAILED"] = 30004] = "REGISTER_SMS_VALID_FAILED";
    ErrorCode[ErrorCode["FORGET_USERNAME_NOT_FOUND"] = 30100] = "FORGET_USERNAME_NOT_FOUND";
    ErrorCode[ErrorCode["FORGET_EMAIL_NOT_FOUND"] = 30101] = "FORGET_EMAIL_NOT_FOUND";
    ErrorCode[ErrorCode["FORGET_PHONE_NOT_FOUND"] = 30102] = "FORGET_PHONE_NOT_FOUND";
    ErrorCode[ErrorCode["FORGET_SMS_CODE_FAILED"] = 30103] = "FORGET_SMS_CODE_FAILED";
    ErrorCode[ErrorCode["FORGET_EMAIL_TOKEN_FAILED"] = 30104] = "FORGET_EMAIL_TOKEN_FAILED";
    ErrorCode[ErrorCode["USER_NOT_FOUND"] = 40000] = "USER_NOT_FOUND";
    ErrorCode[ErrorCode["USER_EMAIL_IS_EXIST"] = 40001] = "USER_EMAIL_IS_EXIST";
    ErrorCode[ErrorCode["USER_PHONE_IS_EXIST"] = 40002] = "USER_PHONE_IS_EXIST";
    ErrorCode[ErrorCode["USER_USERNAME_IS_EXIST"] = 40003] = "USER_USERNAME_IS_EXIST";
    ErrorCode[ErrorCode["FILTER_NOT_FOUND"] = 50000] = "FILTER_NOT_FOUND";
    ErrorCode[ErrorCode["FILTER_PHONE_IS_EXIST"] = 50001] = "FILTER_PHONE_IS_EXIST";
    ErrorCode[ErrorCode["PEPAY_RECEIVE01_IS_EXIST"] = 60001] = "PEPAY_RECEIVE01_IS_EXIST";
    ErrorCode[ErrorCode["PEPAY_RECEIVE02_IS_EXIST"] = 60002] = "PEPAY_RECEIVE02_IS_EXIST";
    ErrorCode[ErrorCode["PEPAY_RECEIVE01_CHECK_CODE_FAILED"] = 60010] = "PEPAY_RECEIVE01_CHECK_CODE_FAILED";
    ErrorCode[ErrorCode["PEPAY_RECEIVE02_CHECK_CODE_FAILED"] = 60011] = "PEPAY_RECEIVE02_CHECK_CODE_FAILED";
    ErrorCode[ErrorCode["SMS_SEND_ERROR"] = 70000] = "SMS_SEND_ERROR";
    ErrorCode[ErrorCode["SMS_CODE_EXPIRED"] = 70001] = "SMS_CODE_EXPIRED";
    ErrorCode[ErrorCode["SMS_CODE_IS_USE"] = 70002] = "SMS_CODE_IS_USE";
    ErrorCode[ErrorCode["SMS_CODE_NOT_FOUND"] = 70003] = "SMS_CODE_NOT_FOUND";
    ErrorCode[ErrorCode["EMAIL_SEND_ERROR"] = 70010] = "EMAIL_SEND_ERROR";
    ErrorCode[ErrorCode["EMAIL_TOKEN_EXPIRED"] = 70011] = "EMAIL_TOKEN_EXPIRED";
    ErrorCode[ErrorCode["EMAIL_TOKEN_IS_USE"] = 70012] = "EMAIL_TOKEN_IS_USE";
    ErrorCode[ErrorCode["EMAIL_TOKEN_NOT_FOUND"] = 70013] = "EMAIL_TOKEN_NOT_FOUND";
})(ErrorCode = exports.ErrorCode || (exports.ErrorCode = {}));
exports.getErrorList = () => {
    return Object.values(ErrorCode)
        .filter(x => typeof x === 'number')
        .map(x => ({
        errMessage: ErrorCode[x],
        errCode: x,
    }));
};
//# sourceMappingURL=Error.js.map