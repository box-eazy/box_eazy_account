import {Property} from '@tsed/schema';

export class UserPayload {
  @Property()
  fieldName: string;
  @Property()
  fieldValue: string;
}
