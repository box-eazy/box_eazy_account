import {Service} from '@tsed/di';
import * as randToken from 'rand-token';
import {AppRepo} from '../repositorys/AppRepo';
import * as jwt from 'jsonwebtoken';
import {BaseError} from '../exceptions/BaseError';
import {AuthAppInfo} from '../models/AppAuthInfo';

@Service()
export class AppService {
  constructor(private app: AppRepo) {}
  async create(appName: string) {
    const apiKey = randToken.generate(12);
    const apiSecert = randToken.generate(16);
    return await this.app.save({
      tilte: appName,
      apiKey,
      apiSecert,
      isDisabled: false,
      createdAt: new Date(),
    });
  }

  async getAppByApiKey(apiKey: string) {
    const app = await this.app.findOne({
      apiKey,
    });
    if (app === undefined || app === null) {
      throw new BaseError(BaseError.Code.API_KEY_NOT_FOUND);
    }
    return app;
  }

  async findAll() {
    return await this.app.find();
  }

  async findOne(id: number) {
    return await this.app.findOne(id);
  }

  decodeToken(token: string) {
    const decodeData: AuthAppInfo = jwt.decode(token) as AuthAppInfo;
    if (decodeData === null) {
      throw new BaseError(BaseError.Code.API_TOKEN_FAILED);
    }
    if (decodeData.exp === undefined) {
      throw new BaseError(BaseError.Code.API_TOKEN_FAILED);
    }
    if (decodeData.exp < new Date().getTime()) {
      throw new BaseError(BaseError.Code.API_TOKEN_EXPIRE);
    }
    return decodeData;
  }

  validToken(token: string, apiSecert: string) {
    try {
      jwt.verify(token, apiSecert);
      return true;
    } catch (err) {
      return false;
    }
  }

  async encodeToken(
    appId: number,
    apiKey: string,
    apiSecert: string,
    expire = 60
  ) {
    return jwt.sign(
      {
        apiKey,
        appId,
        exp: new Date().getTime() + expire * 1000,
      },
      apiSecert,
      {
        algorithm: 'HS256',
      }
    );
  }
}
