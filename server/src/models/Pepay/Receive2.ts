import {PepayReceive1Body} from './Receive1';

export class PepayReceive2Body extends PepayReceive1Body {
  BILL_ID: string;
  DATA_ID: string;
  PAY_TYPE: string;
  USER_ID: string;
  SOURCE_AMOUNT: number;
  DATA_CODE: number;
  TRADE_CODE: number;
  CDATE: string;
  CTIME: string;
  BILL_DATE: string;
  BILL_TIME: string;
  DATE: string;
  TIME: string;
}

export class PepayReceive2Result {}
