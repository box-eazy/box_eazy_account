import {GenerateOrderId} from './orderId';
import {format} from 'date-fns';

const generate = new GenerateOrderId('EAZY', 'yyyyMMdd', 10);
const mockId = `EAZY${format(new Date(), 'yyyyMMdd')}0000033456`;
describe('generator orderId', () => {
  it('generateOrder', () => {
    expect(generate.generateOrderId(33456)).toBe(mockId);
  });

  it('getOrderNum', () => {
    expect(generate.getOrderNum(mockId)).toBe(33456);
  });
});
