import {EntityRepository} from 'typeorm';
import {EmailLog} from '../entities/EmailLog';
import {BaseRepo} from './BaseRepo';

@EntityRepository(EmailLog)
export class EmailRepo extends BaseRepo<EmailLog> {}
