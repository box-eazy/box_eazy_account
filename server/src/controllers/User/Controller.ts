import {
  BodyParams,
  Controller,
  Delete,
  Get,
  Patch,
  PathParams,
  Post,
  Put,
  QueryParams,
} from '@tsed/common';
import {Description, Returns, Security, Summary} from '@tsed/schema';
import {Query} from 'typeorm/driver/Query';
import {UserBody} from '../../models/User/Body';
import {UserListItem} from '../../models/User/List';
import {PageParams} from '../../models/Common/Page';
import {BaseRes} from '../../models/Res';
import {ResponseService} from '../../services/Response';
import {UserAddBody} from '../../models/User/AddBody';
import {BearerTokenAuth} from '../../middlewares/BearerTokenAuth';
import {AuthApp} from '../../decorators/AuthApp';
import {AuthAppInfo} from '../../models/AppAuthInfo';
import {UserService} from '../../services/UserSerivce';
import {UserSearchBody} from '../../models/User/Search';
import {UserPayload} from '../../entities/UserPayload';
import {UserUpdatePayloadBody} from '../../models/User/UpdatePayloadBody';

@Controller('/users')
@BearerTokenAuth()
@Description('使用者操作相關api')
export class UserController {
  constructor(private user: UserService, private res: ResponseService) {}

  // @Summary('簡單搜尋會員列表')
  // @Returns(200, ResponseService.toPagiation(UserListItem))
  // @Get()
  // findAll(
  //   @AuthApp() app: AuthAppInfo,
  //   @QueryParams('keyword') keyword: string,
  //   @QueryParams() page: PageParams,
  //   @QueryParams('appId') appId: number
  // ) {
  //   return this.res.toPage(this.user.findAll(appId, keyword, page));
  // }

  @Summary('搜尋會員')
  @Description(`
    filters.key 命名方式
    * 一般欄位的部份直接帶入對應的columnName (EX: nickname)
    * payload的部份一律以 {payload.[fieldName]} 來定義

    filters 查詢方式
    * Array第一層皆會做 And 判斷
    * Array第二層則會做 OR 判斷
      EX:
      [
        { key: 'A', value: '1' },
        [
          { key: 'B', value: '2' },
          { key: 'C', value: '3' }
        ]
      ]
      則會產生
      A = '1' AND ( B = '2' OR C = '3')

    filter Params 說明
    {
      key: 要過濾的欄位,
      value: 要搜尋的值 (可undefined & null),
      isNot: true/false, 是否反轉查詢結果
    }

    filter.value 說明
    若value帶入 null 以及 undefined時，針對一般欄位以及payload的搜尋是不一樣的
    * 若是一般欄位，則會判斷該欄位是否為NULL
    * 若是payload, 則會判斷是否存在該欄位
    
  `)
  @Post('/search')
  @Returns(200, ResponseService.toPagiation(UserListItem))
  adFindAll(@BodyParams() data: UserSearchBody) {
    return this.res.toPage(this.user.adFindAll(data));
  }

  @Summary('透過userId取得會員資料')
  @Get('/:id')
  @Returns(200, ResponseService.toResult(UserBody))
  findOne(@PathParams('id') id: number) {
    return this.res.toResult(this.user.findOne(id));
  }

  @Summary('新增會員')
  @Post()
  @Returns(200, ResponseService.toResult(UserBody))
  add(@AuthApp() app: AuthAppInfo, @BodyParams() data: UserAddBody) {
    return this.res.toResult(this.user.create(app.appId, data));
  }

  @Summary('修改會員資料(部份修改)')
  @Patch('/:id')
  @Returns(200, ResponseService.toResult(UserBody))
  edit(@PathParams('id') id: number, @BodyParams() data: UserAddBody) {
    return this.res.toResult(this.user.patchUpdate(id, data));
  }

  @Summary('刪除會員資料')
  @Delete('/:id')
  @Returns(200, ResponseService.toResult(UserBody))
  delete(@PathParams('id') id: number) {
    return this.res.toResult(this.user.delete(id));
  }

  @Summary('更新會員的payload (完全覆蓋更新)')
  @Put('/:id/payload')
  @Returns(200, ResponseService.toResult(UserBody))
  updatePayloadPut(
    @AuthApp() app: AuthAppInfo,
    @PathParams('id') id: number,
    @BodyParams() data: UserUpdatePayloadBody
  ) {
    return this.res.toResult(
      this.user.updateMultiPayload(app.appId, id, data, true)
    );
  }

  @Summary('更新會員的payload(僅新增/更新Body內的欄位)')
  @Patch('/:id/payload')
  @Returns(200, ResponseService.toResult(UserBody))
  updatePayloadPatch(
    @AuthApp() app: AuthAppInfo,
    @PathParams('id') id: number,
    @BodyParams() data: UserUpdatePayloadBody
  ) {
    return this.res.toResult(
      this.user.updateMultiPayload(app.appId, id, data, false)
    );
  }
  @Summary('修改會員特定的Payload(若不存在則新增)')
  @Put('/:id/payload/:fieldName')
  @Returns(200, ResponseService.toResult(UserBody))
  updatePayload(
    @AuthApp() app: AuthAppInfo,
    @PathParams('id') id: number,
    @PathParams('fieldName') fieldName: string,
    @BodyParams('fieldValue') fieldValue: string
  ) {
    return this.res.toResult(
      this.user.updatePayload(app.appId, id, fieldName, fieldValue)
    );
  }

  @Summary('刪除指定的Payload')
  @Delete('/:id/payload/:fieldName')
  deletePayload(
    @AuthApp() app: AuthAppInfo,
    @PathParams('id') id: number,
    @PathParams('fieldName') fieldName: string
  ) {
    return this.res.toResult(this.user.deletePayload(app.appId, id, fieldName));
  }
}
