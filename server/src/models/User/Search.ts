import {
  Any,
  AnyOf,
  array,
  ArrayOf,
  CollectionOf,
  Description,
  Example,
  For,
  Ignore,
  object,
  OneOf,
  Property,
  Schema,
  SpecTypes,
  string,
} from '@tsed/schema';
import {BaseModel} from '../Common/BaseModel';
import {FilterParam} from '../Common/Filter';
import {OrderByParam} from '../Common/OrderBy';
import {PageParams} from '../Common/Page';

type FilterType = FilterParam | FilterParam[];

export class UserSearchBody extends BaseModel<UserSearchBody> {
  @Property(PageParams)
  page: PageParams;

  @CollectionOf('number')
  userIds: number[];

  @Property()
  keyword: string;

  @Description(`
    Array第一層皆會做 And 判斷
    Array第二層則會做 OR 判斷
    EX:
    [
      { key: 'A', value: '1' },
      [
        { key: 'B', value: '2' },
        { key: 'C', value: '3' }
      ]
    ]
    則會產生
    A = '1' AND ( B = '2' OR C = '3')
`)
  @Any(Object, Array)
  @Example('[{"key": "", "value":""}]')
  filters: (FilterParam | FilterParam[])[];

  @Property(OrderByParam)
  @Description('可針對目前有的欄位進行比對')
  orderBy: OrderByParam;
}
