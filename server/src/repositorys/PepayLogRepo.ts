import {EntityRepository} from 'typeorm';
import {PepayLog} from '../entities/PepayLog';
import {PepayReceive1Log} from '../entities/PepayReceive1Log';
import {PepayReceive2Log} from '../entities/PepayReceive2Log';
import {BaseRepo} from './BaseRepo';

@EntityRepository(PepayLog)
export class PepayLogRepo extends BaseRepo<PepayLog> {}

@EntityRepository(PepayReceive1Log)
export class PepayReceive1LogRepo extends BaseRepo<PepayReceive1Log> {}

@EntityRepository(PepayReceive2Log)
export class PepayReceive2LogRepo extends BaseRepo<PepayReceive2Log> {}
