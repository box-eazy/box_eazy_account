export class BaseModel<T = any> {
  constructor(init?: Partial<T>) {
    Object.assign(this, init);
  }
}
