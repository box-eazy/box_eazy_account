"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@tsed/common");
const cryptoJs = require("crypto-js");
const typeorm_1 = require("typeorm");
const User_1 = require("../entities/User");
const UserPayload_1 = require("../entities/UserPayload");
const BaseError_1 = require("../exceptions/BaseError");
const AddBody_1 = require("../models/User/AddBody");
const Body_1 = require("../models/User/Body");
const UpdatePayloadBody_1 = require("../models/User/UpdatePayloadBody");
const UserPayloadRepo_1 = require("../repositorys/UserPayloadRepo");
const UserRepo_1 = require("../repositorys/UserRepo");
const BaseSerivce_1 = require("./BaseSerivce");
let UserService = class UserService extends BaseSerivce_1.BaseService {
    constructor(user, payload) {
        super();
        this.user = user;
        this.payload = payload;
    }
    cryptoPassword(password) {
        return cryptoJs.SHA256(password).toString();
    }
    async delete(userId) {
        await this.user.update({ id: userId }, { deletedAt: new Date() });
        return this.findOne(userId);
    }
    async findAll(data) {
        return this.user.search(data);
    }
    async adFindAll(search) {
        return this.user.search(search);
    }
    async findOne(id) {
        const dbData = await this.user.findOne({
            where: {
                id,
                deletedAt: typeorm_1.IsNull(),
            },
            relations: ['payloads'],
        });
        if (dbData === null || dbData === undefined) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_NOT_FOUND);
        }
        return new Body_1.UserBody(dbData);
    }
    async create(appId, data, manager) {
        if (await this.user.exist({ email: data.email })) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_EMAIL_IS_EXIST);
        }
        if (await this.user.exist({ phone: data.phone })) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_PHONE_IS_EXIST);
        }
        if (await this.user.exist({ username: data.username })) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_USERNAME_IS_EXIST);
        }
        const dbData = new User_1.User({
            appId,
            createdAt: new Date(),
            email: data.email,
            username: data.username,
            password: this.cryptoPassword(data.password),
            nickname: data.nickname,
            phone: data.phone,
            isEmailValid: data.isEmailValid,
            isSmsValid: data.isSmsValid,
            image: data.image,
        });
        await manager.save(dbData);
        const payloadList = data.payloads.map(x => new UserPayload_1.UserPayload({
            appId,
            userId: dbData.id,
            fieldName: x.fieldName,
            fieldValue: x.fieldValue,
        }));
        await manager.save(payloadList);
        return new Body_1.UserBody({
            ...dbData,
            payloads: payloadList,
        });
    }
    async patchUpdate(id, data, manager) {
        let dbData = await this.user.findOne(id, { relations: ['payloads'] });
        if (dbData === undefined) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_NOT_FOUND);
        }
        const { payloads, ...userData } = data;
        if (userData.password) {
            userData.password = this.cryptoPassword(userData.password);
        }
        dbData = Object.assign(dbData, userData);
        await manager.save(dbData);
        if (payloads !== undefined) {
            const oldPayloads = dbData.payloads;
            const newPayloads = payloads.map(x => new UserPayload_1.UserPayload(x));
            const diff = this.payload.differenceList(oldPayloads, newPayloads, 'fieldName');
            await manager.delete(UserPayload_1.UserPayload, {
                id: typeorm_1.In(diff.remove.map(x => x.id)),
            });
            await manager.save(diff.update);
            await manager.save(diff.add);
        }
        return new Body_1.UserBody(await manager.findOne(User_1.User, id, {
            relations: ['payloads'],
        }));
    }
    async updateMultiPayload(appId, userId, data, isOverwrite, manager) {
        if (isOverwrite) {
            await manager.delete(UserPayload_1.UserPayload, {
                userId,
            });
        }
        else {
            await manager.delete(UserPayload_1.UserPayload, {
                fieldName: typeorm_1.In(data.items.map(x => x.fieldName)),
            });
        }
        if (data.items) {
            const userPayloadList = data.items.map(x => new UserPayload_1.UserPayload({
                ...x,
                userId,
                appId,
            }));
            await manager.save(userPayloadList);
        }
        const userData = await manager.findOne(User_1.User, userId, {
            relations: ['payloads'],
        });
        return new Body_1.UserBody(userData);
    }
    async updatePayload(appId, userId, fieldName, fieldValue) {
        if (await this.payload.exist({ fieldName, userId })) {
            await this.payload.update({ fieldName, userId }, { fieldValue: fieldValue });
        }
        else {
            await this.payload.save({
                appId,
                fieldName,
                fieldValue,
                userId,
            });
        }
        return await this.findOne(userId);
    }
    async deletePayload(appId, userId, fieldName) {
        await this.payload.delete({
            fieldName,
            userId,
        });
        return await this.findOne(userId);
    }
};
__decorate([
    typeorm_1.Transaction(),
    __param(2, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, AddBody_1.UserAddBody,
        typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "create", null);
__decorate([
    typeorm_1.Transaction(),
    __param(2, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "patchUpdate", null);
__decorate([
    typeorm_1.Transaction(),
    __param(4, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number, UpdatePayloadBody_1.UserUpdatePayloadBody, Boolean, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], UserService.prototype, "updateMultiPayload", null);
UserService = __decorate([
    common_1.Service(),
    __metadata("design:paramtypes", [UserRepo_1.UserRepo, UserPayloadRepo_1.UserPayloadRepo])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=UserSerivce.js.map