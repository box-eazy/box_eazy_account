"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Application = void 0;
const schema_1 = require("@tsed/schema");
const typeorm_1 = require("typeorm");
const Base_1 = require("./Common/Base");
const randToken = require("rand-token");
let Application = class Application extends Base_1.CommonBase {
    static generateRandomToken(len) {
        return randToken.generate(len);
    }
};
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], Application.prototype, "name", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], Application.prototype, "apiKey", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], Application.prototype, "apiSecert", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Boolean)
], Application.prototype, "isDisabled", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    schema_1.Description('是否納入統計'),
    __metadata("design:type", Boolean)
], Application.prototype, "isCreditStats", void 0);
Application = __decorate([
    typeorm_1.Entity()
], Application);
exports.Application = Application;
//# sourceMappingURL=Application.js.map