import {Property} from '@tsed/schema';
import {BaseModel} from '../Common/BaseModel';

export class PepayReceive1Body extends BaseModel<PepayReceive1Body> {
  @Property()
  SHOP_ID: string;
  @Property()
  ORDER_ID: string;
  @Property()
  SESS_ID: string;
  @Property()
  PROD_ID: string;
  @Property()
  AMOUNT: number;
  @Property()
  CURRENCY: string;
  @Property()
  SHOP_PARA: string;
  @Property()
  CHECK_CODE: string;
}

export class PepayReceive1Result extends BaseModel<PepayReceive1Result> {
  @Property()
  USER_ID: string;

  @Property()
  RES_CODE: number;

  @Property()
  SHOP_PARA: string;

  @Property()
  RET_URL: string;
}
