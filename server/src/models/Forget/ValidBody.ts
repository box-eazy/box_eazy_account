import {Description, Property} from '@tsed/schema';

export class ForgetValidBody {
  @Property()
  apiKey: string;

  @Property()
  username: string;

  @Property()
  email: string;

  @Property()
  phone: string;

  @Property()
  smsCode: string;

  @Property()
  @Description('email驗證URL')
  validUrl: string;

  @Property()
  @Description('驗證成功導向Url')
  redirectUrl: string;
}
