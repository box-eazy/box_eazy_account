import {Catch, ExceptionFilterMethods, PlatformContext} from '@tsed/common';
import {Exception} from '@tsed/exceptions';
import * as envConfig from '../../envConfig';

@Catch(Error)
export class BaseErrorFilter implements ExceptionFilterMethods {
  catch(exception: Exception, ctx: PlatformContext) {
    console.error('Unknown Error', exception);
    ctx.logger.error(exception);
    ctx.response.status(500);
    if (envConfig.DEV) {
      ctx.response.body(exception.toString());
    }
  }
}
