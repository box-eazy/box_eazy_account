"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const Application_1 = require("../../entities/Application");
const AppService_1 = require("../../services/AppService");
const Response_1 = require("../../services/Response");
let AppController = class AppController {
    constructor(app, res) {
        this.app = app;
        this.res = res;
    }
    async create(name) {
        return this.res.toResult(this.app.create(name));
    }
    findAll() {
        return this.res.toItems(this.app.findAll());
    }
};
__decorate([
    common_1.Post(),
    schema_1.Summary('Create App'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Application_1.Application)),
    __param(0, common_1.BodyParams('name')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AppController.prototype, "create", null);
__decorate([
    common_1.Get(),
    schema_1.Summary('FindAll'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], AppController.prototype, "findAll", null);
AppController = __decorate([
    common_1.Controller('/apps'),
    __metadata("design:paramtypes", [AppService_1.AppService, Response_1.ResponseService])
], AppController);
exports.AppController = AppController;
//# sourceMappingURL=Controller.js.map