import EazyCommon from '@eazy/widget';
import {useModal} from '@eazy/widget/Components/Modal/Context';
import {useCallback, useState} from 'react';
import {SendSMSApi} from '../Api/SendSMS';
import {getApiErrorMessage} from '../libs/handleError';

export const SendSMSButton: React.FC<{
  phone: string;
  type: 'REGISTER' | 'FORGET';
  apiKey: string;
}> = ({phone, type, apiKey}) => {
  const modal = useModal();
  const [loading, setLoading] = useState(false);
  const $SendSMS = useCallback(async () => {
    setLoading(true);
    try {
      await SendSMSApi(apiKey, phone, type);
      await modal.alert('發送成功，麻煩請查看簡訊確認驗證碼');
    } catch (err) {
      await modal.alert(getApiErrorMessage(err));
    }
    setLoading(false);
  }, [phone, apiKey, type]);
  return (
    <EazyCommon.Button
      size="sm"
      color="link"
      disabled={phone.length !== 10 || loading}
      onClick={$SendSMS}
      type="button"
    >
      發送簡訊驗證碼
    </EazyCommon.Button>
  );
};
