import {Connection} from 'typeorm';
import {Factory, Seeder} from 'typeorm-seeding';
import {Application} from '../entities/Application';
import * as randToken from 'rand-token';
export default class InitSeed implements Seeder {
  public async run(factory: Factory, connection: Connection) {
    /* #region Generate 4 Role Data  */
    const now = new Date();
    await connection
      .createQueryBuilder()
      .insert()
      .into(Application)
      .values([
        {
          id: 1,
          name: '黃頁',
          isCreditStats: true,
          isDisabled: false,
          createdAt: now,
          apiKey: Application.generateRandomToken(12),
          apiSecert: Application.generateRandomToken(16),
        },
        {
          id: 2,
          name: 'CallBible',
          isCreditStats: true,
          isDisabled: false,
          createdAt: now,
          apiKey: Application.generateRandomToken(12),
          apiSecert: Application.generateRandomToken(16),
        },
        {
          id: 3,
          name: 'EazyDating',
          isCreditStats: true,
          isDisabled: false,
          createdAt: now,
          apiKey: Application.generateRandomToken(12),
          apiSecert: Application.generateRandomToken(16),
        },
        {
          id: 4,
          name: 'Eazy總後台',
          isCreditStats: false,
          isDisabled: false,
          createdAt: now,
          apiKey: Application.generateRandomToken(12),
          apiSecert: Application.generateRandomToken(16),
        },
      ])
      .execute();
  }
}
