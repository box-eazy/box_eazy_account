import {Property} from '@tsed/schema';
import {BaseModel} from '../Common/BaseModel';

export class BasicAuthInfo extends BaseModel<BasicAuthInfo> {
  @Property()
  userId: number;
}

export class WebAuthInfo extends BaseModel<WebAuthInfo> {
  @Property()
  token: string;

  userId: number;
}
