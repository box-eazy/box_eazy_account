"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const AppService_1 = require("../services/AppService");
let DevController = class DevController {
    constructor(app) {
        this.app = app;
    }
    encodeToken(appId, apiKey, apiSecret, expire) {
        return this.app.encodeToken(appId, apiKey, apiSecret, expire);
    }
    decodeToken(token, apiKey) {
        return this.app.decodeToken(token).apiKey === apiKey ? 'SUCCESS' : 'FAIL';
    }
    async getToken(appId) {
        const dbData = await this.app.findOne(appId);
        return this.app.encodeToken(dbData.id, dbData.apiKey, dbData.apiSecert, 1440 * 60);
    }
};
__decorate([
    common_1.Post('/encode_token'),
    __param(0, common_1.BodyParams('appId')),
    __param(1, common_1.BodyParams('apiKey')),
    __param(2, common_1.BodyParams('apiSecret')),
    __param(3, common_1.BodyParams('expire')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, String, Number]),
    __metadata("design:returntype", void 0)
], DevController.prototype, "encodeToken", null);
__decorate([
    common_1.Post('/decode_token'),
    __param(0, common_1.BodyParams('token')),
    __param(1, common_1.BodyParams('apiKey')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", void 0)
], DevController.prototype, "decodeToken", null);
__decorate([
    common_1.Post('/get_token'),
    __param(0, common_1.BodyParams('appId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], DevController.prototype, "getToken", null);
DevController = __decorate([
    common_1.Controller('/dev'),
    schema_1.Tags('Dev'),
    schema_1.Description('開發輔助用 (正式上線時需禁用)'),
    __metadata("design:paramtypes", [AppService_1.AppService])
], DevController);
exports.DevController = DevController;
//# sourceMappingURL=DevController.js.map