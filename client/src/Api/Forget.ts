import {baseAxios} from './Base';

export const ForgetApi = async (apiKey: string, data: any) => {
  await baseAxios.request({
    url: '/web/forget',
    method: 'POST',
    data: {
      ...data,
      apiKey,
    },
  });
};

export const ResetPasswordApi = async (token: string, data: any) => {
  const res = await baseAxios.request<{
    result: {redirectUrl: string; apiKey: number};
  }>({
    url: `/web/forget/${token}`,
    method: 'POST',
    data,
  });

  return res.data.result;
};
