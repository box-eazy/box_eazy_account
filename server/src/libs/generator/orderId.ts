import {format} from 'date-fns';
import {padStart} from 'lodash';
export class GenerateOrderId {
  private prefixLength: number;
  constructor(
    private name: string,
    private dateFormat: string,
    private orderNumLength: number
  ) {
    this.prefixLength = name.length + dateFormat.length;
  }

  private formatDate(date?: Date) {
    return format(date === undefined ? new Date() : date, this.dateFormat);
  }

  getOrderNum(orderId: string) {
    return parseInt(orderId.slice(this.prefixLength));
  }

  generateOrderId(orderNum: number, date?: Date) {
    return `${this.name}${this.formatDate(date)}${padStart(
      orderNum.toString(),
      this.orderNumLength,
      '0'
    )}`;
  }

  getPrefix(date?: Date) {
    return `${this.name}${this.formatDate(date)}`;
  }
}
