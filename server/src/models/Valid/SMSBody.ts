import {Enum, Property} from '@tsed/schema';
import {ValidTemp} from './Temp';

export class SMSBody {
  @Property()
  phone: string;
  @Property()
  @Enum(ValidTemp)
  temp: ValidTemp;
}

export class SMSValidBody {
  @Property()
  smsCode: string;
}
