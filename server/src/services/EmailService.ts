import {Service} from '@tsed/di';
import {EntityManager, Transaction, TransactionManager} from 'typeorm';
import {EmailLog} from '../entities/EmailLog';
import {User} from '../entities/User';
import {EmailRepo} from '../repositorys/EmailRepo';
import {UserRepo} from '../repositorys/UserRepo';
import {EmailTemp} from '../models/Email/Temp';
import * as fs from 'fs-extra';
import {ValidTemp} from '../models/Valid/Temp';
import {BaseError} from '../exceptions/BaseError';
import {addMinutes, format} from 'date-fns';
import {ValidEmailAddBody} from '../models/Valid/EmailBody';
import * as randToken from 'rand-token';
import * as nodemailer from 'nodemailer';
import * as envConfig from '../../envConfig';

@Service()
export class EmailService {
  constructor(private user: UserRepo, private email: EmailRepo) {}

  private sendEmail(email: string, subject: string, content: string) {
    const transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: envConfig.EMAIL_ACCOUNT,
        pass: envConfig.EMAIL_PASSWORD,
      },
    });

    return transporter.sendMail({
      from: envConfig.EMAIL_ACCOUNT,
      to: email,
      subject,
      html: content,
    });
  }

  generateToken() {}

  private loadSubject(temp: ValidTemp) {
    if (temp === ValidTemp.FOREGT) {
      return 'EAZY會員重設密碼Email驗證';
    } else {
      return 'EAZY會員Email驗證';
    }
  }

  private loadTemplate(temp: ValidTemp, params: any) {
    let html = fs
      .readFileSync(`${process.cwd()}/static/emailTemp/${temp}.html`)
      .toString();
    for (const key in params) {
      html = html.replace(`[${key}]`, params[key]);
    }
    return html;
  }

  async getByToken(token: string) {
    const dbData = await this.email.findOne({
      token,
    });

    if (dbData === undefined || dbData === null) {
      throw new BaseError(BaseError.Code.EMAIL_TOKEN_NOT_FOUND);
    }

    if (dbData.isUse) {
      throw new BaseError(BaseError.Code.EMAIL_TOKEN_IS_USE);
    }
    if (dbData.expired < new Date()) {
      throw new BaseError(BaseError.Code.EMAIL_TOKEN_EXPIRED);
    }

    return dbData;
  }

  async $sendValidEmail(
    appId: number,
    data: ValidEmailAddBody,
    manager?: EntityManager
  ) {
    const {email, temp, redirectUrl, validUrl} = data;
    const token = randToken.generate(20);

    const dbUser = await manager.findOne(User, {email});

    if (dbUser === undefined || dbUser === null) {
      throw new BaseError(BaseError.Code.USER_NOT_FOUND);
    }

    const expired = addMinutes(new Date(), 60);
    const template = this.loadTemplate(temp, {
      EXPIRED: format(expired, 'yyyy-MM-dd HH:mm'),
      VALID_URL: validUrl,
      TOKEN: token,
    });

    await manager.save(dbUser);

    await manager.save(
      new EmailLog({
        appId,
        content: template,
        email: dbUser.email,
        expired,
        token,
        isUse: false,
        createdAt: new Date(),
        temp,
        validUrl,
        redirectUrl,
      })
    );

    try {
      await this.sendEmail(email, this.loadSubject(temp), template);
    } catch (err) {
      console.log(err);
      throw new BaseError(BaseError.Code.EMAIL_SEND_ERROR);
    }
  }

  @Transaction()
  async sendValidEmail(
    appId: number,
    data: ValidEmailAddBody,
    @TransactionManager() manager?: EntityManager
  ) {
    return this.$sendValidEmail(appId, data, manager);
  }

  async $validToken(token: string, manager?: EntityManager) {
    const dbData = await manager.findOne(EmailLog, {
      token,
    });

    if (dbData === null || dbData === undefined) {
      throw new BaseError(BaseError.Code.EMAIL_TOKEN_NOT_FOUND);
    }
    if (dbData.isUse) {
      throw new BaseError(BaseError.Code.EMAIL_TOKEN_IS_USE);
    }
    if (dbData.expired < new Date()) {
      throw new BaseError(BaseError.Code.EMAIL_TOKEN_EXPIRED);
    }
    dbData.isUse = true;
    await manager.save(dbData);
    return dbData;
  }

  @Transaction()
  async validToken(
    token: string,
    @TransactionManager() manager?: EntityManager
  ) {
    return this.$validToken(token, manager);
  }
}
