import {BodyParams, Controller, Post} from '@tsed/common';
import {Description, Tags} from '@tsed/schema';
import {AppService} from '../services/AppService';

@Controller('/dev')
@Tags('Dev')
@Description('開發輔助用 (正式上線時需禁用)')
export class DevController {
  constructor(private app: AppService) {}

  @Post('/encode_token')
  encodeToken(
    @BodyParams('appId') appId: number,
    @BodyParams('apiKey') apiKey: string,
    @BodyParams('apiSecret') apiSecret: string,
    @BodyParams('expire') expire: number
  ) {
    return this.app.encodeToken(appId, apiKey, apiSecret, expire);
  }

  @Post('/decode_token')
  decodeToken(
    @BodyParams('token') token: string,
    @BodyParams('apiKey') apiKey: string
  ) {
    return this.app.decodeToken(token).apiKey === apiKey ? 'SUCCESS' : 'FAIL';
  }

  @Post('/get_token')
  async getToken(@BodyParams('appId') appId: number) {
    const dbData = await this.app.findOne(appId);
    return this.app.encodeToken(
      dbData.id,
      dbData.apiKey,
      dbData.apiSecert,
      1440 * 60
    );
  }
}
