import {EntityRepository} from 'typeorm';
import {SMSLog} from '../entities/SMSLog';
import {BaseRepo} from './BaseRepo';

@EntityRepository(SMSLog)
export class SMSRepo extends BaseRepo<SMSLog> {}
