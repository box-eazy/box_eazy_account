"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const EmailLog_1 = require("../../entities/EmailLog");
const Basic_1 = require("../../models/Auth/Basic");
const ForgetResult_1 = require("../../models/Auth/ForgetResult");
const Info_1 = require("../../models/Auth/Info");
const ResetBody_1 = require("../../models/Forget/ResetBody");
const ValidBody_1 = require("../../models/Forget/ValidBody");
const Body_1 = require("../../models/Register/Body");
const Temp_1 = require("../../models/Valid/Temp");
const AppService_1 = require("../../services/AppService");
const AuthService_1 = require("../../services/AuthService");
const EmailService_1 = require("../../services/EmailService");
const Response_1 = require("../../services/Response");
const SMSService_1 = require("../../services/SMSService");
const UserSerivce_1 = require("../../services/UserSerivce");
let WebController = class WebController {
    constructor(user, res, sms, auth, app, email) {
        this.user = user;
        this.res = res;
        this.sms = sms;
        this.auth = auth;
        this.app = app;
        this.email = email;
    }
    async login(data, session) {
        const res = await this.auth.webLogin(data.apiKey, data.username, data.password);
        session.user = {
            id: res.userId,
        };
        return this.res.toResult(res);
    }
    async loginByParams(apiKey, provider, email, session) {
        const res = await this.auth.webProviderLogin(apiKey, provider, email);
        session.user = {
            id: res.userId,
        };
        return this.res.toResult(res);
    }
    async register(data) {
        const app = await this.app.getAppByApiKey(data.apiKey);
        return this.res.toResult(this.auth.register(app.id, app.apiKey, data));
    }
    async forgetValidData(data) {
        const app = await this.app.getAppByApiKey(data.apiKey);
        await this.auth.sendForgetEmail(app.id, data);
        return this.res.Ok();
    }
    forgetResetPassword(data, token) {
        return this.res.toResult(this.auth.resetPassword(token, data));
    }
    validForgetToken(token) {
        return this.res.toResult(this.auth.validForgetToken(token));
    }
    async sendSMS(apiKey, phone, type) {
        const app = await this.app.getAppByApiKey(apiKey);
        await this.sms.sendSMSCode(app.id, phone, type);
        return this.res.Ok();
    }
    async sendEmail(apiKey, email, type, redirectUrl, validUrl) {
        const app = await this.app.getAppByApiKey(apiKey);
        await this.email.sendValidEmail(app.id, {
            email,
            temp: type,
            redirectUrl,
            validUrl,
        });
        return this.res.Ok();
    }
    async validToken(token) {
        return this.res.toResult(this.email.validToken(token));
    }
};
__decorate([
    common_1.Post('/auth'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Info_1.WebAuthInfo)),
    schema_1.Summary('會員登入'),
    __param(0, common_1.BodyParams()), __param(1, common_1.Session()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Basic_1.WebAuthBody, Object]),
    __metadata("design:returntype", Promise)
], WebController.prototype, "login", null);
__decorate([
    common_1.Post('/auth/provider'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Info_1.WebAuthInfo)),
    schema_1.Summary('FB / Google登入'),
    __param(0, common_1.BodyParams('apiKey')),
    __param(1, common_1.BodyParams('provider')),
    __param(2, common_1.BodyParams('email')),
    __param(3, common_1.Session()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, Object]),
    __metadata("design:returntype", Promise)
], WebController.prototype, "loginByParams", null);
__decorate([
    common_1.Post('/register'),
    schema_1.Summary('會員註冊'),
    __param(0, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Body_1.RegisterBody]),
    __metadata("design:returntype", Promise)
], WebController.prototype, "register", null);
__decorate([
    common_1.Post('/forget'),
    schema_1.Summary('忘記密碼'),
    schema_1.Returns(200, Response_1.ResponseService.Base),
    __param(0, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [ValidBody_1.ForgetValidBody]),
    __metadata("design:returntype", Promise)
], WebController.prototype, "forgetValidData", null);
__decorate([
    common_1.Post('/forget/:token'),
    schema_1.Summary('reset password'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(ForgetResult_1.ForgetResult)),
    __param(0, common_1.BodyParams()),
    __param(1, common_1.PathParams('token')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [ResetBody_1.ForgetResetBody, String]),
    __metadata("design:returntype", void 0)
], WebController.prototype, "forgetResetPassword", null);
__decorate([
    common_1.Get('/forget/:token'),
    schema_1.Summary('valid token'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(ForgetResult_1.ForgetResult)),
    __param(0, common_1.PathParams('token')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], WebController.prototype, "validForgetToken", null);
__decorate([
    common_1.Post('/sms'),
    schema_1.Summary('Send SMS'),
    schema_1.Returns(200, Response_1.ResponseService.Base),
    __param(0, common_1.BodyParams('apiKey')),
    __param(1, common_1.BodyParams('phone')),
    __param(2, common_1.BodyParams('type')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String]),
    __metadata("design:returntype", Promise)
], WebController.prototype, "sendSMS", null);
__decorate([
    common_1.Post('/email'),
    schema_1.Summary('Send Email'),
    __param(0, common_1.BodyParams('apiKey')),
    __param(1, common_1.BodyParams('email')),
    __param(2, common_1.BodyParams('type')),
    __param(3, common_1.BodyParams('redirectUrl')),
    __param(4, common_1.BodyParams('validUrl')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String]),
    __metadata("design:returntype", Promise)
], WebController.prototype, "sendEmail", null);
__decorate([
    common_1.Post('/email/:token'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(EmailLog_1.EmailLog)),
    __param(0, common_1.PathParams('token')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], WebController.prototype, "validToken", null);
WebController = __decorate([
    common_1.Controller('/web'),
    schema_1.Tags('Web'),
    schema_1.Description('整合系統網頁用Api'),
    __metadata("design:paramtypes", [UserSerivce_1.UserService,
        Response_1.ResponseService,
        SMSService_1.SMSService,
        AuthService_1.AuthService,
        AppService_1.AppService,
        EmailService_1.EmailService])
], WebController);
exports.WebController = WebController;
//# sourceMappingURL=Controller.js.map