import {Context, IMiddleware, Middleware, Req, UseAuth} from '@tsed/common';
import {useDecorators} from '@tsed/core';
import {Returns, Security} from '@tsed/schema';
import {BaseError} from '../exceptions/BaseError';
import {BaseRes} from '../models/Res';
import {AppService} from '../services/AppService';

@Middleware()
export class BearerTokenAuthMiddleware implements IMiddleware {
  constructor(private app: AppService) {}
  public use(@Req() req: Req, @Context() ctx: Context) {
    const tokenData = this.app.decodeToken(req.token);
    req.user = tokenData;
  }
}

export function BearerTokenAuth() {
  return useDecorators(
    UseAuth(BearerTokenAuthMiddleware),
    Security('bearerAuth'),
    Returns(401, BaseRes).Description('Unauthorized')
  );
}
