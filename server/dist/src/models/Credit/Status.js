"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreditStatus = void 0;
var CreditStatus;
(function (CreditStatus) {
    CreditStatus["CREATED"] = "CREATED";
    CreditStatus["WAITING"] = "WATING";
    CreditStatus["SUCCESS"] = "SUCCESS";
    CreditStatus["FAILED"] = "FAILED";
})(CreditStatus = exports.CreditStatus || (exports.CreditStatus = {}));
//# sourceMappingURL=Status.js.map