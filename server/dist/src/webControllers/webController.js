"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebViewController = void 0;
const common_1 = require("@tsed/common");
const fs = require("fs");
const envConfig_1 = require("../../envConfig");
const path = require("path");
const AuthService_1 = require("../services/AuthService");
let SendIndexFile = class SendIndexFile {
    use(res) {
        console.log('send indexFile');
        return res.sendFile(path.join(envConfig_1.ROOT_DIR, '../client/build/index.html'));
    }
};
__decorate([
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SendIndexFile.prototype, "use", null);
SendIndexFile = __decorate([
    common_1.Middleware()
], SendIndexFile);
let CheckParam = class CheckParam {
    constructor(auth) {
        this.auth = auth;
    }
    async use(req, res, next) {
        const { api_key, redirect_url } = req.query;
        const user = req.session.user;
        console.log(req.session);
        if (api_key === undefined) {
            res.send('api_key not found');
        }
        else if (redirect_url === undefined) {
            res.send('redirect_url not found');
        }
        else {
            next();
        }
    }
};
__decorate([
    __param(0, common_1.Req()), __param(1, common_1.Res()), __param(2, common_1.Next()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], CheckParam.prototype, "use", null);
CheckParam = __decorate([
    common_1.Middleware(),
    __metadata("design:paramtypes", [AuthService_1.AuthService])
], CheckParam);
let CheckSession = class CheckSession {
    constructor(auth) {
        this.auth = auth;
    }
    async use(req, res, next) {
        const { api_key, redirect_url } = req.query;
        const user = req.session.user;
        if (user !== undefined) {
            res.redirect(`${redirect_url}?token=${await this.auth.generateToken(api_key, user.id)}`);
            next();
        }
        else {
            next();
        }
    }
};
__decorate([
    __param(0, common_1.Req()), __param(1, common_1.Res()), __param(2, common_1.Next()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], CheckSession.prototype, "use", null);
CheckSession = __decorate([
    common_1.Middleware(),
    __metadata("design:paramtypes", [AuthService_1.AuthService])
], CheckSession);
let WebViewController = class WebViewController {
    Index() {
        return fs.readFileSync(path.join(envConfig_1.ROOT_DIR, '../client/build/index.html'));
    }
    Register() {
        return fs.readFileSync(path.join(envConfig_1.ROOT_DIR, '../client/build/index.html'));
    }
    Forget() {
        return fs.readFileSync(path.join(envConfig_1.ROOT_DIR, '../client/build/index.html'));
    }
    Logout(session, res, req) {
        delete session.user;
        res.redirect(req.query.redirect_url);
    }
};
__decorate([
    common_1.Get('/'),
    common_1.UseBefore(CheckParam, CheckSession),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], WebViewController.prototype, "Index", null);
__decorate([
    common_1.Get('/register'),
    common_1.UseBefore(CheckParam, CheckSession),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], WebViewController.prototype, "Register", null);
__decorate([
    common_1.Get('/forget'),
    common_1.UseBefore(CheckParam, CheckSession),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], WebViewController.prototype, "Forget", null);
__decorate([
    common_1.Get('/logout'),
    common_1.UseBefore(CheckParam),
    __param(0, common_1.Session()), __param(1, common_1.Res()), __param(2, common_1.Req()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", void 0)
], WebViewController.prototype, "Logout", null);
WebViewController = __decorate([
    common_1.Controller('/')
], WebViewController);
exports.WebViewController = WebViewController;
//# sourceMappingURL=webController.js.map