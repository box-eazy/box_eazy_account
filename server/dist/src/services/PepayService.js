"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PepayService = void 0;
const di_1 = require("@tsed/di");
const typeorm_1 = require("typeorm");
const PepayLog_1 = require("../entities/PepayLog");
const Add_1 = require("../models/Pepay/Add");
const PepayLogRepo_1 = require("../repositorys/PepayLogRepo");
const BaseSerivce_1 = require("./BaseSerivce");
const orderId_1 = require("../libs/generator/orderId");
const PostForm_1 = require("../models/Pepay/PostForm");
const crypto_js_1 = require("crypto-js");
const Receive1_1 = require("../models/Pepay/Receive1");
const BaseError_1 = require("../exceptions/BaseError");
const PepayReceive1Log_1 = require("../entities/PepayReceive1Log");
const Receive2_1 = require("../models/Pepay/Receive2");
const PepayReceive2Log_1 = require("../entities/PepayReceive2Log");
const CreditLog_1 = require("../entities/CreditLog");
const Status_1 = require("../models/Credit/Status");
const Page_1 = require("../models/Common/Page");
const DateRange_1 = require("../models/Common/DateRange");
const axios_1 = require("axios");
let PepayService = class PepayService extends BaseSerivce_1.BaseService {
    constructor(pepayLog) {
        super();
        this.pepayLog = pepayLog;
        this.shopId = 'PPS_172049';
        this.shopTrustCode = 'm1EWEGM4Te';
        this.sysTrustCode = '0MSl72xico';
        this.postUrl = 'https://gate.pepay.com.tw/pepay/paysel_amt.php';
        this.generateOrderId = new orderId_1.GenerateOrderId('EAZY', 'yyyyMMdd', 10);
    }
    encodeCheckCode(arr) {
        const str = arr.join('#');
        console.log(str);
        return crypto_js_1.MD5(str).toString();
    }
    getCheckCode(data) {
        return this.encodeCheckCode([
            this.sysTrustCode,
            this.shopId,
            data.orderId,
            data.amount,
            this.shopTrustCode,
        ]);
    }
    getReceive1CheckCode(data) {
        return this.encodeCheckCode([
            this.sysTrustCode,
            data.SHOP_ID,
            data.ORDER_ID,
            data.AMOUNT,
            data.SESS_ID,
            data.PROD_ID,
            this.shopTrustCode,
        ]);
    }
    getReceive2CheckCode(data) {
        console.log('receive2 encode');
        return this.encodeCheckCode([
            this.sysTrustCode,
            data.SHOP_ID,
            data.ORDER_ID,
            data.AMOUNT,
            data.SESS_ID,
            data.PROD_ID,
            data.USER_ID,
            this.shopTrustCode,
        ]);
    }
    async findAll(page, dateRange) {
        const [items, total] = await this.pepayLog.findAndCount({
            relations: ['user', 'receive1', 'receive2'],
            where: {
                ...DateRange_1.DateRangeParams.toWhereBetween('createdAt', dateRange),
            },
            ...Page_1.PageParams.toTypeOrmOptions(page),
        });
        return {
            items,
            total,
        };
    }
    async create(appId, data, manager) {
        let orderNum = 1;
        const lastOrder = await manager.findOne(PepayLog_1.PepayLog, undefined, {
            where: {
                orderId: typeorm_1.Like(`${this.generateOrderId.getPrefix()}%`),
            },
            order: {
                createdAt: 'DESC',
            },
        });
        if (lastOrder !== undefined) {
            orderNum = this.generateOrderId.getOrderNum(lastOrder.orderId) + 1;
        }
        let dbData = new PepayLog_1.PepayLog({
            ...data,
            appId,
            orderId: this.generateOrderId.generateOrderId(orderNum),
            createdAt: new Date(),
        });
        dbData.checkCode = this.getCheckCode(dbData);
        dbData = await manager.save(dbData);
        await manager.save(new CreditLog_1.CreditLog({
            pepayId: dbData.id,
            status: Status_1.CreditStatus.CREATED,
            createdAt: new Date(),
            ...dbData,
        }));
        return new PostForm_1.PepayPostForm({
            data: {
                ORDER_ID: dbData.orderId,
                AMOUNT: dbData.amount,
                SHOP_ID: this.shopId,
                PROD_ID: '',
                SHOP_PARA: '',
                ORDER_ITEM: dbData.orderItem,
                CURRENCY: 'TWD',
                CHECK_CODE: dbData.checkCode,
            },
            url: this.postUrl,
        });
    }
    async createReceive1(data, manager) {
        if (data.CHECK_CODE !== this.getReceive1CheckCode(data)) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.PEPAY_RECEIVE01_CHECK_CODE_FAILED);
        }
        const dbData = await manager.findOne(PepayLog_1.PepayLog, { orderId: data.ORDER_ID }, {
            relations: ['receive1', 'receive2'],
        });
        if (dbData.receive1) {
            await manager.delete(PepayReceive1Log_1.PepayReceive1Log, {
                orderId: data.ORDER_ID,
            });
            // throw new BaseError(BaseError.Code.PEPAY_RECEIVE01_IS_EXIST);
        }
        const receive = new PepayReceive1Log_1.PepayReceive1Log({
            shopId: data.SHOP_ID,
            orderId: data.ORDER_ID,
            sessId: data.SESS_ID,
            prodId: data.PROD_ID,
            amount: data.AMOUNT,
            pepayId: dbData.id,
            userId: `${dbData.userId}`,
            checkCode: data.CHECK_CODE,
            shopPara: data.SHOP_PARA,
        });
        try {
            await axios_1.default({
                url: dbData.returnUrl,
                method: 'POST',
                data: {
                    type: 'receive1',
                    data,
                },
            });
            receive.isReturn = true;
        }
        catch (err) {
            receive.isReturn = false;
        }
        await manager.save(receive);
        await manager.update(CreditLog_1.CreditLog, { pepayId: dbData.id }, {
            status: Status_1.CreditStatus.WAITING,
            updatedAt: new Date(),
        });
        return new Receive1_1.PepayReceive1Result({
            RES_CODE: 0,
            USER_ID: receive.userId.toString(),
            SHOP_PARA: receive.shopPara,
            RET_URL: `http://13.113.142.165:30011/api/pepay/${dbData.orderId}/receive2`,
        });
    }
    async createReceive2(data, manager) {
        if (data.CHECK_CODE !== this.getReceive2CheckCode(data)) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.PEPAY_RECEIVE02_CHECK_CODE_FAILED);
        }
        const dbData = await manager.findOne(PepayLog_1.PepayLog, { orderId: data.ORDER_ID }, {
            relations: ['receive1', 'receive2'],
        });
        if (dbData.receive2) {
            await manager.delete(PepayReceive2Log_1.PepayReceive2Log, {
                orderId: data.ORDER_ID,
            });
            // throw new BaseError(BaseError.Code.PEPAY_RECEIVE02_IS_EXIST);
        }
        const receive = new PepayReceive2Log_1.PepayReceive2Log({
            pepayId: dbData.id,
            sessId: data.SESS_ID,
            orderId: data.ORDER_ID,
            billId: data.BILL_ID,
            dataId: data.DATA_ID,
            shopId: data.SHOP_ID,
            payType: data.PAY_TYPE,
            prodId: data.PROD_ID,
            userId: data.USER_ID,
            sourceAmount: data.SOURCE_AMOUNT,
            amount: data.AMOUNT,
            currency: data.CURRENCY,
            dataCode: data.DATA_CODE,
            tradeCode: data.TRADE_CODE,
            shopPara: data.SHOP_PARA,
            cdate: data.CDATE,
            ctime: data.CTIME,
            billDate: data.BILL_DATE,
            billTime: data.BILL_TIME,
            date: data.DATE,
            time: data.TIME,
            checkCode: data.CHECK_CODE,
        });
        try {
            await axios_1.default({
                url: dbData.returnUrl,
                method: 'POST',
                data: {
                    type: 'receive2',
                    data,
                },
            });
            receive.isReturn = true;
        }
        catch (err) {
            receive.isReturn = false;
        }
        await manager.save(receive);
        await manager.update(CreditLog_1.CreditLog, {
            pepayId: dbData.id,
        }, {
            updatedAt: new Date(),
            status: Status_1.CreditStatus.SUCCESS,
        });
        return new Receive1_1.PepayReceive1Result({
            RES_CODE: 0,
        });
    }
    toOutputString(obj) {
        return Object.keys(obj)
            .map(key => `${key}=${obj[key]}`)
            .join('&');
    }
};
__decorate([
    typeorm_1.Transaction(),
    __param(2, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Add_1.PepayAddBody,
        typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PepayService.prototype, "create", null);
__decorate([
    typeorm_1.Transaction(),
    __param(1, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Receive1_1.PepayReceive1Body,
        typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PepayService.prototype, "createReceive1", null);
__decorate([
    typeorm_1.Transaction(),
    __param(1, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Receive2_1.PepayReceive2Body,
        typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], PepayService.prototype, "createReceive2", null);
PepayService = __decorate([
    di_1.Service(),
    __metadata("design:paramtypes", [PepayLogRepo_1.PepayLogRepo])
], PepayService);
exports.PepayService = PepayService;
//# sourceMappingURL=PepayService.js.map