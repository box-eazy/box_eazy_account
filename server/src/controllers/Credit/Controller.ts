import {
  BodyParams,
  Controller,
  Delete,
  Get,
  PathParams,
  Post,
  Put,
  QueryParams,
} from '@tsed/common';
import {Description, In, Returns, Security, Summary} from '@tsed/schema';
import {id} from 'date-fns/locale';
import {AuthApp} from '../../decorators/AuthApp';
import {CreditLog} from '../../entities/CreditLog';
import {BearerTokenAuth} from '../../middlewares/BearerTokenAuth';
import {AuthAppInfo} from '../../models/AppAuthInfo';
import {DateRangeParams} from '../../models/Common/DateRange';
import {PageParams} from '../../models/Common/Page';
import {CreditAddBody} from '../../models/Credit/AddBody';
import {CreditStats} from '../../models/Credit/Stats';
import {CreditStatus} from '../../models/Credit/Status';
import {CreditService} from '../../services/CreditService';
import {ResponseService} from '../../services/Response';

@Controller('/credit')
@Description('入帳資料相關API')
@BearerTokenAuth()
export class CreditController {
  constructor(private credit: CreditService, private res: ResponseService) {}

  @Summary('取得入帳列表')
  @Get()
  @Returns(200, ResponseService.toPagiation(CreditLog))
  findAll(
    @QueryParams() dateRange: DateRangeParams,
    @QueryParams() page: PageParams,
    @QueryParams('appId') appId: number,
    @QueryParams('status') status: CreditStatus,
    @QueryParams('keyword') keyword: string
  ) {
    return this.res.toPage(this.credit.findAll(dateRange, page));
  }

  @Summary('新增入帳資料(用此API新增的入帳資料會與Pepay脫勾)')
  @Post()
  @Returns(200, ResponseService.toResult(CreditLog))
  create(@BodyParams() data: CreditAddBody, @AuthApp() app: AuthAppInfo) {
    return this.res.toResult(this.credit.create(app.appId, data));
  }

  @Summary('編輯入帳資料')
  @Put('/:id')
  @Returns(200, ResponseService.toResult(CreditLog))
  edit(@PathParams('id') id: number, @BodyParams() data: CreditAddBody) {
    return this.res.toResult(this.credit.edit(id, data));
  }

  @Summary('刪除入帳資料')
  @Delete('/:id')
  @Returns(200, ResponseService.toResult(CreditLog))
  delete(@PathParams('id') id: number) {
    return this.res.toResult(this.credit.delete(id));
  }

  @Summary('/統計各平台收入')
  @Get('/stats')
  @Returns(200, ResponseService.toResult(CreditStats))
  stats() {
    return this.res.toResult(this.credit.stats());
  }
}
