import {$log} from '@tsed/common';
import {PlatformExpress} from '@tsed/platform-express';
import {Property} from '@tsed/schema';
import * as envConfig from '../envConfig';
import {AuthUserInfo} from './models/Auth/UserInfo';
import {Server} from './Server';

async function bootstrap() {
  try {
    $log.debug('Start server...');
    const platform = await PlatformExpress.bootstrap(Server, {
      // extra settings
    });

    await platform.listen();
    $log.debug('Server initialized');
  } catch (er) {
    $log.error(er);
  }
}

bootstrap();
