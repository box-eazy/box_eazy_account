import {
  Configuration,
  Constant,
  Inject,
  Next,
  PlatformApplication,
  Req,
  Res,
} from '@tsed/common';
import '@tsed/platform-express';
import * as bodyParser from 'body-parser';
import * as compress from 'compression';
import * as cookieParser from 'cookie-parser';
import * as methodOverride from 'method-override';
import '@tsed/typeorm';
import '@tsed/swagger';
import '@tsed/passport';
import '@tsed/ajv';
import * as session from 'express-session';
// import * as passport from 'passport';
import './filters/AjvErrorFilter';
import './filters/BaseErrorFilter';
import './filters/ErrorFilter';
import {AuthUserInfo} from './models/Auth/UserInfo';
import * as ormConfig from '../ormconfig';
import * as mySqlStore from 'express-mysql-session';
import * as path from 'path';
import * as envConfig from '../envConfig';
import * as expressBerearToken from 'express-bearer-token';
import {getErrorList} from './models/Error';
import {AuthService} from './services/AuthService';

const MyStoreStore = mySqlStore(session);
const rootDir = __dirname;

@Configuration({
  port: envConfig.PORT,
  rootDir,
  statics: {
    '/': {
      root: `${envConfig.ROOT_DIR}/../client/build`,
      index: false,
    },
  },
  ajv: {},
  typeorm: [require('../ormconfig.js')],
  swagger: [
    {
      path: '/docs',
      specVersion: '3.0.3',
      spec: {
        tags: [{name: 'Web', description: '整合系統網頁端用Api'}],
        info: {
          title: 'title',
          version: '1.0.0',
          description: `### ErrorCodeList\n${getErrorList()
            .map(x => `- \`${x.errCode}\`${x.errMessage}`)
            .join('\n')}`,
        },
        components: {
          securitySchemes: {
            bearerAuth: {
              type: 'http',
              scheme: 'bearer',
            },
          },
        },
      },
    },
  ],
  mount: {
    '/api': '${rootDir}/controllers/**/*.ts',
    '/': '${rootDir}/webControllers/**/*.ts',
  },
  componentsScan: ['${rootDir}/protocols/*{.ts,.js}'],
  passport: {
    userInfoModel: AuthUserInfo,
  },
})
export class Server {
  @Inject()
  app: PlatformApplication;

  @Configuration()
  settings: Configuration;

  /**
   * This method let you configure the express middleware required by your application to works.
   * @returns {Server}
   */
  public $beforeRoutesInit(): void | Promise<any> {
    this.app
      .use(cookieParser())
      .use(compress({}))
      .use(methodOverride())
      .use(bodyParser.json())
      .use(expressBerearToken())
      .use(
        bodyParser.urlencoded({
          extended: true,
        })
      )
      .use(
        session({
          //若是測試環境則不要使用檔案儲存的方式，避免一直產生廢檔
          store: envConfig.TEST
            ? undefined
            : new MyStoreStore({
                host: ormConfig.host,
                port: ormConfig.port,
                user: ormConfig.username,
                password: ormConfig.password,
                database: ormConfig.database,
              }),
          secret: 'keyboard cat', // change secret key
          resave: true,
          saveUninitialized: false,
          cookie: {
            maxAge: 1000 * 60 * 60 * 24 * 365,
            secure: false, // set true if HTTPS is enabled
          },
        })
      );
  }

  $afterRoutesInit() {
    this.app.get('/*', (req: any, res: Res) => {
      res.sendFile(path.join(envConfig.ROOT_DIR, '../client/build/index.html'));
    });
  }
}
