import {useMemo} from 'react';
import qs from 'qs';
export const useQueryString = <T = {}>(): T => {
  return useMemo<T>(() => {
    return qs.parse(window.location.search.slice(1)) as any;
  }, []);
};
