"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PepayReceive1Result = exports.PepayReceive1Body = void 0;
const schema_1 = require("@tsed/schema");
const BaseModel_1 = require("../Common/BaseModel");
class PepayReceive1Body extends BaseModel_1.BaseModel {
}
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Body.prototype, "SHOP_ID", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Body.prototype, "ORDER_ID", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Body.prototype, "SESS_ID", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Body.prototype, "PROD_ID", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayReceive1Body.prototype, "AMOUNT", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Body.prototype, "CURRENCY", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Body.prototype, "SHOP_PARA", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Body.prototype, "CHECK_CODE", void 0);
exports.PepayReceive1Body = PepayReceive1Body;
class PepayReceive1Result extends BaseModel_1.BaseModel {
}
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Result.prototype, "USER_ID", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayReceive1Result.prototype, "RES_CODE", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Result.prototype, "SHOP_PARA", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive1Result.prototype, "RET_URL", void 0);
exports.PepayReceive1Result = PepayReceive1Result;
//# sourceMappingURL=Receive1.js.map