"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisterProvider = void 0;
var RegisterProvider;
(function (RegisterProvider) {
    RegisterProvider["GENERAL"] = "GENERAL";
    RegisterProvider["FACEBOOK"] = "FACEBOOK";
    RegisterProvider["GOOGLE"] = "GOOGLE";
})(RegisterProvider = exports.RegisterProvider || (exports.RegisterProvider = {}));
//# sourceMappingURL=RegisterProvider.js.map