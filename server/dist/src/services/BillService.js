"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillService = void 0;
const typeorm_1 = require("typeorm");
const BillLog_1 = require("../entities/BillLog");
const DateRange_1 = require("../models/Common/DateRange");
const Page_1 = require("../models/Common/Page");
class BillService {
    constructor(bill) {
        this.bill = bill;
    }
    async findAll(date, page) {
        const [items, total] = await this.bill.findAndCount({
            relations: ['user'],
            where: {
                deletedAt: typeorm_1.IsNull(),
                ...DateRange_1.DateRangeParams.toWhereBetween('createdAt', date),
            },
            ...Page_1.PageParams.toTypeOrmOptions(page),
        });
        return {
            items,
            total,
        };
    }
    async create(appId, data) {
        let dbData = new BillLog_1.BillLog({
            ...data,
            appId,
            createdAt: new Date(),
        });
        dbData = await this.bill.save(dbData);
        return dbData;
    }
    async edit(id, data) {
        let dbData = await this.bill.findOne(id, {
            relations: ['user'],
        });
        dbData = Object.assign(dbData, data);
        dbData.updatedAt = new Date();
        return dbData;
    }
    async delete(id) {
        const dbData = await this.bill.findOne(id, {
            relations: ['user'],
        });
        dbData.deletedAt = new Date();
        return dbData;
    }
}
exports.BillService = BillService;
//# sourceMappingURL=BillService.js.map