"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const AuthApp_1 = require("../../decorators/AuthApp");
const BearerTokenAuth_1 = require("../../middlewares/BearerTokenAuth");
const AppAuthInfo_1 = require("../../models/AppAuthInfo");
const Basic_1 = require("../../models/Auth/Basic");
const Info_1 = require("../../models/Auth/Info");
const AuthService_1 = require("../../services/AuthService");
const Response_1 = require("../../services/Response");
let AuthController = class AuthController {
    constructor(auth, res) {
        this.auth = auth;
        this.res = res;
    }
    basicAuth(data) {
        return this.res.toResult(this.auth.basicLogin(data.username, data.password));
    }
    tokenAuth(token, app) {
        return this.res.toResult(this.auth.tokenLogin(app.appId, token));
    }
};
__decorate([
    BearerTokenAuth_1.BearerTokenAuth(),
    schema_1.Summary('使用帳號及密碼取得userId'),
    common_1.Post(),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Info_1.BasicAuthInfo)),
    __param(0, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Basic_1.BasicAuthBody]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "basicAuth", null);
__decorate([
    BearerTokenAuth_1.BearerTokenAuth(),
    schema_1.Summary('使用token取得使用者的userId'),
    common_1.Post('/:token'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Info_1.BasicAuthInfo)),
    __param(0, common_1.PathParams('token')), __param(1, AuthApp_1.AuthApp()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, AppAuthInfo_1.AuthAppInfo]),
    __metadata("design:returntype", void 0)
], AuthController.prototype, "tokenAuth", null);
AuthController = __decorate([
    common_1.Controller('/auth'),
    schema_1.Description('使用者認證相關API'),
    __metadata("design:paramtypes", [AuthService_1.AuthService, Response_1.ResponseService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=Controller.js.map