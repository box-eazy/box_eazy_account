const envConfig = require('./envConfig');
const config = {
  test: {
    name: 'default',
    database: ':memory:',
    type: 'sqlite',
    logging: false,
    entities: ['./src/entities/**.ts'],
    migrations: ['./src/migrations/*.ts'],
    seeds: ['./src/seeds/**/*{.ts,.js}'],
    factories: ['./src/factories/**/*{.ts,.js}'],
    cli: {
      migrationsDir: 'src/migrations',
    },
  },
  development: {
    name: 'default',
    type: 'mysql',
    host: envConfig.DB_HOST,
    port: envConfig.DB_PORT,
    username: envConfig.DB_USERNAME,
    password: envConfig.DB_PASSWORD,
    database: envConfig.DB_DATABASE,
    synchronize: true,
    charset: 'utf8mb4_general_ci',
    entities: ['./src/entities/*{.ts,.js}'],
    migrations: ['./src/migrations/*{.ts,.js}'],
    seeds: ['./src/seeds/**/*{.ts,.js}'],
    factories: ['./src/factories/**/*{.ts,.js}'],
    logging: true,
    cli: {
      migrationsDir: 'src/migrations',
    },
  },
  production: {
    name: 'default',
    type: 'mysql',
    host: envConfig.DB_HOST,
    port: envConfig.DB_PORT,
    username: envConfig.DB_USERNAME,
    password: envConfig.DB_PASSWORD,
    database: envConfig.DB_DATABASE,
    charset: 'utf8mb4_general_ci',
    entities: ['./dist/src/entities/*{.ts,.js}'],
    migrations: ['./dist/src/migrations/*{.ts,.js}'],
    seeds: ['./dist/src/seeds/**/*{.ts,.js}'],
    factories: ['./dist/src/factories/**/*{.ts,.js}'],
    logging: false,
    cli: {
      migrationsDir: 'dist/src/migrations',
    },
  },
};

module.exports = config[process.env.NODE_ENV] || config.development;
