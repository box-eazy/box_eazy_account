import {Description, Property} from '@tsed/schema';
import {Between} from 'typeorm';

export class DateRangeParams {
  @Property(Date)
  @Description('查詢起時 格式支援 yyyy-MM-dd OR yyyy-MM-dd HH:mm:ss')
  startAt: Date;

  @Property(Date)
  @Description('查詢訖時 格式支援 yyyy-MM-dd OR yyyy-MM-dd HH:mm:ss')
  endAt: Date;

  static toWhereBetween(fieldName: string, data?: DateRangeParams) {
    const result: any = {};
    if (
      data !== undefined &&
      data.startAt !== undefined &&
      data.endAt !== undefined
    ) {
      result[fieldName] = Between(data.startAt, data.endAt);
    }

    return result;
  }
}
