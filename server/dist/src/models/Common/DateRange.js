"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateRangeParams = void 0;
const schema_1 = require("@tsed/schema");
const typeorm_1 = require("typeorm");
class DateRangeParams {
    static toWhereBetween(fieldName, data) {
        const result = {};
        if (data !== undefined &&
            data.startAt !== undefined &&
            data.endAt !== undefined) {
            result[fieldName] = typeorm_1.Between(data.startAt, data.endAt);
        }
        return result;
    }
}
__decorate([
    schema_1.Property(Date),
    schema_1.Description('查詢起時 格式支援 yyyy-MM-dd OR yyyy-MM-dd HH:mm:ss'),
    __metadata("design:type", Date)
], DateRangeParams.prototype, "startAt", void 0);
__decorate([
    schema_1.Property(Date),
    schema_1.Description('查詢訖時 格式支援 yyyy-MM-dd OR yyyy-MM-dd HH:mm:ss'),
    __metadata("design:type", Date)
], DateRangeParams.prototype, "endAt", void 0);
exports.DateRangeParams = DateRangeParams;
//# sourceMappingURL=DateRange.js.map