"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ForgetValidBody = void 0;
const schema_1 = require("@tsed/schema");
class ForgetValidBody {
}
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], ForgetValidBody.prototype, "apiKey", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], ForgetValidBody.prototype, "username", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], ForgetValidBody.prototype, "email", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], ForgetValidBody.prototype, "phone", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], ForgetValidBody.prototype, "smsCode", void 0);
__decorate([
    schema_1.Property(),
    schema_1.Description('email驗證URL'),
    __metadata("design:type", String)
], ForgetValidBody.prototype, "validUrl", void 0);
__decorate([
    schema_1.Property(),
    schema_1.Description('驗證成功導向Url'),
    __metadata("design:type", String)
], ForgetValidBody.prototype, "redirectUrl", void 0);
exports.ForgetValidBody = ForgetValidBody;
//# sourceMappingURL=ValidBody.js.map