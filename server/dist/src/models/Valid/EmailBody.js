"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidEmailAddBody = void 0;
const schema_1 = require("@tsed/schema");
const Temp_1 = require("./Temp");
class ValidEmailAddBody {
}
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], ValidEmailAddBody.prototype, "email", void 0);
__decorate([
    schema_1.Enum(Temp_1.ValidTemp),
    schema_1.Property(),
    __metadata("design:type", String)
], ValidEmailAddBody.prototype, "temp", void 0);
__decorate([
    schema_1.Property(),
    schema_1.Description('驗證信箱的連結'),
    __metadata("design:type", String)
], ValidEmailAddBody.prototype, "validUrl", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], ValidEmailAddBody.prototype, "redirectUrl", void 0);
exports.ValidEmailAddBody = ValidEmailAddBody;
//# sourceMappingURL=EmailBody.js.map