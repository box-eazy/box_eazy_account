"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Application_1 = require("../entities/Application");
class InitSeed {
    async run(factory, connection) {
        /* #region Generate 4 Role Data  */
        const now = new Date();
        await connection
            .createQueryBuilder()
            .insert()
            .into(Application_1.Application)
            .values([
            {
                id: 1,
                name: '黃頁',
                isCreditStats: true,
                isDisabled: false,
                createdAt: now,
                apiKey: Application_1.Application.generateRandomToken(12),
                apiSecert: Application_1.Application.generateRandomToken(16),
            },
            {
                id: 2,
                name: 'CallBible',
                isCreditStats: true,
                isDisabled: false,
                createdAt: now,
                apiKey: Application_1.Application.generateRandomToken(12),
                apiSecert: Application_1.Application.generateRandomToken(16),
            },
            {
                id: 3,
                name: 'EazyDating',
                isCreditStats: true,
                isDisabled: false,
                createdAt: now,
                apiKey: Application_1.Application.generateRandomToken(12),
                apiSecert: Application_1.Application.generateRandomToken(16),
            },
            {
                id: 4,
                name: 'Eazy總後台',
                isCreditStats: false,
                isDisabled: false,
                createdAt: now,
                apiKey: Application_1.Application.generateRandomToken(12),
                apiSecert: Application_1.Application.generateRandomToken(16),
            },
        ])
            .execute();
    }
}
exports.default = InitSeed;
//# sourceMappingURL=init-app.js.map