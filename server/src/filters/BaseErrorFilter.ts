import {Catch, ExceptionFilterMethods, PlatformContext} from '@tsed/common';
import {Exception} from '@tsed/exceptions';
import {response} from 'express';
import {BaseError} from '../exceptions/BaseError';

@Catch(BaseError)
export class BaseErrorFilter implements ExceptionFilterMethods {
  catch(exception: Exception, ctx: PlatformContext) {
    console.error('BaseErrorFilter', exception.message);
    ctx.logger.error(exception);
    ctx.response.status(400);
    ctx.response.body(exception);
  }
}
