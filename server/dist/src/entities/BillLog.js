"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillLog = void 0;
const typeorm_1 = require("typeorm");
const User_1 = require("./User");
const Application_1 = require("./Application");
const Base_1 = require("./Common/Base");
const schema_1 = require("@tsed/schema");
let BillLog = class BillLog extends Base_1.CommonBase {
};
__decorate([
    typeorm_1.ManyToOne(() => User_1.User),
    schema_1.Property(User_1.User),
    __metadata("design:type", User_1.User)
], BillLog.prototype, "user", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], BillLog.prototype, "userId", void 0);
__decorate([
    typeorm_1.Column({ default: 0 }),
    schema_1.Property(),
    __metadata("design:type", Number)
], BillLog.prototype, "amount", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], BillLog.prototype, "note", void 0);
__decorate([
    typeorm_1.ManyToOne(() => Application_1.Application),
    __metadata("design:type", Application_1.Application)
], BillLog.prototype, "app", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], BillLog.prototype, "appId", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], BillLog.prototype, "bankCode", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], BillLog.prototype, "bankAccount", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Boolean)
], BillLog.prototype, "isBill", void 0);
BillLog = __decorate([
    typeorm_1.Entity()
], BillLog);
exports.BillLog = BillLog;
//# sourceMappingURL=BillLog.js.map