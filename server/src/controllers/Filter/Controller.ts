import {
  BodyParams,
  Controller,
  Delete,
  Get,
  PathParams,
  Post,
  Put,
  QueryParams,
} from '@tsed/common';
import {Description, Returns, Security, Summary} from '@tsed/schema';
import {AuthApp} from '../../decorators/AuthApp';
import {FilterLog} from '../../entities/FilterLog';
import {BearerTokenAuth} from '../../middlewares/BearerTokenAuth';
import {AuthAppInfo} from '../../models/AppAuthInfo';
import {PageParams} from '../../models/Common/Page';
import {FilterLogAddBody} from '../../models/FilterLog/AddBody';
import {FilterService} from '../../services/FilterService';
import {ResponseService} from '../../services/Response';

@Controller('/filter')
@Description('黑白名單相關api')
@BearerTokenAuth()
export class FilterController {
  constructor(private filter: FilterService, private res: ResponseService) {}

  @Get()
  @Summary('取得黑白名單列表')
  @Returns(200, ResponseService.toPagiation(FilterLog))
  findAll(
    @QueryParams() page: PageParams,
    @QueryParams('phone')
    @Description('電話過濾，可以用來判斷電話是否在名單內')
    phone: string
  ) {
    return this.res.toPage(this.filter.findAll(phone, page));
  }

  @Post()
  @Summary('新增黑/白名單')
  @Returns(200, ResponseService.toResult(FilterLog))
  create(@BodyParams() data: FilterLogAddBody, @AuthApp() app: AuthAppInfo) {
    return this.res.toResult(this.filter.create(app.appId, data));
  }

  @Put('/:id')
  @Summary('修改黑/白名單')
  @Returns(200, ResponseService.toResult(FilterLog))
  edit(@PathParams('id') id: number, @BodyParams() data: FilterLogAddBody) {
    return this.res.toResult(this.filter.update(id, data));
  }
  @Delete('/:id')
  @Summary('刪除黑/白名單')
  delete(@PathParams('id') id: number) {
    return this.res.toResult(this.filter.delete(id));
  }
}
