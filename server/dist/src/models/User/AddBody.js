"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAddBody = void 0;
const schema_1 = require("@tsed/schema");
const Payload_1 = require("./Payload");
const RegisterProvider_1 = require("./RegisterProvider");
class UserAddBody {
}
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserAddBody.prototype, "username", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserAddBody.prototype, "password", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserAddBody.prototype, "phone", void 0);
__decorate([
    schema_1.Property(),
    schema_1.Format('email'),
    __metadata("design:type", String)
], UserAddBody.prototype, "email", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserAddBody.prototype, "nickname", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", Boolean)
], UserAddBody.prototype, "isEmailValid", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", Boolean)
], UserAddBody.prototype, "isSmsValid", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserAddBody.prototype, "image", void 0);
__decorate([
    schema_1.CollectionOf(Payload_1.UserPayload),
    __metadata("design:type", Array)
], UserAddBody.prototype, "payloads", void 0);
__decorate([
    schema_1.Property(),
    schema_1.Enum(RegisterProvider_1.RegisterProvider),
    __metadata("design:type", String)
], UserAddBody.prototype, "provider", void 0);
exports.UserAddBody = UserAddBody;
//# sourceMappingURL=AddBody.js.map