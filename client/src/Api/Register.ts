import {baseAxios} from './Base';

export const RegisterApi = async (apiKey: string, data: any) => {
  const res = await baseAxios.request<{token: string}>({
    url: '/web/register',
    method: 'post',
    data: {
      ...data,
      apiKey,
    },
  });
  return res.data.token;
};
