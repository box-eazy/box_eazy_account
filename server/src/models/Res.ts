import {Enum, Property} from '@tsed/schema';
import {ErrorCode} from './Error';

export class BaseRes {
  @Property()
  errMessage?: string;
  @Enum(ErrorCode)
  errCode?: ErrorCode;
}

export class ItemsRes<T> extends BaseRes {
  @Property()
  items: T[];
}

export class ResultRes<T> extends BaseRes {
  @Property()
  result: T;
}

export class PageRes<T> extends BaseRes {
  @Property()
  items: T[];
  @Property()
  total: number;
}
