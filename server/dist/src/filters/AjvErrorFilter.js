"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AjvErrorFilter = void 0;
const ajv_1 = require("@tsed/ajv");
const common_1 = require("@tsed/common");
const exceptions_1 = require("@tsed/exceptions");
const BaseError_1 = require("../exceptions/BaseError");
let AjvErrorFilter = class AjvErrorFilter {
    catch(exception, ctx) {
        ctx.logger.error(exception);
        if (exception.origin instanceof ajv_1.AjvValidationError) {
            ctx.response.status(400);
            ctx.response.body(new BaseError_1.BaseError(BaseError_1.BaseError.Code.AJV_VALID_ERROR, undefined, exception.origin.errors));
        }
    }
};
AjvErrorFilter = __decorate([
    common_1.Catch(exceptions_1.BadRequest)
], AjvErrorFilter);
exports.AjvErrorFilter = AjvErrorFilter;
//# sourceMappingURL=AjvErrorFilter.js.map