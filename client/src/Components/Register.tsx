import React, {ChangeEvent, useCallback, useState} from 'react';
import EazyCommon from '@eazy/widget';
import {useQueryString} from '../hooks/useQueryString';
import {SendSMSApi} from '../Api/SendSMS';
import {RegisterApi} from '../Api/Register';
import {getApiErrorMessage} from '../libs/handleError';
import {SendMailResult} from './SendMailResult';
import {SendSMSButton} from './SendSMSButton';

export const EazyRegister: React.FC = () => {
  const modal = EazyCommon.ModalUtils.useModal();
  const {
    api_key: apiKey,
    app_id: appId,
    redirect_url: redirectUrl,
  } = useQueryString<{api_key: string; app_id: string; redirect_url: string}>();
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [data, setData] = useState(() => ({
    nickname: '',
    username: '',
    password: '',
    confirmPassword: '',
    email: '',
    phone: '',
    smsCode: '',
    provider: 'GENERAL',
  }));

  const $onChange = useCallback(
    (field: keyof typeof data) => (e: ChangeEvent<HTMLInputElement>) => {
      setData(prevData => ({
        ...prevData,
        [field]: e.target.value,
      }));
    },
    []
  );

  const $onSubmit = useCallback(async () => {
    setLoading(true);
    try {
      await RegisterApi(apiKey, data);
      await modal.alert('註冊成功，將返回到首頁');
      setSuccess(true);
    } catch (err) {
      await modal.alert(getApiErrorMessage(err));
    }
    setLoading(false);
  }, [data, apiKey, redirectUrl]);

  if (success) {
    return (
      <SendMailResult
        title="註冊成功"
        email={data.email}
        type="REGISTER"
        validUrl="/register"
        apiKey={apiKey}
        redirectUrl={redirectUrl}
      >
        <div className="text-center">
          <div>您的註冊申請已送出！</div>
          <div>請至您的信箱收取Email驗證信，</div>
          <div>並點擊驗證連結以完成註冊</div>
          <br />
          <div>Email驗證完成後</div>
          <div>將可使用本站會員功能</div>
          <br />
          <div>若需變更手機號碼或Email</div>
          <div>可至「編輯個人資訊」頁面變更</div>
          <div>變更後，需通過驗證才算變更完成</div>
        </div>
      </SendMailResult>
    );
  }

  return (
    <EazyCommon.Layout.Wrapper
      headerTitle="會員註冊"
      back
      footer={
        <EazyCommon.Box p={'1rem'}>
          <EazyCommon.Button
            block
            size="lg"
            color="primary"
            type="submit"
            form="EazyRegisterForm"
          >
            註冊
          </EazyCommon.Button>
        </EazyCommon.Box>
      }
    >
      <EazyCommon.Form.Wrapper onSubmit={$onSubmit} id="EazyRegisterForm">
        <EazyCommon.Form.FormItem title="暱稱">
          <EazyCommon.Controls.Input
            placeholder="輸入暱稱"
            value={data.nickname}
            onChange={$onChange('nickname')}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="會員帳號">
          <EazyCommon.Controls.Input
            placeholder="輸入會員帳號"
            value={data.username}
            onChange={$onChange('username')}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="會員密碼">
          <EazyCommon.Controls.Input
            placeholder="輸入會員密碼"
            value={data.password}
            onChange={$onChange('password')}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="確認密碼">
          <EazyCommon.Controls.Input
            placeholder="再輸入一次密碼"
            value={data.confirmPassword}
            onChange={$onChange('confirmPassword')}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="電子信箱">
          <EazyCommon.Controls.Input
            placeholder="輸入電子信箱"
            value={data.email}
            onChange={$onChange('email')}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem
          title="手機號碼"
          suffix={
            <SendSMSButton phone={data.phone} apiKey={apiKey} type="REGISTER" />
          }
        >
          <EazyCommon.Controls.Input
            placeholder="輸入手機號碼"
            value={data.phone}
            onChange={$onChange('phone')}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="簡訊驗證碼">
          <EazyCommon.Controls.Input
            placeholder="輸入簡訊驗證碼"
            value={data.smsCode}
            onChange={$onChange('smsCode')}
            required
          />
        </EazyCommon.Form.FormItem>
      </EazyCommon.Form.Wrapper>
    </EazyCommon.Layout.Wrapper>
  );
};
