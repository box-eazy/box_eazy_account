"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseError = void 0;
const Error_1 = require("../models/Error");
class BaseError extends Error {
    constructor(errCode, errMessage, payload) {
        super(errMessage ? errMessage : Error_1.ErrorCode[errCode]);
        this.errCode = errCode;
        this.errMessage = errMessage;
        this.payload = payload;
        this.errMessage = errMessage ? errMessage : Error_1.ErrorCode[errCode];
    }
}
exports.BaseError = BaseError;
BaseError.Code = Error_1.ErrorCode;
//# sourceMappingURL=BaseError.js.map