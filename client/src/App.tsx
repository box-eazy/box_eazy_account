import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Login} from './Components/Login';
import EazyCommon from '@eazy/widget';
import 'bootstrap/dist/css/bootstrap.css';
import {EazyRegister} from './Components/Register';
import {EazyForget} from './Components/Forget';
import {EazyResetPassword} from './Components/ResetPassword';
function App() {
  return (
    <EazyCommon.Theme.Provider>
      <EazyCommon.ModalUtils.Provider>
        <EazyCommon.ReactRouterDom.BrowserRouter>
          <EazyCommon.ReactRouterDom.Switch>
            <EazyCommon.ReactRouterDom.Route
              path="/forget/:token"
              component={EazyResetPassword}
            />
            <EazyCommon.ReactRouterDom.Route
              path="/forget"
              component={EazyForget}
            />
            <EazyCommon.ReactRouterDom.Route
              path="/register"
              component={EazyRegister}
            />
            <EazyCommon.ReactRouterDom.Route
              path=""
              exact
              strict
              component={Login}
            />
          </EazyCommon.ReactRouterDom.Switch>
        </EazyCommon.ReactRouterDom.BrowserRouter>
      </EazyCommon.ModalUtils.Provider>
    </EazyCommon.Theme.Provider>
  );
}

export default App;
