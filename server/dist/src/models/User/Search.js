"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSearchBody = void 0;
const schema_1 = require("@tsed/schema");
const BaseModel_1 = require("../Common/BaseModel");
const OrderBy_1 = require("../Common/OrderBy");
const Page_1 = require("../Common/Page");
class UserSearchBody extends BaseModel_1.BaseModel {
}
__decorate([
    schema_1.Property(Page_1.PageParams),
    __metadata("design:type", Page_1.PageParams)
], UserSearchBody.prototype, "page", void 0);
__decorate([
    schema_1.CollectionOf('number'),
    __metadata("design:type", Array)
], UserSearchBody.prototype, "userIds", void 0);
__decorate([
    schema_1.Property(),
    __metadata("design:type", String)
], UserSearchBody.prototype, "keyword", void 0);
__decorate([
    schema_1.Description(`
    Array第一層皆會做 And 判斷
    Array第二層則會做 OR 判斷
    EX:
    [
      { key: 'A', value: '1' },
      [
        { key: 'B', value: '2' },
        { key: 'C', value: '3' }
      ]
    ]
    則會產生
    A = '1' AND ( B = '2' OR C = '3')
`),
    schema_1.Any(Object, Array),
    schema_1.Example('[{"key": "", "value":""}]'),
    __metadata("design:type", Array)
], UserSearchBody.prototype, "filters", void 0);
__decorate([
    schema_1.Property(OrderBy_1.OrderByParam),
    schema_1.Description('可針對目前有的欄位進行比對'),
    __metadata("design:type", OrderBy_1.OrderByParam)
], UserSearchBody.prototype, "orderBy", void 0);
exports.UserSearchBody = UserSearchBody;
//# sourceMappingURL=Search.js.map