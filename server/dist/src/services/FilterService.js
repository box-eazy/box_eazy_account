"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilterService = void 0;
const di_1 = require("@tsed/di");
const BaseError_1 = require("../exceptions/BaseError");
const Page_1 = require("../models/Common/Page");
const FilterLogRepo_1 = require("../repositorys/FilterLogRepo");
const BaseSerivce_1 = require("./BaseSerivce");
let FilterService = class FilterService extends BaseSerivce_1.BaseService {
    constructor(filterLog) {
        super();
        this.filterLog = filterLog;
    }
    async findAll(phone, page) {
        const [items, total] = await this.filterLog.findAndCount({
            where: {
                phone,
            },
            ...Page_1.PageParams.toTypeOrmOptions(page),
        });
        return {
            items,
            total,
        };
    }
    async findOne(id) {
        return this.filterLog.findOne({ id });
    }
    async update(id, data) {
        if (!(await this.filterLog.exist({ id }))) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.FILTER_NOT_FOUND);
        }
        await this.filterLog.update({
            id,
        }, {
            ...data,
            updatedAt: new Date(),
        });
        return this.filterLog.findOne(id);
    }
    async create(appId, data) {
        if (await this.filterLog.exist({ phone: data.phone })) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.FILTER_PHONE_IS_EXIST);
        }
        return await this.filterLog.save({
            ...data,
            appId,
            createdAt: new Date(),
        });
    }
    async delete(id) {
        if (!(await this.filterLog.exist({ id }))) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.FILTER_NOT_FOUND);
        }
        const dbData = await this.filterLog.findOne(id);
        await this.filterLog.delete({
            id,
        });
        return dbData;
    }
};
FilterService = __decorate([
    di_1.Service(),
    __metadata("design:paramtypes", [FilterLogRepo_1.FilterLogRepo])
], FilterService);
exports.FilterService = FilterService;
//# sourceMappingURL=FilterService.js.map