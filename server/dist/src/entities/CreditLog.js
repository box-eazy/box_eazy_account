"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreditLog = void 0;
const typeorm_1 = require("typeorm");
const User_1 = require("./User");
const Application_1 = require("./Application");
const Base_1 = require("./Common/Base");
const PepayLog_1 = require("./PepayLog");
const schema_1 = require("@tsed/schema");
const Status_1 = require("../models/Credit/Status");
let CreditLog = class CreditLog extends Base_1.CommonBase {
};
__decorate([
    typeorm_1.ManyToOne(() => User_1.User),
    schema_1.Property(),
    __metadata("design:type", User_1.User)
], CreditLog.prototype, "user", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], CreditLog.prototype, "userId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => Application_1.Application),
    __metadata("design:type", Application_1.Application)
], CreditLog.prototype, "app", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], CreditLog.prototype, "appId", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], CreditLog.prototype, "amount", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], CreditLog.prototype, "note", void 0);
__decorate([
    typeorm_1.OneToOne(() => PepayLog_1.PepayLog),
    typeorm_1.JoinColumn(),
    schema_1.Property(PepayLog_1.PepayLog),
    __metadata("design:type", PepayLog_1.PepayLog)
], CreditLog.prototype, "pepay", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", Number)
], CreditLog.prototype, "pepayId", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Enum(Status_1.CreditStatus),
    __metadata("design:type", String)
], CreditLog.prototype, "status", void 0);
CreditLog = __decorate([
    typeorm_1.Entity()
], CreditLog);
exports.CreditLog = CreditLog;
//# sourceMappingURL=CreditLog.js.map