import {BodyParams, Controller, PathParams, Post} from '@tsed/common';
import {Description, Returns, Security, Summary} from '@tsed/schema';
import {AuthApp} from '../../decorators/AuthApp';
import {BearerTokenAuth} from '../../middlewares/BearerTokenAuth';
import {AuthAppInfo} from '../../models/AppAuthInfo';
import {BaseRes} from '../../models/Res';
import {ValidEmailAddBody} from '../../models/Valid/EmailBody';
import {SMSBody, SMSValidBody} from '../../models/Valid/SMSBody';
import {ValidTemp} from '../../models/Valid/Temp';
import {EmailService} from '../../services/EmailService';
import {ResponseService} from '../../services/Response';
import {SMSService} from '../../services/SMSService';

@Controller('/verify')
@BearerTokenAuth()
@Description('手機 & 信箱驗證相關 API')
export class ValidController {
  constructor(
    private sms: SMSService,
    private email: EmailService,
    private res: ResponseService
  ) {}

  @Summary('發送手機驗證碼')
  @Post('/sms')
  @Returns(200, BaseRes)
  async sendSMS(@AuthApp() app: AuthAppInfo, @BodyParams() data: SMSBody) {
    await this.sms.sendSMSCode(app.appId, data.phone, data.temp);
  }

  @Summary('驗證手機驗證碼')
  @Post('/sms/:phone')
  @Returns(200, BaseRes)
  async validSMS(
    @PathParams('phone') phone: string,
    @BodyParams() data: SMSValidBody
  ) {
    await this.sms.validSMSCode(phone, data.smsCode);
    return this.res.Ok();
  }

  @Summary('發送Email驗證信')
  @Post('/email')
  @Returns(200, BaseRes)
  async sendEmail(
    @BodyParams() data: ValidEmailAddBody,
    @AuthApp() app: AuthAppInfo
  ) {
    await this.email.sendValidEmail(app.appId, data);
    return this.res.Ok();
  }

  @Summary('驗證Email token')
  @Post('/email/:token')
  @Returns(200, BaseRes)
  async validEmail(@PathParams('token') token: string) {
    return this.res.Ok();
  }
}
