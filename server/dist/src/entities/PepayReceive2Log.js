"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PepayReceive2Log = void 0;
const schema_1 = require("@tsed/schema");
const typeorm_1 = require("typeorm");
const Base_1 = require("./Common/Base");
const PepayLog_1 = require("./PepayLog");
let PepayReceive2Log = class PepayReceive2Log extends Base_1.CommonBase {
};
__decorate([
    typeorm_1.OneToOne(() => PepayLog_1.PepayLog),
    typeorm_1.JoinColumn(),
    __metadata("design:type", PepayLog_1.PepayLog)
], PepayReceive2Log.prototype, "pepay", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayReceive2Log.prototype, "pepayId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "sessId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "orderId", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "billId", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "dataId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "shopId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "payType", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "prodId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "userId", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayReceive2Log.prototype, "sourceAmount", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayReceive2Log.prototype, "amount", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "currency", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayReceive2Log.prototype, "dataCode", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayReceive2Log.prototype, "tradeCode", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "shopPara", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "cdate", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "ctime", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "billDate", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "billTime", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "date", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "time", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayReceive2Log.prototype, "checkCode", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], PepayReceive2Log.prototype, "isReturn", void 0);
PepayReceive2Log = __decorate([
    typeorm_1.Entity()
], PepayReceive2Log);
exports.PepayReceive2Log = PepayReceive2Log;
//# sourceMappingURL=PepayReceive2Log.js.map