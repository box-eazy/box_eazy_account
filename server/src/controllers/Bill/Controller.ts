import {
  BodyParams,
  Controller,
  Delete,
  Get,
  Patch,
  PathParams,
  Post,
  Put,
  QueryParams,
} from '@tsed/common';
import {Description, Returns, Security, Summary} from '@tsed/schema';
import {AuthApp} from '../../decorators/AuthApp';
import {BillLog} from '../../entities/BillLog';
import {BearerTokenAuth} from '../../middlewares/BearerTokenAuth';
import {AuthAppInfo} from '../../models/AppAuthInfo';
import {BillAddBody} from '../../models/Bill/AddBody';
import {BillListItem} from '../../models/Bill/ListItem';
import {DateRangeParams} from '../../models/Common/DateRange';
import {PageParams} from '../../models/Common/Page';
import {BillService} from '../../services/BillService';
import {ResponseService} from '../../services/Response';

@Controller('/billing')
@Description('出帳相關Api')
@BearerTokenAuth()
export class BillController {
  constructor(private bill: BillService, private res: ResponseService) {}

  @Summary('取得出帳列表')
  @Get()
  @Returns(200, ResponseService.toPagiation(BillLog))
  findAll(
    @QueryParams() dateRange: DateRangeParams,
    @QueryParams() pageParams: PageParams,
    @QueryParams('userId') userId: number
  ) {
    return this.res.toPage(this.bill.findAll(dateRange, pageParams));
  }

  @Summary('新增出帳資料')
  @Post()
  @Returns(200, ResponseService.toResult(BillLog))
  add(@BodyParams() data: BillAddBody, @AuthApp() app: AuthAppInfo) {
    return this.res.toResult(this.bill.create(app.appId, data));
  }

  @Summary('修改出帳資料(部份更新)')
  @Patch('/:id')
  @Returns(200, ResponseService.toResult(BillLog))
  edit(@PathParams('id') id: number, @BodyParams() data: BillAddBody) {
    return this.res.toResult(this.bill.edit(id, data));
  }

  @Summary('刪除出帳資料')
  @Delete('/:id')
  @Returns(200, ResponseService.toResult(BillLog))
  delete(@PathParams('id') id: number) {
    return this.res.toResult(this.bill.delete(id));
  }
}
