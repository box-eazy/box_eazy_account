import EazyCommon from '@eazy/widget';
import React, {ChangeEvent, useCallback, useState} from 'react';
import {RouteComponentProps} from 'react-router-dom';
import {ResetPasswordApi} from '../Api/Forget';
import {getApiErrorMessage} from '../libs/handleError';
export const EazyResetPassword: React.FC<
  RouteComponentProps<{token: string}>
> = () => {
  const {token} = EazyCommon.ReactRouterDom.useParams<{token: string}>();
  const history = EazyCommon.ReactRouterDom.useHistory();
  const [loading, setLoading] = useState(false);
  const modal = EazyCommon.ModalUtils.useModal();
  const [data, setData] = useState({
    phone: '',
    password: '',
    confirmPassword: '',
  });
  const $onChange = useCallback(
    (field: keyof typeof data) => (e: ChangeEvent<HTMLInputElement>) => {
      setData(prevData => ({
        ...prevData,
        [field]: e.target.value,
      }));
    },
    []
  );

  const $onSubmit = useCallback(async () => {
    setLoading(true);
    try {
      const res = await ResetPasswordApi(token, data);
      await modal.alert('重設密碼成功，將返回登入頁');
      history.push(`/api_key=${res.apiKey}&redirect_url=${res.redirectUrl}`);
    } catch (err) {
      await modal.alert(getApiErrorMessage(err));
    }
    setLoading(false);
  }, [token, data]);

  return (
    <EazyCommon.Layout.Wrapper
      headerTitle="重設密碼"
      headerRight={() => {
        <EazyCommon.Layout.Button
          icon="Home"
          onClick={() => history.push('/')}
        />;
      }}
      footer={
        <EazyCommon.Box p={12}>
          <EazyCommon.Button
            form="EazyResetPassword"
            color="primary"
            block
            size="lg"
            disabled={
              Object.values(data).includes('') ||
              data.password !== data.confirmPassword
            }
          >
            完成
          </EazyCommon.Button>
        </EazyCommon.Box>
      }
    >
      <EazyCommon.Form.Wrapper id="EazyResetPassword" onSubmit={$onSubmit}>
        <EazyCommon.Form.FormItem title="手機號碼">
          <EazyCommon.Controls.Input
            placeholder="請輸入手機號碼"
            required
            value={data.phone}
            onChange={$onChange('phone')}
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="密碼">
          <EazyCommon.Controls.Input
            placeholder="請輸入密碼"
            required
            value={data.password}
            onChange={$onChange('password')}
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="確認密碼">
          <EazyCommon.Controls.Input
            placeholder="再一次輸入密碼"
            required
            value={data.confirmPassword}
            onChange={$onChange('confirmPassword')}
          />
        </EazyCommon.Form.FormItem>
      </EazyCommon.Form.Wrapper>
    </EazyCommon.Layout.Wrapper>
  );
};
