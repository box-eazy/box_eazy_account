"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseService = void 0;
const di_1 = require("@tsed/di");
const schema_1 = require("@tsed/schema");
const Error_1 = require("../models/Error");
const Res_1 = require("../models/Res");
let ResponseService = class ResponseService {
    static toPagiation(instance) {
        class Cls extends Res_1.BaseRes {
        }
        __decorate([
            schema_1.CollectionOf(instance),
            __metadata("design:type", Array)
        ], Cls.prototype, "items", void 0);
        __decorate([
            schema_1.Property(),
            __metadata("design:type", Number)
        ], Cls.prototype, "total", void 0);
        Object.defineProperty(Cls, 'name', {
            value: `${instance.name}PageItems`,
        });
        return Cls;
    }
    static toItems(instance) {
        class Cls extends Res_1.BaseRes {
        }
        __decorate([
            schema_1.CollectionOf(instance),
            __metadata("design:type", Array)
        ], Cls.prototype, "items", void 0);
        Object.defineProperty(Cls, 'name', {
            value: `${instance.name}Items`,
        });
        return Cls;
    }
    static toResult(instance) {
        class Cls extends Res_1.BaseRes {
        }
        __decorate([
            schema_1.Property(instance),
            __metadata("design:type", Object)
        ], Cls.prototype, "result", void 0);
        Object.defineProperty(Cls, 'name', {
            value: `${instance.name}Result`,
        });
        return Cls;
    }
    async toResult(wait) {
        return {
            result: await wait,
            errCode: Error_1.ErrorCode.SUCCESS,
        };
    }
    async toItems(wait) {
        return {
            items: await wait,
            errCode: Error_1.ErrorCode.SUCCESS,
        };
    }
    async toPage(wait) {
        const result = {
            ...(await wait),
            errCode: Error_1.ErrorCode.SUCCESS,
        };
        console.log(result);
        return result;
    }
    async Ok() {
        return {
            errCode: Error_1.ErrorCode.SUCCESS,
        };
    }
    async Error(code, message) {
        return {
            errCode: code,
            errMessage: message,
        };
    }
};
ResponseService.Base = Res_1.BaseRes;
ResponseService = __decorate([
    di_1.Service()
], ResponseService);
exports.ResponseService = ResponseService;
//# sourceMappingURL=Response.js.map