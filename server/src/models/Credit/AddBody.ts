import {Description, Enum, Property} from '@tsed/schema';
import {BaseModel} from '../Common/BaseModel';
import {CreditStatus} from './Status';

export class CreditAddBody extends BaseModel<CreditAddBody> {
  @Property()
  userId: number;
  @Property()
  amount: number;
  @Property()
  note: string;

  @Enum(CreditStatus)
  @Description('')
  status: CreditStatus;
}
