import React, {FormEvent, useCallback, useMemo, useState} from 'react';
import qs from 'qs';
import {WebLogin} from '../Api/Login';
import EazyCommon from '@eazy/widget';
import GoogleLogin from 'react-google-login';

export const Login: React.FC = () => {
  const history = EazyCommon.ReactRouterDom.useHistory();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const qsInfo = useMemo<{
    redirect_url: string;
    api_key: string;
  }>(() => {
    return qs.parse(window.location.search.slice(1)) as any;
  }, []);

  const $onSubmit = useCallback(
    async (e: FormEvent) => {
      e.preventDefault();
      try {
        const token = await WebLogin(qsInfo.api_key, username, password);
        window.location.href = qsInfo.redirect_url + `?token=${token}`;
      } catch (err) {
        window.alert(err.response.data.errMessage);
      }
    },
    [username, password, qsInfo]
  );

  const $onGoogleLoginSuccess = useCallback(() => {}, []);
  const $onGoogleLoginFailed = useCallback(() => {}, []);
  return (
    <EazyCommon.Layout.Wrapper
      back
      headerTitle="登入"
      footer={
        <div className="m-3">
          <EazyCommon.Divider>其它</EazyCommon.Divider>
          <EazyCommon.Button
            color="warning"
            size="lg"
            block
            onClick={() => {
              history.push(`/register${window.location.search}`);
            }}
          >
            一般註冊
          </EazyCommon.Button>
          <GoogleLogin
            clientId={
              '613502523574-ae0lnh2qk7jf2io0do0n16hgaoq2s408.apps.googleusercontent.com'
            }
            scope="profile email"
            onSuccess={$onGoogleLoginSuccess}
            onFailure={$onGoogleLoginFailed}
            render={renderProps => (
              <EazyCommon.Button
                color="danger"
                size="lg"
                block
                {...renderProps}
              >
                Google帳號登入/註冊
              </EazyCommon.Button>
            )}
          />
        </div>
      }
    >
      <EazyCommon.Form.Wrapper onSubmit={$onSubmit}>
        <EazyCommon.Form.FormItem title="會員帳號">
          <EazyCommon.Controls.Input
            placeholder="輸入帳號"
            value={username}
            onChange={e => setUsername(e.target.value)}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Form.FormItem title="會員密碼">
          <EazyCommon.Controls.Input
            placeholder="輸入密碼"
            value={password}
            onChange={e => setPassword(e.target.value)}
            required
          />
        </EazyCommon.Form.FormItem>
        <EazyCommon.Button
          color="link"
          block
          className="text-right"
          type="button"
          onClick={() => {
            history.push(`/forget${window.location.search}`);
          }}
        >
          忘記密碼了嗎？點我重新設定密碼
        </EazyCommon.Button>
        <EazyCommon.Button color="primary" block size="lg" type="submit">
          登入
        </EazyCommon.Button>
      </EazyCommon.Form.Wrapper>
    </EazyCommon.Layout.Wrapper>
  );
  // return (
  //   <div>
  //     <form onSubmit={$onSubmit}>
  //       <div>
  //         <input
  //           type="text"
  //           value={username}
  //           onChange={e => setUsername(e.target.value)}
  //           placeholder="username"
  //         />
  //       </div>
  //       <div>
  //         <input
  //           type="text"
  //           value={password}
  //           onChange={e => setPassword(e.target.value)}
  //           placeholder="password"
  //         />
  //       </div>
  //       <button>login</button>
  //     </form>
  //   </div>
  // );
};
