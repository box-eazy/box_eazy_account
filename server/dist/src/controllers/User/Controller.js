"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const Body_1 = require("../../models/User/Body");
const List_1 = require("../../models/User/List");
const Response_1 = require("../../services/Response");
const AddBody_1 = require("../../models/User/AddBody");
const BearerTokenAuth_1 = require("../../middlewares/BearerTokenAuth");
const AuthApp_1 = require("../../decorators/AuthApp");
const AppAuthInfo_1 = require("../../models/AppAuthInfo");
const UserSerivce_1 = require("../../services/UserSerivce");
const Search_1 = require("../../models/User/Search");
const UpdatePayloadBody_1 = require("../../models/User/UpdatePayloadBody");
let UserController = class UserController {
    constructor(user, res) {
        this.user = user;
        this.res = res;
    }
    // @Summary('簡單搜尋會員列表')
    // @Returns(200, ResponseService.toPagiation(UserListItem))
    // @Get()
    // findAll(
    //   @AuthApp() app: AuthAppInfo,
    //   @QueryParams('keyword') keyword: string,
    //   @QueryParams() page: PageParams,
    //   @QueryParams('appId') appId: number
    // ) {
    //   return this.res.toPage(this.user.findAll(appId, keyword, page));
    // }
    adFindAll(data) {
        return this.res.toPage(this.user.adFindAll(data));
    }
    findOne(id) {
        return this.res.toResult(this.user.findOne(id));
    }
    add(app, data) {
        return this.res.toResult(this.user.create(app.appId, data));
    }
    edit(id, data) {
        return this.res.toResult(this.user.patchUpdate(id, data));
    }
    delete(id) {
        return this.res.toResult(this.user.delete(id));
    }
    updatePayloadPut(app, id, data) {
        return this.res.toResult(this.user.updateMultiPayload(app.appId, id, data, true));
    }
    updatePayloadPatch(app, id, data) {
        return this.res.toResult(this.user.updateMultiPayload(app.appId, id, data, false));
    }
    updatePayload(app, id, fieldName, fieldValue) {
        return this.res.toResult(this.user.updatePayload(app.appId, id, fieldName, fieldValue));
    }
    deletePayload(app, id, fieldName) {
        return this.res.toResult(this.user.deletePayload(app.appId, id, fieldName));
    }
};
__decorate([
    schema_1.Summary('搜尋會員'),
    schema_1.Description(`
    filters.key 命名方式
    * 一般欄位的部份直接帶入對應的columnName (EX: nickname)
    * payload的部份一律以 {payload.[fieldName]} 來定義

    filters 查詢方式
    * Array第一層皆會做 And 判斷
    * Array第二層則會做 OR 判斷
      EX:
      [
        { key: 'A', value: '1' },
        [
          { key: 'B', value: '2' },
          { key: 'C', value: '3' }
        ]
      ]
      則會產生
      A = '1' AND ( B = '2' OR C = '3')

    filter Params 說明
    {
      key: 要過濾的欄位,
      value: 要搜尋的值 (可undefined & null),
      isNot: true/false, 是否反轉查詢結果
    }

    filter.value 說明
    若value帶入 null 以及 undefined時，針對一般欄位以及payload的搜尋是不一樣的
    * 若是一般欄位，則會判斷該欄位是否為NULL
    * 若是payload, 則會判斷是否存在該欄位
    
  `),
    common_1.Post('/search'),
    schema_1.Returns(200, Response_1.ResponseService.toPagiation(List_1.UserListItem)),
    __param(0, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Search_1.UserSearchBody]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "adFindAll", null);
__decorate([
    schema_1.Summary('透過userId取得會員資料'),
    common_1.Get('/:id'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Body_1.UserBody)),
    __param(0, common_1.PathParams('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "findOne", null);
__decorate([
    schema_1.Summary('新增會員'),
    common_1.Post(),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Body_1.UserBody)),
    __param(0, AuthApp_1.AuthApp()), __param(1, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [AppAuthInfo_1.AuthAppInfo, AddBody_1.UserAddBody]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "add", null);
__decorate([
    schema_1.Summary('修改會員資料(部份修改)'),
    common_1.Patch('/:id'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Body_1.UserBody)),
    __param(0, common_1.PathParams('id')), __param(1, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, AddBody_1.UserAddBody]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "edit", null);
__decorate([
    schema_1.Summary('刪除會員資料'),
    common_1.Delete('/:id'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Body_1.UserBody)),
    __param(0, common_1.PathParams('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "delete", null);
__decorate([
    schema_1.Summary('更新會員的payload (完全覆蓋更新)'),
    common_1.Put('/:id/payload'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Body_1.UserBody)),
    __param(0, AuthApp_1.AuthApp()),
    __param(1, common_1.PathParams('id')),
    __param(2, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [AppAuthInfo_1.AuthAppInfo, Number, UpdatePayloadBody_1.UserUpdatePayloadBody]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "updatePayloadPut", null);
__decorate([
    schema_1.Summary('更新會員的payload(僅新增/更新Body內的欄位)'),
    common_1.Patch('/:id/payload'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Body_1.UserBody)),
    __param(0, AuthApp_1.AuthApp()),
    __param(1, common_1.PathParams('id')),
    __param(2, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [AppAuthInfo_1.AuthAppInfo, Number, UpdatePayloadBody_1.UserUpdatePayloadBody]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "updatePayloadPatch", null);
__decorate([
    schema_1.Summary('修改會員特定的Payload(若不存在則新增)'),
    common_1.Put('/:id/payload/:fieldName'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(Body_1.UserBody)),
    __param(0, AuthApp_1.AuthApp()),
    __param(1, common_1.PathParams('id')),
    __param(2, common_1.PathParams('fieldName')),
    __param(3, common_1.BodyParams('fieldValue')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [AppAuthInfo_1.AuthAppInfo, Number, String, String]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "updatePayload", null);
__decorate([
    schema_1.Summary('刪除指定的Payload'),
    common_1.Delete('/:id/payload/:fieldName'),
    __param(0, AuthApp_1.AuthApp()),
    __param(1, common_1.PathParams('id')),
    __param(2, common_1.PathParams('fieldName')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [AppAuthInfo_1.AuthAppInfo, Number, String]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "deletePayload", null);
UserController = __decorate([
    common_1.Controller('/users'),
    BearerTokenAuth_1.BearerTokenAuth(),
    schema_1.Description('使用者操作相關api'),
    __metadata("design:paramtypes", [UserSerivce_1.UserService, Response_1.ResponseService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=Controller.js.map