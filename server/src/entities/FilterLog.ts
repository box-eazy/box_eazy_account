import {Property} from '@tsed/schema';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  Unique,
} from 'typeorm';
import {FilterLogType} from '../models/FilterLog/AddBody';
import {Application} from './Application';
import {CommonBase} from './Common/Base';
import {User} from './User';

@Entity()
export class FilterLog extends CommonBase {
  @ManyToOne(() => User, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({
    name: 'phone',
    referencedColumnName: 'phone',
  })
  user?: User;

  @Column({nullable: false})
  @Property()
  @Index()
  phone: string;

  @Property()
  @Column({})
  type: FilterLogType;

  @Column()
  note: string;

  @Column()
  @Property()
  appId: number;

  @ManyToOne(() => Application)
  app?: Application;
}
