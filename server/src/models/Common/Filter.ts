import {Any, Description, Ignore, Property} from '@tsed/schema';

export class FilterParam {
  @Property()
  @Any()
  key: string;

  @Property()
  @Any()
  @Description('key = value')
  value?: null | string;

  @Property()
  @Description('NOT key = value')
  isNot?: boolean;
}
