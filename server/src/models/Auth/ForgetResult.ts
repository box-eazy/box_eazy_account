import {Property} from '@tsed/schema';
import {BaseModel} from '../Common/BaseModel';

export class ForgetResult extends BaseModel<ForgetResult> {
  @Property()
  redirectUrl: string;

  @Property()
  apiKey: string;
}
