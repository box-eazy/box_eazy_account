import {CollectionOf, Description, Property} from '@tsed/schema';

export class PageParams {
  static toTypeOrmOptions(param: PageParams) {
    return {
      take: param.pageSize,
      skip: param.pageIndex * param.pageSize,
    };
  }

  @Property()
  @Description('分頁，從0開始')
  pageIndex: number;

  @Property()
  pageSize: number;
}

export class PageResult<T> {
  items: T[];
  total: number;
}
