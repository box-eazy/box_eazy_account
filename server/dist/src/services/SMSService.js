"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SMSService = void 0;
const di_1 = require("@tsed/di");
const axios_1 = require("axios");
const date_fns_1 = require("date-fns");
const SMSLog_1 = require("../entities/SMSLog");
const SMSRepo_1 = require("../repositorys/SMSRepo");
const BaseError_1 = require("../exceptions/BaseError");
const UserRepo_1 = require("../repositorys/UserRepo");
const Temp_1 = require("../models/Valid/Temp");
const typeorm_1 = require("typeorm");
const User_1 = require("../entities/User");
let SMSService = class SMSService {
    constructor(sms, user) {
        this.sms = sms;
        this.user = user;
        this.id = '588';
        this.password = '588mms588';
        this.smsUrl = 'http://api.message.net.tw/send.php';
    }
    randomCode(len = 6) {
        let code = '';
        for (let i = 0; i < len; i++) {
            code = `${code}${Math.floor(Math.random() * 10)}`;
        }
        return code;
    }
    async sendSMS(phone, content) {
        const res = await axios_1.default.request({
            url: `${this.smsUrl}`,
            params: {
                id: this.id,
                password: this.password,
                tel: phone,
                msg: content,
                mtype: 'G',
                encoding: 'utf8',
            },
        });
    }
    async sendSMSCode(appId, phone, temp, manager) {
        const dbData = await manager.findOne(User_1.User, {
            phone: phone,
        });
        if (temp === Temp_1.ValidTemp.UPDATE_PROFILE || temp === Temp_1.ValidTemp.FOREGT) {
            if (dbData === undefined || dbData === null) {
                throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_NOT_FOUND);
            }
            await manager.save(dbData);
        }
        else if (temp === Temp_1.ValidTemp.REGISTER) {
            if (dbData !== undefined && dbData !== null) {
                throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.USER_PHONE_IS_EXIST);
            }
        }
        const code = this.randomCode(6);
        const expired = date_fns_1.addMinutes(new Date(), 10);
        const content = `您的驗證碼為：${code}，請在${date_fns_1.format(expired, 'yyyy-MM-dd HH:mm:ss')}前使用`;
        try {
            await this.sendSMS(phone, content);
        }
        catch (err) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.SMS_SEND_ERROR);
        }
        await manager.save(new SMSLog_1.SMSLog({
            appId,
            code,
            phone,
            expired,
            isUse: false,
            createdAt: new Date(),
            content,
            temp,
        }));
    }
    async $validSMSCode(phone, code, manager) {
        const dbData = await manager.findOne(SMSLog_1.SMSLog, {
            phone,
            code,
        });
        console.log(dbData);
        if (dbData === undefined || dbData === null) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.SMS_CODE_NOT_FOUND);
        }
        if (dbData.isUse) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.SMS_CODE_IS_USE);
        }
        if (dbData.expired < new Date()) {
            throw new BaseError_1.BaseError(BaseError_1.BaseError.Code.SMS_CODE_EXPIRED);
        }
        dbData.isUse = true;
        await manager.save(dbData);
    }
    async validSMSCode(phone, code, manager) {
        return this.$validSMSCode(phone, code, manager);
    }
};
__decorate([
    typeorm_1.Transaction(),
    __param(3, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, String, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], SMSService.prototype, "sendSMSCode", null);
__decorate([
    typeorm_1.Transaction(),
    __param(2, typeorm_1.TransactionManager()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, typeorm_1.EntityManager]),
    __metadata("design:returntype", Promise)
], SMSService.prototype, "validSMSCode", null);
SMSService = __decorate([
    di_1.Service(),
    __metadata("design:paramtypes", [SMSRepo_1.SMSRepo, UserRepo_1.UserRepo])
], SMSService);
exports.SMSService = SMSService;
//# sourceMappingURL=SMSService.js.map