import {EntityRepository, FindConditions, Repository} from 'typeorm';
import {CommonBase} from '../entities/Common/Base';
import * as _ from 'lodash';

export class BaseRepo<T extends CommonBase> extends Repository<T> {
  async exist(where: FindConditions<T>) {
    return (await this.count(where)) > 0;
  }

  differenceList<T extends object>(
    originalList: T[],
    newList: T[],
    differenceKey: keyof T
  ) {
    const result: {
      update: T[];
      remove: T[];
      add: T[];
    } = {
      update: [],
      remove: [],
      add: [],
    };

    const oldKeys = originalList.map(x => x[differenceKey]);
    const newKeys = newList.map(x => x[differenceKey]);

    const newValue = _.keyBy(newList, differenceKey);

    for (const original of originalList) {
      if (!newKeys.includes(original[differenceKey])) {
        result.remove.push(original);
      } else {
        const newData = Object.assign(
          original,
          newValue[original[differenceKey] as any] as any
        );

        result.update.push(newData);
      }
    }

    for (const newItem of newList) {
      if (!oldKeys.includes(newItem[differenceKey])) {
        result.add.push(newItem);
      }
    }

    return result;
  }
}
