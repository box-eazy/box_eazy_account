"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PepayController = void 0;
const common_1 = require("@tsed/common");
const schema_1 = require("@tsed/schema");
const AuthApp_1 = require("../../decorators/AuthApp");
const PepayLog_1 = require("../../entities/PepayLog");
const BaseError_1 = require("../../exceptions/BaseError");
const BearerTokenAuth_1 = require("../../middlewares/BearerTokenAuth");
const AppAuthInfo_1 = require("../../models/AppAuthInfo");
const DateRange_1 = require("../../models/Common/DateRange");
const Page_1 = require("../../models/Common/Page");
const Add_1 = require("../../models/Pepay/Add");
const PostForm_1 = require("../../models/Pepay/PostForm");
const Receive1_1 = require("../../models/Pepay/Receive1");
const Receive2_1 = require("../../models/Pepay/Receive2");
const PepayService_1 = require("../../services/PepayService");
const Response_1 = require("../../services/Response");
let PepayController = class PepayController {
    constructor(pepay, res) {
        this.pepay = pepay;
        this.res = res;
    }
    findAll(page, dateRange) {
        return this.res.toPage(this.pepay.findAll(page, dateRange));
    }
    async create(data, app) {
        return this.res.toResult(this.pepay.create(app.appId, data));
    }
    findOne() { }
    async receive1(data) {
        const res = await this.pepay.createReceive1(data);
        return this.pepay.toOutputString(res);
    }
    async receive2(data) {
        try {
            const res = await this.pepay.createReceive2(data);
            return 'RES_CODE=0';
        }
        catch (err) {
            console.log('receive2 error', err);
            if (err instanceof BaseError_1.BaseError) {
                if (err.errCode === BaseError_1.BaseError.Code.PEPAY_RECEIVE02_IS_EXIST) {
                    return 'RES_CODE=20290';
                }
            }
            return 'RES_CODE=20000';
        }
    }
};
__decorate([
    common_1.Get(),
    schema_1.Summary('取得PepayList'),
    schema_1.Returns(200, Response_1.ResponseService.toPagiation(PepayLog_1.PepayLog)),
    BearerTokenAuth_1.BearerTokenAuth(),
    __param(0, common_1.QueryParams()),
    __param(1, common_1.QueryParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Page_1.PageParams,
        DateRange_1.DateRangeParams]),
    __metadata("design:returntype", void 0)
], PepayController.prototype, "findAll", null);
__decorate([
    schema_1.Summary('建立Pepay訂單'),
    common_1.Post(),
    schema_1.Returns(200, Response_1.ResponseService.toResult(PostForm_1.PepayPostForm)),
    BearerTokenAuth_1.BearerTokenAuth(),
    __param(0, common_1.BodyParams()), __param(1, AuthApp_1.AuthApp()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Add_1.PepayAddBody, AppAuthInfo_1.AuthAppInfo]),
    __metadata("design:returntype", Promise)
], PepayController.prototype, "create", null);
__decorate([
    schema_1.Summary('取得訂單資料 & 狀態'),
    common_1.Get('/:orderId'),
    schema_1.Returns(200, Response_1.ResponseService.toResult(PepayLog_1.PepayLog)),
    BearerTokenAuth_1.BearerTokenAuth(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], PepayController.prototype, "findOne", null);
__decorate([
    common_1.Post('/:orderId/receive1'),
    schema_1.Returns(200, String),
    __param(0, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Receive1_1.PepayReceive1Body]),
    __metadata("design:returntype", Promise)
], PepayController.prototype, "receive1", null);
__decorate([
    common_1.Get('/:orderId/receive2'),
    __param(0, common_1.QueryParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Receive2_1.PepayReceive2Body]),
    __metadata("design:returntype", Promise)
], PepayController.prototype, "receive2", null);
PepayController = __decorate([
    common_1.Controller('/pepay'),
    schema_1.Description('Pepay金流相關API'),
    __metadata("design:paramtypes", [PepayService_1.PepayService, Response_1.ResponseService])
], PepayController);
exports.PepayController = PepayController;
//# sourceMappingURL=Controller.js.map