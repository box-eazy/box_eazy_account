"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PepayReceive2LogRepo = exports.PepayReceive1LogRepo = exports.PepayLogRepo = void 0;
const typeorm_1 = require("typeorm");
const PepayLog_1 = require("../entities/PepayLog");
const PepayReceive1Log_1 = require("../entities/PepayReceive1Log");
const PepayReceive2Log_1 = require("../entities/PepayReceive2Log");
const BaseRepo_1 = require("./BaseRepo");
let PepayLogRepo = class PepayLogRepo extends BaseRepo_1.BaseRepo {
};
PepayLogRepo = __decorate([
    typeorm_1.EntityRepository(PepayLog_1.PepayLog)
], PepayLogRepo);
exports.PepayLogRepo = PepayLogRepo;
let PepayReceive1LogRepo = class PepayReceive1LogRepo extends BaseRepo_1.BaseRepo {
};
PepayReceive1LogRepo = __decorate([
    typeorm_1.EntityRepository(PepayReceive1Log_1.PepayReceive1Log)
], PepayReceive1LogRepo);
exports.PepayReceive1LogRepo = PepayReceive1LogRepo;
let PepayReceive2LogRepo = class PepayReceive2LogRepo extends BaseRepo_1.BaseRepo {
};
PepayReceive2LogRepo = __decorate([
    typeorm_1.EntityRepository(PepayReceive2Log_1.PepayReceive2Log)
], PepayReceive2LogRepo);
exports.PepayReceive2LogRepo = PepayReceive2LogRepo;
//# sourceMappingURL=PepayLogRepo.js.map