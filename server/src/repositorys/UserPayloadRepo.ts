import {EntityRepository} from 'typeorm';
import {UserPayload} from '../entities/UserPayload';
import {BaseRepo} from './BaseRepo';

@EntityRepository(UserPayload)
export class UserPayloadRepo extends BaseRepo<UserPayload> {}
