import {
  Controller,
  Get,
  Middleware,
  Next,
  Req,
  Res,
  Session,
  UseAfter,
  UseBefore,
} from '@tsed/common';
import * as fs from 'fs';
import {ROOT_DIR} from '../../envConfig';
import * as path from 'path';
import {AuthService} from '../services/AuthService';

@Middleware()
class SendIndexFile {
  use(@Res() res: Res) {
    console.log('send indexFile');
    return res.sendFile(path.join(ROOT_DIR, '../client/build/index.html'));
  }
}

@Middleware()
class CheckParam {
  constructor(private auth: AuthService) {}
  async use(@Req() req: Req, @Res() res: Res, @Next() next: any) {
    const {api_key, redirect_url}: any = req.query;
    const user = (req.session as any).user;
    console.log(req.session);
    if (api_key === undefined) {
      res.send('api_key not found');
    } else if (redirect_url === undefined) {
      res.send('redirect_url not found');
    } else {
      next();
    }
  }
}

@Middleware()
class CheckSession {
  constructor(private auth: AuthService) {}
  async use(@Req() req: Req, @Res() res: Res, @Next() next: any) {
    const {api_key, redirect_url}: any = req.query;
    const user = (req.session as any).user;
    if (user !== undefined) {
      res.redirect(
        `${redirect_url}?token=${await this.auth.generateToken(
          api_key,
          user.id
        )}`
      );
      next();
    } else {
      next();
    }
  }
}

@Controller('/')
export class WebViewController {
  @Get('/')
  @UseBefore(CheckParam, CheckSession)
  Index() {
    return fs.readFileSync(path.join(ROOT_DIR, '../client/build/index.html'));
  }

  @Get('/register')
  @UseBefore(CheckParam, CheckSession)
  Register() {
    return fs.readFileSync(path.join(ROOT_DIR, '../client/build/index.html'));
  }

  @Get('/forget')
  @UseBefore(CheckParam, CheckSession)
  Forget() {
    return fs.readFileSync(path.join(ROOT_DIR, '../client/build/index.html'));
  }

  @Get('/logout')
  @UseBefore(CheckParam)
  Logout(@Session() session, @Res() res: Res, @Req() req: Req) {
    delete session.user;
    res.redirect(req.query.redirect_url as any);
  }
}
