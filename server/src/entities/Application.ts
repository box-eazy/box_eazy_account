import {Description, Property} from '@tsed/schema';
import {Column, Entity} from 'typeorm';
import {CommonBase} from './Common/Base';
import * as randToken from 'rand-token';

@Entity()
export class Application extends CommonBase {
  @Column()
  @Property()
  name: string;

  @Column()
  @Property()
  apiKey: string;

  @Column()
  @Property()
  apiSecert: string;

  @Column()
  @Property()
  isDisabled: boolean;

  @Column()
  @Property()
  @Description('是否納入統計')
  isCreditStats: boolean;

  static generateRandomToken(len: number) {
    return randToken.generate(len);
  }
}
