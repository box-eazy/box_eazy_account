"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepo = void 0;
const typeorm_1 = require("typeorm");
const User_1 = require("../entities/User");
const UserPayload_1 = require("../entities/UserPayload");
const List_1 = require("../models/User/List");
const BaseRepo_1 = require("./BaseRepo");
let UserRepo = class UserRepo extends BaseRepo_1.BaseRepo {
    async search(data) {
        const { keyword, page } = data;
        let qb = this.createQueryBuilder('user').where('user.deletedAt is null');
        qb = qb.leftJoinAndSelect('user.payloads', 'payload');
        qb = qb.leftJoinAndSelect('user.filters', 'filters');
        if (data.userIds && data.userIds.length > 0) {
            qb = qb.andWhere('user.id IN (:...userIds)', { userIds: data.userIds });
        }
        if (keyword !== undefined) {
            qb.andWhere(new typeorm_1.Brackets(qb => {
                // qb.where(qb => {
                //   const sub = qb
                //     .subQuery()
                //     .from(UserPayload, 'sub_payload')
                //     .select('userId')
                //     .distinct(true)
                //     .where('user.id = sub_payload.userId')
                //     .andWhere('sub_payload.fieldValue LIKE :keyword')
                //     .getQuery();
                //   return `user.id IN ${sub}`;
                // });
                // qb.orWhere('user.email LIKE :keyword');
                qb.orWhere('user.phone LIKE :keyword');
                qb.orWhere('user.username LIKE :keyword');
                qb.orWhere('user.nickname LIKE :keyword');
                return qb;
            }));
            qb.setParameter('keyword', `%${keyword}%`);
        }
        if (data.filters !== undefined) {
            const { filters } = data;
            for (const filter of filters) {
                qb = qb.andWhere(qb => {
                    const result = [];
                    const processPayload = (filter) => {
                        const key = filter.key.replace('payload.', '');
                        let payloadQb = qb
                            .subQuery()
                            .from(UserPayload_1.UserPayload, 'filter_payload')
                            .select('userId')
                            .distinct();
                        if (filter.value === undefined || filter.value === null) {
                            const gruopConcatPayload = qb
                                .subQuery()
                                .from(UserPayload_1.UserPayload, 'p')
                                .select('GROUP_CONCAT(p.fieldName)', 'fieldNames')
                                .addSelect('p.userId', 'userId')
                                .groupBy('userId');
                            payloadQb = qb
                                .subQuery()
                                .from(`(${gruopConcatPayload.getQuery()})`, 'p')
                                .select('userId')
                                .distinct()
                                .where(`p.fieldNames ${filter.isNot ? 'NOT' : ''} LIKE '%${key}%'`);
                        }
                        else {
                            payloadQb = payloadQb.where(`user.id = filter_payload.userId AND (filter_payload.fieldName='${key}' AND filter_payload.fieldValue='${filter.value}')`);
                        }
                        return `user.id IN ${payloadQb.getQuery()}`;
                    };
                    const processGeneral = (filter) => {
                        if (filter.value) {
                            return `${filter.isNot ? 'NOT' : ''} user.${filter.key} = ${filter.value}`;
                        }
                        else {
                            return `user.${filter.key} IS ${filter.isNot ? '' : 'NOT'} NULL`;
                        }
                    };
                    if (Array.isArray(filter)) {
                        const orList = [];
                        for (const subFilter of filter) {
                            if (subFilter.key.includes('payload.')) {
                                orList.push(processPayload(subFilter));
                            }
                            else {
                                orList.push(processGeneral(subFilter));
                            }
                        }
                        if (orList.length > 0) {
                            result.push(orList.join(' OR '));
                        }
                    }
                    else {
                        if (filter.key.includes('payload.')) {
                            result.push(processPayload(filter));
                        }
                        else {
                            result.push(processGeneral(filter));
                        }
                    }
                    const text = result.map(x => `(${x})`).join('AND');
                    return text;
                });
            }
        }
        if (page !== undefined &&
            page.pageSize !== undefined &&
            page.pageIndex !== undefined) {
            qb = qb.skip(page.pageIndex * page.pageSize);
            qb = qb.take(page.pageSize);
        }
        if (data.orderBy !== undefined) {
            if (data.orderBy.key.includes('payload.')) {
                qb = qb.addSelect(sqb => sqb
                    .subQuery()
                    .from(UserPayload_1.UserPayload, 'orderPayload')
                    .select('fieldValue')
                    .where('fieldName=:orderPayloadKey AND orderPayload.userId = user.id', { orderPayloadKey: data.orderBy.key.replace('payload.', '') }), 'payloadOrderBy');
                qb.orderBy({
                    payloadOrderBy: data.orderBy.by,
                });
            }
            else {
                qb = qb.orderBy({
                    [`user.${data.orderBy.key}`]: data.orderBy.by,
                });
            }
        }
        const [items, total] = await qb.getManyAndCount();
        return {
            items: items.map(x => new List_1.UserListItem(x)),
            total,
        };
    }
};
UserRepo = __decorate([
    typeorm_1.EntityRepository(User_1.User)
], UserRepo);
exports.UserRepo = UserRepo;
//# sourceMappingURL=UserRepo.js.map