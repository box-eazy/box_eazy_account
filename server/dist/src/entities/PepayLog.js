"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PepayLog = void 0;
const schema_1 = require("@tsed/schema");
const typeorm_1 = require("typeorm");
const User_1 = require("./User");
const Application_1 = require("./Application");
const Base_1 = require("./Common/Base");
const PepayReceive1Log_1 = require("./PepayReceive1Log");
const PepayReceive2Log_1 = require("./PepayReceive2Log");
let PepayLog = class PepayLog extends Base_1.CommonBase {
};
__decorate([
    typeorm_1.ManyToOne(() => Application_1.Application),
    __metadata("design:type", Application_1.Application)
], PepayLog.prototype, "app", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayLog.prototype, "appId", void 0);
__decorate([
    typeorm_1.ManyToOne(() => User_1.User),
    schema_1.Property(User_1.User),
    __metadata("design:type", User_1.User)
], PepayLog.prototype, "user", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayLog.prototype, "userId", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayLog.prototype, "shopId", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayLog.prototype, "orderId", void 0);
__decorate([
    typeorm_1.Column({
        type: 'text',
    }),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayLog.prototype, "orderItem", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", Number)
], PepayLog.prototype, "amount", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayLog.prototype, "currency", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayLog.prototype, "prodId", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayLog.prototype, "shopPara", void 0);
__decorate([
    typeorm_1.Column(),
    schema_1.Property(),
    __metadata("design:type", String)
], PepayLog.prototype, "checkCode", void 0);
__decorate([
    typeorm_1.OneToOne(() => PepayReceive1Log_1.PepayReceive1Log, x => x.pepay),
    schema_1.Property(PepayReceive1Log_1.PepayReceive1Log),
    __metadata("design:type", PepayReceive1Log_1.PepayReceive1Log)
], PepayLog.prototype, "receive1", void 0);
__decorate([
    typeorm_1.OneToOne(() => PepayReceive2Log_1.PepayReceive2Log, x => x.pepay),
    schema_1.Property(PepayReceive2Log_1.PepayReceive2Log),
    __metadata("design:type", PepayReceive2Log_1.PepayReceive2Log)
], PepayLog.prototype, "receive2", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", String)
], PepayLog.prototype, "returnUrl", void 0);
PepayLog = __decorate([
    typeorm_1.Entity()
], PepayLog);
exports.PepayLog = PepayLog;
//# sourceMappingURL=PepayLog.js.map