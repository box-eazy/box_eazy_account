import {Property} from '@tsed/schema';
import {Column, PrimaryGeneratedColumn} from 'typeorm';

export class CommonBase<T = any> {
  @PrimaryGeneratedColumn()
  @Property()
  id: number;

  @Column({nullable: true})
  @Property()
  createdAt?: Date | null;

  @Column({nullable: true})
  updatedAt?: Date | null;

  @Column({nullable: true})
  deletedAt?: Date | null;
  constructor(init?: Partial<T>) {
    Object.assign(this, init);
  }
}
