import {Property} from '@tsed/schema';
import {Column, Entity, JoinColumn, ManyToOne, OneToOne} from 'typeorm';
import {User} from './User';
import {Application} from './Application';
import {CommonBase} from './Common/Base';
import {PepayReceive1Log} from './PepayReceive1Log';
import {PepayReceive2Log} from './PepayReceive2Log';

@Entity()
export class PepayLog extends CommonBase<PepayLog> {
  @ManyToOne(() => Application)
  app?: Application;

  @Column()
  @Property()
  appId: number;

  @ManyToOne(() => User)
  @Property(User)
  user?: User;

  @Column()
  @Property()
  userId: number;

  @Column()
  @Property()
  shopId: string;

  @Column()
  @Property()
  orderId: string;

  @Column({
    type: 'text',
  })
  @Property()
  orderItem: string;

  @Column()
  @Property()
  amount: number;

  @Column()
  @Property()
  currency: string;

  @Column()
  @Property()
  prodId: string;

  @Column()
  @Property()
  shopPara: string;

  @Column()
  @Property()
  checkCode: string;

  @OneToOne(() => PepayReceive1Log, x => x.pepay)
  @Property(PepayReceive1Log)
  receive1: PepayReceive1Log;

  @OneToOne(() => PepayReceive2Log, x => x.pepay)
  @Property(PepayReceive2Log)
  receive2: PepayReceive2Log;

  @Column({nullable: true})
  returnUrl: string;
}
