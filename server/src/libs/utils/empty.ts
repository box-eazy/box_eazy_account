export const isEmpty = <T>(
  value: T | null | undefined
): value is null | undefined => {
  return value !== null && value !== undefined;
};

export const notEmpty = <T>(value: T | null | undefined): value is T =>
  !isEmpty(value);
