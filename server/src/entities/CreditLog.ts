import {Column, Entity, JoinColumn, ManyToOne, OneToOne} from 'typeorm';
import {User} from './User';
import {Application} from './Application';
import {CommonBase} from './Common/Base';
import {PepayLog} from './PepayLog';
import {Enum, Property} from '@tsed/schema';
import {CreditStatus} from '../models/Credit/Status';

@Entity()
export class CreditLog extends CommonBase<CreditLog> {
  @ManyToOne(() => User)
  @Property()
  user?: User;

  @Column()
  @Property()
  userId: number;

  @ManyToOne(() => Application)
  app?: Application;

  @Column()
  @Property()
  appId: number;

  @Column()
  @Property()
  amount: number;

  @Column()
  @Property()
  note: string;

  @OneToOne(() => PepayLog)
  @JoinColumn()
  @Property(PepayLog)
  pepay?: PepayLog | null;

  @Column({nullable: true})
  @Property()
  pepayId: number | null;

  @Column()
  @Enum(CreditStatus)
  status: CreditStatus;
}
