import {baseAxios} from './Base';

export const SendSMSApi = async (
  apiKey: string,
  phone: string,
  type: 'REGISTER' | 'FORGET' | 'UPDATE_PROFILE'
) => {
  await baseAxios.request({
    url: '/web/sms',
    method: 'POST',
    data: {
      apiKey,
      phone,
      type,
    },
  });
};
