import {Column} from 'typeorm';
import {CommonBase} from './Common/Base';

export class FileLog extends CommonBase<FileLog> {
  @Column()
  url: string;
}
