import {Service} from '@tsed/di';
import {EntityManager, Like, Transaction, TransactionManager} from 'typeorm';
import {PepayLog} from '../entities/PepayLog';
import {PepayAddBody} from '../models/Pepay/Add';
import {PepayLogRepo} from '../repositorys/PepayLogRepo';
import {BaseService} from './BaseSerivce';
import {GenerateOrderId} from '../libs/generator/orderId';
import {PepayPostForm} from '../models/Pepay/PostForm';
import {MD5} from 'crypto-js';
import {PepayReceive1Body, PepayReceive1Result} from '../models/Pepay/Receive1';
import {BaseError} from '../exceptions/BaseError';
import {PepayReceive1Log} from '../entities/PepayReceive1Log';
import {PepayReceive2Body} from '../models/Pepay/Receive2';
import {PepayReceive2Log} from '../entities/PepayReceive2Log';
import {CreditLog} from '../entities/CreditLog';
import {CreditStatus} from '../models/Credit/Status';
import {PageParams} from '../models/Common/Page';
import {DateRangeParams} from '../models/Common/DateRange';
import axios from 'axios';

@Service()
export class PepayService extends BaseService {
  private generateOrderId: GenerateOrderId;

  private shopId = 'PPS_172049';
  private shopTrustCode = 'm1EWEGM4Te';
  private sysTrustCode = '0MSl72xico';

  private postUrl = 'https://gate.pepay.com.tw/pepay/paysel_amt.php';

  constructor(private pepayLog: PepayLogRepo) {
    super();
    this.generateOrderId = new GenerateOrderId('EAZY', 'yyyyMMdd', 10);
  }

  private encodeCheckCode(arr: any[]) {
    const str = arr.join('#');
    console.log(str);
    return MD5(str).toString();
  }

  private getCheckCode(data: PepayLog) {
    return this.encodeCheckCode([
      this.sysTrustCode,
      this.shopId,
      data.orderId,
      data.amount,
      this.shopTrustCode,
    ]);
  }

  private getReceive1CheckCode(data: PepayReceive1Body) {
    return this.encodeCheckCode([
      this.sysTrustCode,
      data.SHOP_ID,
      data.ORDER_ID,
      data.AMOUNT,
      data.SESS_ID,
      data.PROD_ID,
      this.shopTrustCode,
    ]);
  }

  private getReceive2CheckCode(data: PepayReceive2Body) {
    console.log('receive2 encode');
    return this.encodeCheckCode([
      this.sysTrustCode,
      data.SHOP_ID,
      data.ORDER_ID,
      data.AMOUNT,
      data.SESS_ID,
      data.PROD_ID,
      data.USER_ID,
      this.shopTrustCode,
    ]);
  }

  async findAll(page: PageParams, dateRange: DateRangeParams) {
    const [items, total] = await this.pepayLog.findAndCount({
      relations: ['user', 'receive1', 'receive2'],
      where: {
        ...DateRangeParams.toWhereBetween('createdAt', dateRange),
      },
      ...PageParams.toTypeOrmOptions(page),
    });

    return {
      items,
      total,
    };
  }

  @Transaction()
  async create(
    appId: number,
    data: PepayAddBody,
    @TransactionManager() manager?: EntityManager
  ): Promise<PepayPostForm> {
    let orderNum = 1;
    const lastOrder = await manager.findOne(PepayLog, undefined, {
      where: {
        orderId: Like(`${this.generateOrderId.getPrefix()}%`),
      },
      order: {
        createdAt: 'DESC',
      },
    });

    if (lastOrder !== undefined) {
      orderNum = this.generateOrderId.getOrderNum(lastOrder.orderId) + 1;
    }

    let dbData = new PepayLog({
      ...data,
      appId,
      orderId: this.generateOrderId.generateOrderId(orderNum),
      createdAt: new Date(),
    });

    dbData.checkCode = this.getCheckCode(dbData);

    dbData = await manager.save(dbData);

    await manager.save(
      new CreditLog({
        pepayId: dbData.id,
        status: CreditStatus.CREATED,
        createdAt: new Date(),
        ...dbData,
      })
    );

    return new PepayPostForm({
      data: {
        ORDER_ID: dbData.orderId,
        AMOUNT: dbData.amount,
        SHOP_ID: this.shopId,
        PROD_ID: '',
        SHOP_PARA: '',
        ORDER_ITEM: dbData.orderItem,
        CURRENCY: 'TWD',
        CHECK_CODE: dbData.checkCode,
      },
      url: this.postUrl,
    });
  }

  @Transaction()
  async createReceive1(
    data: PepayReceive1Body,
    @TransactionManager() manager?: EntityManager
  ): Promise<PepayReceive1Result> {
    if (data.CHECK_CODE !== this.getReceive1CheckCode(data)) {
      throw new BaseError(BaseError.Code.PEPAY_RECEIVE01_CHECK_CODE_FAILED);
    }
    const dbData = await manager.findOne(
      PepayLog,
      {orderId: data.ORDER_ID},
      {
        relations: ['receive1', 'receive2'],
      }
    );

    if (dbData.receive1) {
      await manager.delete(PepayReceive1Log, {
        orderId: data.ORDER_ID,
      });
      // throw new BaseError(BaseError.Code.PEPAY_RECEIVE01_IS_EXIST);
    }

    const receive = new PepayReceive1Log({
      shopId: data.SHOP_ID,
      orderId: data.ORDER_ID,
      sessId: data.SESS_ID,
      prodId: data.PROD_ID,
      amount: data.AMOUNT,
      pepayId: dbData.id,
      userId: `${dbData.userId}`,
      checkCode: data.CHECK_CODE,
      shopPara: data.SHOP_PARA,
    });

    try {
      await axios({
        url: dbData.returnUrl,
        method: 'POST',
        data: {
          type: 'receive1',
          data,
        },
      });
      receive.isReturn = true;
    } catch (err) {
      receive.isReturn = false;
    }

    await manager.save(receive);

    await manager.update(
      CreditLog,
      {pepayId: dbData.id},
      {
        status: CreditStatus.WAITING,
        updatedAt: new Date(),
      }
    );

    return new PepayReceive1Result({
      RES_CODE: 0,
      USER_ID: receive.userId.toString(),
      SHOP_PARA: receive.shopPara,
      RET_URL: `http://13.113.142.165:30011/api/pepay/${dbData.orderId}/receive2`,
    });
  }

  @Transaction()
  async createReceive2(
    data: PepayReceive2Body,
    @TransactionManager() manager?: EntityManager
  ) {
    if (data.CHECK_CODE !== this.getReceive2CheckCode(data)) {
      throw new BaseError(BaseError.Code.PEPAY_RECEIVE02_CHECK_CODE_FAILED);
    }

    const dbData = await manager.findOne(
      PepayLog,
      {orderId: data.ORDER_ID},
      {
        relations: ['receive1', 'receive2'],
      }
    );

    if (dbData.receive2) {
      await manager.delete(PepayReceive2Log, {
        orderId: data.ORDER_ID,
      });
      // throw new BaseError(BaseError.Code.PEPAY_RECEIVE02_IS_EXIST);
    }

    const receive = new PepayReceive2Log({
      pepayId: dbData.id,
      sessId: data.SESS_ID,
      orderId: data.ORDER_ID,
      billId: data.BILL_ID,
      dataId: data.DATA_ID,
      shopId: data.SHOP_ID,
      payType: data.PAY_TYPE,
      prodId: data.PROD_ID,
      userId: data.USER_ID,
      sourceAmount: data.SOURCE_AMOUNT,
      amount: data.AMOUNT,
      currency: data.CURRENCY,
      dataCode: data.DATA_CODE,
      tradeCode: data.TRADE_CODE,
      shopPara: data.SHOP_PARA,
      cdate: data.CDATE,
      ctime: data.CTIME,
      billDate: data.BILL_DATE,
      billTime: data.BILL_TIME,
      date: data.DATE,
      time: data.TIME,
      checkCode: data.CHECK_CODE,
    });

    try {
      await axios({
        url: dbData.returnUrl,
        method: 'POST',
        data: {
          type: 'receive2',
          data,
        },
      });
      receive.isReturn = true;
    } catch (err) {
      receive.isReturn = false;
    }

    await manager.save(receive);

    await manager.update(
      CreditLog,
      {
        pepayId: dbData.id,
      },
      {
        updatedAt: new Date(),
        status: CreditStatus.SUCCESS,
      }
    );

    return new PepayReceive1Result({
      RES_CODE: 0,
    });
  }

  toOutputString(obj: {[key: string]: any}) {
    return Object.keys(obj)
      .map(key => `${key}=${obj[key]}`)
      .join('&');
  }
}
