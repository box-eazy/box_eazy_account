"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailLog = void 0;
const schema_1 = require("@tsed/schema");
const typeorm_1 = require("typeorm");
const Temp_1 = require("../models/Valid/Temp");
const Application_1 = require("./Application");
const Base_1 = require("./Common/Base");
let EmailLog = class EmailLog extends Base_1.CommonBase {
};
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], EmailLog.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({
        type: 'text',
    }),
    __metadata("design:type", String)
], EmailLog.prototype, "content", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], EmailLog.prototype, "token", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Date)
], EmailLog.prototype, "expired", void 0);
__decorate([
    typeorm_1.Column({ default: false }),
    __metadata("design:type", Boolean)
], EmailLog.prototype, "isUse", void 0);
__decorate([
    typeorm_1.Column({
        comment: '驗證email的url',
    }),
    __metadata("design:type", String)
], EmailLog.prototype, "validUrl", void 0);
__decorate([
    typeorm_1.Column({
        comment: '驗證成功時導向的連結',
    }),
    schema_1.Property(),
    __metadata("design:type", String)
], EmailLog.prototype, "redirectUrl", void 0);
__decorate([
    typeorm_1.ManyToOne(() => Application_1.Application),
    __metadata("design:type", Application_1.Application)
], EmailLog.prototype, "app", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Number)
], EmailLog.prototype, "appId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], EmailLog.prototype, "temp", void 0);
EmailLog = __decorate([
    typeorm_1.Entity()
], EmailLog);
exports.EmailLog = EmailLog;
//# sourceMappingURL=EmailLog.js.map