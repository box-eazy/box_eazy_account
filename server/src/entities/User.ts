import {Property} from '@tsed/schema';
import {
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import {UserPayload} from './UserPayload';
import {CommonBase} from './Common/Base';
import {Application} from './Application';
import {FilterLog} from './FilterLog';
import {FilterLogRepo} from '../repositorys/FilterLogRepo';
import {RegisterProvider} from '../models/User/RegisterProvider';

@Entity()
export class User extends CommonBase {
  constructor(init?: Partial<User>) {
    super(init);
  }
  @Column()
  @Property()
  username: string;

  @Column()
  password: string;

  @Column({nullable: true})
  facebookId: string | null;

  @Column({nullable: true})
  googleId: string | null;

  @Column({nullable: false})
  @Property()
  phone: string;

  @Column({nullable: false})
  @Property()
  email: string;

  @Column()
  @Property()
  nickname: string;

  @Column({default: false})
  isEmailValid: boolean;

  @Column({default: false})
  isSmsValid: boolean;

  @Column({default: false})
  isDisabled: boolean;

  @Column({nullable: true})
  image: string | null;

  @OneToMany(() => UserPayload, x => x.user)
  payloads: UserPayload[];

  @Column({
    nullable: true,
  })
  lastLoginAt: Date | null;

  @Column()
  appId: number;

  @ManyToOne(() => Application)
  app?: Application;

  @OneToMany(() => FilterLog, x => x.user, {
    createForeignKeyConstraints: false,
  })
  @JoinColumn({
    name: 'phone',
    referencedColumnName: 'phone',
  })
  filters?: FilterLog[];

  @Column({
    comment: '註冊方式',
    default: 'GENERAL',
  })
  provider?: RegisterProvider;
}
